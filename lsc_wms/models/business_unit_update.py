# -*- coding: utf-8 -*-
import pdb

from odoo import models, fields, api
from odoo.exceptions import UserError, ValidationError


class wms_bu_update(models.Model):
    _name = 'wms.bu.update'
    _inherit = ['mail.thread', 'mail.activity.mixin', 'mail.render.mixin']
    _description = 'wms_bu_update'
    _rec_name = 'document_no'

    document_no = fields.Char(string='Document Number')
    transaction_date = fields.Date(string='Transaction Date')
    customer_id = fields.Many2one('res.partner',string='Customer')
    warehouse_id = fields.Many2one('lsc.sub.warehouse',store=True,string='Warehouse')
    warehouse_main_id = fields.Many2one(related='warehouse_id.warehouse_id', string='Main Warehouse')
    state = fields.Selection([('draft', 'Draft'), ('validated', 'Validated')], string="Status",
                             default='draft',tracking=True)
    labour_id = fields.Many2one('lsc.labour.master', track_visibility='onchange', string="Labour")
    bu_update_line_ids = fields.One2many('bu.update.line','bu_update_id',string='Business Unit Update Lines')
    # notes_id = fields.Many2one('inventory.hold.notes',string='Note')

    def bu_updated(self):
        for rec in self:
            for line in rec.bu_update_line_ids:
                if line.inventory_report_id:
                    transaction_vals = {
                        'quantity': -(line.inventory_report_id.total_quantity),
                        'remarks': line.reason,
                        'lot1': line.lot1,
                        'lot2': line.lot2,
                        'lot3': line.lot3,
                        'lot4': line.lot4,
                        'lot5': line.lot5,
                        'lot6': line.lot6.id,
                        'lot7': line.lot7.id,
                        'lot8': line.lot8,
                        'lot9': line.lot9,
                        'lot10': line.lot10,
                        'serial_no': line.serial_no or False,
                        'description': line.description,
                        'uom_id': line.uom_id.id,
                        'sku_id': line.sku_id.id,
                        'pack_key_id': line.pack_key_id.id,
                        'bussines_unit_id': line.inventory_report_id.bussines_unit_id.id,
                        'batch_no': line.batch_no or False,
                        'manf_date': line.manf_date,
                        'expiry_date': line.expiry_date,
                        'lot': line.lot,
                        'location': line.location.id,
                        'transaction_type': 'adjustment',
                        'document_no': rec.document_no,
                        'customer_id': rec.customer_id.id,
                        'date': fields.date.today(),
                        'warehouse_id': line.inventory_report_id.warehouse_id.id or False,
                    }
                    transaction = self.env['wms.transactions'].create(transaction_vals)
                    line.inventory_report_id.bussines_unit_id = line.new_business_unit_id.id
                    transaction_vals = {
                        'quantity': line.inventory_report_id.total_quantity,
                        'remarks': line.reason,
                        'lot1': line.lot1,
                        'lot2': line.lot2,
                        'lot3': line.lot3,
                        'lot4': line.lot4,
                        'lot5': line.lot5,
                        'lot6': line.lot6.id,
                        'lot7': line.lot7.id,
                        'lot8': line.lot8,
                        'lot9': line.lot9,
                        'lot10': line.lot10,
                        'serial_no': line.serial_no or False,
                        'description': line.description,
                        'uom_id': line.uom_id.id,
                        'sku_id': line.sku_id.id,
                        'pack_key_id': line.pack_key_id.id,
                        'bussines_unit_id': line.new_business_unit_id.id,
                        'batch_no': line.batch_no or False,
                        'manf_date': line.manf_date,
                        'expiry_date': line.expiry_date,
                        'lot': line.lot,
                        'location': line.location.id,
                        'transaction_type': 'adjustment',
                        'document_no': rec.document_no,
                        'customer_id': rec.customer_id.id,
                        'date': fields.date.today(),
                        'warehouse_id': line.inventory_report_id.warehouse_id.id or False,
                    }
                    transaction = self.env['wms.transactions'].create(transaction_vals)

            rec.transaction_date = fields.date.today()
            rec.state = 'validated'

    @api.model
    def create(self, vals):
        transfer_seq = self.env['ir.sequence'].next_by_code('seq.wms.bu.update')
        vals['document_no'] = transfer_seq
        res = super(wms_bu_update, self).create(vals)
        return res


    @api.model
    def open_bu_update_wizard(self):
        return {
            'name': "Business Unit Update",
            'type': 'ir.actions.act_window',
            'view_mode': 'form',
            'view_type': 'form',
            'res_model': 'bu.update.wizard',
            'views': [(False, 'form')],
            'target': 'new',
        }

class bu_update_line(models.Model):
    _name = 'bu.update.line'
    _inherit = ['mail.thread', 'mail.activity.mixin', 'mail.render.mixin']
    _description = 'bu_update_line'

    bu_update_id = fields.Many2one('wms.bu.update',string='Document Number')
    transaction_date = fields.Date(related='bu_update_id.transaction_date',string='Transaction Date',store=True)
    customer_id = fields.Many2one(related='bu_update_id.customer_id', string='Customer',store=True)
    warehouse_id = fields.Many2one(store=True, string='Warehouse')
    sku_id = fields.Many2one('lsc.sku.master', string="SKU")
    description = fields.Char(string='Description')
    uom_id = fields.Many2one('uom.uom', string='UOM')
    batch_no = fields.Char(string="Batch No")
    serial_no = fields.Char(string="Serial No")
    bussines_unit_id = fields.Many2one('lsc.bussines.unit', string="Business Unit")
    business_id = fields.Many2many(related='sku_id.business_id', string="Business Unit", tracking=True)
    new_business_unit_id = fields.Many2one('lsc.bussines.unit', string="New Business Unit",domain="[('id','in', business_id)]")
    location_type = fields.Many2one(related='location.location_type', string='Location Type', store=True)
    manf_date = fields.Date(string="Manufacturing Date")
    expiry_date = fields.Date("Expiry Date")
    lot = fields.Char("Lot No")
    lot1 = fields.Char("Lottable01")
    lot2 = fields.Char('Lottable02')
    lot3 = fields.Char('Lottable03')
    lot4 = fields.Char('Lottable04')
    lot5 = fields.Char('Lottable05')
    lot6 = fields.Many2one('sku.lotable.six',string='Lottable06')
    lot7 = fields.Many2one('sku.lotable',string='Lot07')
    lot8 = fields.Datetime('Lottable08')
    lot9 = fields.Datetime('Lottable09')
    lot10 = fields.Datetime('Lottable10')
    remarks = fields.Char("Remarks")

    location = fields.Many2one('lsc.location.master', string='Location')
    location_id = fields.Integer(related='location.id', string='Location id',store=True)
    qty = fields.Float(string='Quantity', store=True)
    state = fields.Selection(related='bu_update_id.state',string='Status')
    pack_key_id = fields.Many2one('lsc.pack.key', 'Pack Key')
    inventory_report_id = fields.Many2one('inventory.report', string='Inventory Report ID')

    reason = fields.Text(string='Reason')

    # def show_detail(self):
    #     self.ensure_one()
    #     return {
    #         'name': "WMS Transfer Form",
    #         'type': 'ir.actions.act_window',
    #         'view_type': 'form',
    #         'view_mode': 'form',
    #         'res_model': 'wms.transfers',
    #         'res_id': self.wms_transfer_id.id,
    #     }

class bu_update_wizard(models.TransientModel):
    _name = 'bu.update.wizard'
    _inherit = ['mail.thread', 'mail.activity.mixin', 'mail.render.mixin']
    _description = 'bu_update_wizard'

    customer_id = fields.Many2one('res.partner', string='Customer',domain="[('wms','=',True),('customer_rank','=',1)]",required=True)
    sku_id = fields.Many2one('lsc.sku.master', string="SKU")
    batch_no = fields.Char(string="Batch No")
    serial_no = fields.Char(string="Serial No")
    bussines_unit_id = fields.Many2one('lsc.bussines.unit', string="Business Unit")
    location = fields.Many2one('lsc.location.master',string='Location')
    wizard_line_ids = fields.One2many('bu.wizard.lines','bu_update_wizard_id',string='Inventory Report Lines',readonly=False)
    warehouse_id = fields.Many2one('lsc.sub.warehouse', required=True, string='Warehouse')
    # type = fields.Selection([('hold', 'Hold'), ('unhold', 'Un-Hold')], string="Type",required=True)


    @api.onchange('sku_id', 'batch_no', 'serial_no', 'bussines_unit_id', 'location','customer_id','warehouse_id')
    def get_inventory_report_lines(self):
        for rec in self:
            domain = []
            if rec.customer_id:
                domain += [('customer_id', '=', rec.customer_id.id)]
            if rec.sku_id:
                domain += [('sku_id', '=', rec.sku_id.id)]
            if rec.batch_no:
                domain += [('batch_no', '=', rec.batch_no)]
            if rec.serial_no:
                domain += [('serial_no', '=', rec.serial_no)]
            if rec.bussines_unit_id:
                domain += [('bussines_unit_id', '=', rec.bussines_unit_id.id)]
            if rec.location:
                domain += [('location', '=', rec.location.id)]
            if rec.warehouse_id:
                domain += [('warehouse_id', '=', rec.warehouse_id.id)]
            if domain != []:
                # wizard_lines_previous = self.env['hold.wizard.lines'].search([])
                # wizard_lines_previous.unlink()
                from_locaiton_inventory_line = self.env['inventory.report'].search(domain)
                # rec.wizard_line_ids = from_locaiton_inventory_line.ids
                wizard_lines = self.env['bu.wizard.lines']
                for line in from_locaiton_inventory_line.filtered(lambda b: b.available_qty > 0):
                    vals = {
                        'customer_id': line.customer_id.id,
                        'sku_id': line.sku_id.id,
                        'serial_no': line.serial_no or False,
                        'bussines_unit_id': line.bussines_unit_id.id,
                        'batch_no': line.batch_no or False,
                        'location': line.location.id or False,
                        'bu_update_wizard_id': rec.id,
                        'inventory_report_id': line.id,
                        'lot': line.lot,
                        'qty': line.available_qty,
                    }
                    wizard_line = self.env['bu.wizard.lines'].create(vals)
                    wizard_lines += wizard_line

                rec.wizard_line_ids = wizard_lines.ids
            else:
                rec.wizard_line_ids = False

    def map_inventory_report_lines(self):
        for rec in self:
            wizard_line = self.env['bu.wizard.lines'].search([('id', 'in', rec.wizard_line_ids.ids)])

            transfer_vals = {
                'customer_id': rec.customer_id.id or False,
                'warehouse_id': rec.warehouse_id.id,
            }
            wms_bu_update = self.env['wms.bu.update'].create(transfer_vals)

            for line in rec.wizard_line_ids.filtered(lambda b: b.select == True):
                transfer_line_vals = {
                    'sku_id': line.inventory_report_id.sku_id.id,
                    'location': line.inventory_report_id.location.id,
                    'qty': line.inventory_report_id.available_qty,
                    'description': line.inventory_report_id.description,
                    'uom_id': line.inventory_report_id.uom_id.id,
                    'serial_no': line.inventory_report_id.serial_no or False,
                    'lot': line.inventory_report_id.lot,
                    'bussines_unit_id': line.inventory_report_id.bussines_unit_id.id,
                    'batch_no': line.inventory_report_id.batch_no or False,
                    'remarks': line.inventory_report_id.remarks,
                    'pack_key_id': line.inventory_report_id.pack_key_id.id,
                    'lot1': line.inventory_report_id.lot1,
                    'lot2': line.inventory_report_id.lot2,
                    'lot3': line.inventory_report_id.lot3,
                    'lot4': line.inventory_report_id.lot4,
                    'lot5': line.inventory_report_id.lot5,
                    'lot6': line.inventory_report_id.lot6.id,
                    'lot7': line.inventory_report_id.lot7.id,
                    'lot8': line.inventory_report_id.lot8,
                    'lot9': line.inventory_report_id.lot9,
                    'lot10': line.inventory_report_id.lot10,
                    'manf_date': line.inventory_report_id.manf_date,
                    'expiry_date': line.inventory_report_id.expiry_date,
                    'inventory_report_id': line.inventory_report_id.id,
                    'bu_update_id': wms_bu_update.id,
                    'warehouse_id': line.inventory_report_id.warehouse_id.id,
                }
                adjustment_line = self.env['bu.update.line'].create(transfer_line_vals)


class bu_wizard_lines(models.TransientModel):
    _name = 'bu.wizard.lines'
    _description = 'bu_wizard_lines'

    customer_id = fields.Many2one('res.partner', string='Customer')
    sku_id = fields.Many2one('lsc.sku.master', string="SKU")
    batch_no = fields.Char(string="Batch No")
    serial_no = fields.Char(string="Serial No")
    bussines_unit_id = fields.Many2one('lsc.bussines.unit', string="Business Unit")
    location = fields.Many2one('lsc.location.master', string='Location')
    bu_update_wizard_id = fields.Many2one('bu.update.wizard',string='Business Unit Update Wizard Id')
    inventory_report_id = fields.Many2one('inventory.report',string='Inventory Report ID')
    select = fields.Boolean(string='Select')
    lot = fields.Char("Lot No")
    qty = fields.Float(string='Quantity')