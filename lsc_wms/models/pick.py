from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError

class Pick(models.Model):
    _name = 'lsc.pick'
    _inherit = ['mail.thread', 'mail.activity.mixin', 'mail.render.mixin']
    _description = "Pick"


    name= fields.Char("Order ID")
    order_type = fields.Selection([
        ('delivery','Delivery'),
        ('vendor_return','Vendor Return'),
        ('stock_trnasfer','Stock Transfer')],string="Order Type")
    external_ref = fields.Char("External Ref")
    invoice_number = fields.Char("Invoice Number")
    allocation_id = fields.Many2one('lsc.allocation',"Outbound ID")
    order_date_time = fields.Datetime("Order Date time")
    customer_id = fields.Many2one('res.partner',"Customer",domain="[('customer_rank','>',0)]")
    supplier_id = fields.Many2one('res.partner',"Supplier/Vendor",domain="[('supplier_rank','>',0)]")
    remarks = fields.Text("Remarks")

    order_ids = fields.One2many('lsc.pick.line','order_id',string="Order Lines")

    state = fields.Selection([('draft','Draft'),('part_picked','Part Picked'),('picked','Picked'),('closed','closed')],string="state",default='draft')

    # def create_pick(self):
    #     create_pick = self.env['lsc.dispatch'].create({
    #         'order_id':self.id
    #     })
    #
    # def dispatch_line(self,):
    #     lines = {
    #         'sku_id':
    #     }
class PickLines(models.Model):
    _name = 'lsc.pick.line'

    order_id = fields.Many2one('lsc.pick',"Rel")
    line_no = fields.Integer("Line no")
    sku_id = fields.Many2one('lsc.sku.master',"SKU")
    description = fields.Char("Description")
    uom_id = fields.Many2one('uom.uom',"UOM")
    qty = fields.Float("Quantity")
    allocation_qty = fields.Float("Allocated QTY")
    batch_number = fields.Char("Batch No")
    # serilizad_bo = fields.Boolean("Serilized Bo")
    serial_no = fields.Char("Serial Number")
    lot = fields.Char("Lot")
    bussines_id = fields.Many2one('lsc.bussines.unit', string="Business Unit")
    remarks = fields.Char("Remarks")
    labour_id = fields.Many2one('res.users',"Labour ID")
    allocation_line_id = fields.Many2one('lsc.allocation.line',"Allocation Line")