import pdb

from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError

class GrnWms(models.Model):
    _name = 'lsc.grn'
    _inherit = ['mail.thread', 'mail.activity.mixin', 'mail.render.mixin']
    _description = 'Grn'

    name = fields.Char('Name',readonly=1)
    asn_id = fields.Many2one('lsc.asn',"ASN ID")
    external_ref = fields.Char("External Ref")
    po_number = fields.Char("PO Number")
    asn_datetime = fields.Datetime("ASN Date & time")
    customer_id = fields.Many2one('res.partner',"Customer ",domain="[('customer_rank','>',0),('wms','=',True)]")
    warhouse_ids = fields.Many2many(related='customer_id.warhouse_ids', string="Warehouse Mapping")
    warehouse_id = fields.Many2one('lsc.sub.warehouse',store=True,string='Warehouse',domain="[('id','in', warhouse_ids)]")
    warehouse_main_id = fields.Many2one(related='warehouse_id.warehouse_id', string='Main Warehouse')
    vendor_id = fields.Many2one('res.partner',"Vendor ",domain="[('supplier_rank','>',0),('wms','=',True)]")
    asn_type = fields.Selection([('purchase','Purchase'),('cust_return','Customer Return'),('stock_transfer','Stock Transfer')],required=1,string="ASN Type")
    grn_datetime = fields.Datetime("GRN Date & time",default= fields.Datetime.now(),readonly=1)
    grn_line_ids = fields.One2many('lsc.grn.lines','grn_id',ondelete='cascade', index=True, string="GRN Lines")
    state = fields.Selection(
        [('new', 'New'), ('in_receiving', 'In Receiving'), ('received', 'Received'),
         ('close', 'Close'), ('cancel', 'Cancel')], default='new', string='State')
    receiving_date = fields.Datetime(string='Receiving Date')
    for_compute = fields.Char(string='For Compute', compute='for_compute_in_grn')
    for_close = fields.Boolean(string='For Close')
    labour_id = fields.Many2one('lsc.labour.master',string='Labour')



    def action_putaway_open(self):
        self.ensure_one()
        action = self.env["ir.actions.actions"]._for_xml_id("lsc_wms.action_wms_putaway")
        putaway = self.env['wms.putaway'].search([('grn_id', '=', self.id)])
        action['domain'] = [('id', 'in', putaway.ids)]
        return action



    # def write(self, values):
    #     result = super(GrnWms, self).write(values)
    #
    #     # if not any(self.grn_line_ids.filtered(lambda b: b.state != 'received')):
    #     #     if self.state != 'received':
    #     #         # pdb.set_trace()
    #     #         self.write({'state': 'received'})
    #     #         print(self.state,"IF conditon")
    #     #         self.asn_id.state = 'received'
    #     #         self.received_closed_grn()
    #     # print(self.state)
    #     return result




    # @api.depends('grn_line_ids', 'grn_line_ids.state')
    def for_compute_in_grn(self):
        for rec in self:
            if not any(rec.grn_line_ids.filtered(
                    lambda b: b.state != 'received' and b.recieved_qty != b.expected_qty)):
                if rec.state != 'received':
                    rec.write({'state': 'received'})
                    rec.asn_id.state = 'received'
                    rec.receiving_date = fields.Datetime.today()
                    rec.received_closed_grn()
                    rec.create_putaway()
            rec.for_compute = "For Compute in GRN"







    def in_receiving_grn(self):
        for rec in self:
            rec.state = 'in_receiving'
            rec.asn_id.state = 'in_receiving'

    def close_grn(self):
        for rec in self:
            rec.receiving_date = fields.Datetime.today()
            rec.received_closed_grn()
            rec.create_putaway()
            if any(rec.grn_line_ids.filtered(lambda b: b.recieved_qty != b.expected_qty)):
                rec.state = 'close'
                rec.asn_id.state = 'close'

    def received_closed_grn(self):
        for rec in self:
            data_dict = {}
            for line in rec.grn_line_ids:
                if line.sku_type == 'serilized' and line.serial_no == False:
                    raise ValidationError("You must have to enter serial no for serialized sku!")
                manf_date = None
                if line.manf_date:
                    manf_date = line.manf_date
                expiry_date = None
                if line.expiry_date:
                    expiry_date = line.expiry_date
                receiving_date = None
                if line.receiving_date:
                    receiving_date = line.receiving_date.date()
                key = (str(line.sku_id.id)+str(line.batch_no)+str(manf_date)+str(expiry_date)+str(line.bussines_unit_id.id)+str(receiving_date))
                if key in data_dict:
                    values = {
                        'product_uom_qty': data_dict[key].get('lines').append(line),
                    }
                    data_dict[key].update(values)
                else:
                    data_dict[key] = {
                        'key': key,
                        'lines': [line],
                    }

            for d in data_dict:
                lot_sequence = self.env['ir.sequence'].next_by_code('seq.wms.lot.no')
                current_date = fields.date.today()
                lot_seq_with_date = str(current_date.year) + str(current_date.month) + str(current_date.day) + lot_sequence
                # pdb.set_trace()
                transactions = self.env['wms.transactions']
                for line in data_dict[d].get('lines'):
                    line.write({'lot':lot_seq_with_date})
                    if line.serial_no and line.sku_id.sku_type == 'serilized':
                        # if line.recieved_qty != 0:
                        vals = {
                            'quantity': 1,
                            'remarks': line.remarks,
                            'lot1': line.lot1,
                            'lot2': line.lot2,
                            'lot3': line.lot3,
                            'lot4': line.lot4,
                            'lot5': line.lot5,
                            'lot6': line.lot6.id,
                            'lot7': line.lot7.id,
                            'lot8': line.lot8,
                            'lot9': line.lot9,
                            'lot10': line.lot10,
                            'serial_no': line.serial_no,
                            'description': line.description,
                            'uom_id': line.uom_id.id,
                            'sku_id': line.sku_id.id,
                            'pack_key_id': line.pack_key_id.id,
                            'bussines_unit_id': line.bussines_unit_id.id,
                            'batch_no': line.batch_no or False,
                            'manf_date': line.manf_date,
                            'expiry_date': line.expiry_date,
                            'lot': line.lot,
                            'location': line.location.id,
                            'transaction_type': 'grn',
                            'document_no': line.grn_id.name,
                            'customer_id': line.grn_id.customer_id.id,
                            'date': line.grn_id.grn_datetime.date(),
                            'warehouse_id': line.grn_id.warehouse_id.id,
                        }
                        transaction = self.env['wms.transactions'].create(vals)
                        inventory_report_vals = {
                            'customer_id': line.grn_id.customer_id.id,
                            'sku_id': line.sku_id.id,
                            'location': line.location.id,
                            'available_qty': 1,
                            'description': line.description,
                            'uom_id': line.uom_id.id,
                            'pack_key_id': line.pack_key_id.id,
                            'serial_no': line.serial_no or False,
                            'lot': line.lot,
                            'bussines_unit_id': line.bussines_unit_id.id,
                            # 'batch_no': line.batch_id.batch_no or False,
                            'remarks': line.remarks,
                            'lot1': line.lot1,
                            'lot2': line.lot2,
                            'lot3': line.lot3,
                            'lot4': line.lot4,
                            'lot5': line.lot5,
                            'lot6': line.lot6.id,
                            'lot7': line.lot7.id,
                            'lot8': line.lot8,
                            'lot9': line.lot9,
                            'lot10': line.lot10,
                            # 'batch_id': line.batch_id.id or False,
                            # 'serial_no_id': line.id or False,
                            'manf_date': line.manf_date,
                            'expiry_date': line.expiry_date,
                            'grn_line_id': line.id,
                            # 'serial_line_id': serial._origin.id,
                            'warehouse_id': line.grn_id.warehouse_id.id,
                            'grn_id': line.grn_id.id,
                            'asn_id': line.grn_id.asn_id.id,
                        }
                        inventory_report = self.env['inventory.report'].create(inventory_report_vals)
                    else:
                        # if line.recieved_qty != 0:
                        vals = {
                            'quantity': line.recieved_qty,
                            'remarks': line.remarks,
                            'lot1': line.lot1,
                            'lot2': line.lot2,
                            'lot3': line.lot3,
                            'lot4': line.lot4,
                            'lot5': line.lot5,
                            'lot6': line.lot6.id,
                            'lot7': line.lot7.id,
                            'lot8': line.lot8,
                            'lot9': line.lot9,
                            'lot10': line.lot10,
                            'description': line.description,
                            'uom_id': line.uom_id.id,
                            'sku_id': line.sku_id.id,
                            'pack_key_id': line.pack_key_id.id,
                            'bussines_unit_id': line.bussines_unit_id.id,
                            'batch_no': line.batch_no,
                            'manf_date': line.manf_date,
                            'expiry_date': line.expiry_date,
                            'lot': line.lot,
                            'location': line.location.id,
                            'transaction_type': 'grn',
                            'document_no': line.grn_id.name,
                            'customer_id': line.grn_id.customer_id.id,
                            'warehouse_id': line.grn_id.warehouse_id.id,
                            'date': line.grn_id.grn_datetime.date(),
                        }
                        inventory_report_vals = {
                            'warehouse_id': line.grn_id.warehouse_id.id,
                            'customer_id': line.grn_id.customer_id.id,
                            'sku_id': line.sku_id.id,
                            'location': line.location.id,
                            'available_qty': line.recieved_qty,
                            'description': line.description,
                            'uom_id': line.uom_id.id,
                            'pack_key_id': line.pack_key_id.id,
                            # 'serial_no': line.serial_no.name or False,
                            'lot': line.lot,
                            'bussines_unit_id': line.bussines_unit_id.id,
                            'batch_no': line.batch_no or False,
                            'remarks': line.remarks,
                            'lot1': line.lot1,
                            'lot2': line.lot2,
                            'lot3': line.lot3,
                            'lot4': line.lot4,
                            'lot5': line.lot5,
                            'lot6': line.lot6.id,
                            'lot7': line.lot7.id,
                            'lot8': line.lot8,
                            'lot9': line.lot9,
                            'lot10': line.lot10,
                            # 'batch_id': line.id or False,
                            # 'serial_no_id': line.serial_no.id or False,
                            'manf_date': line.manf_date,
                            'expiry_date': line.expiry_date,
                            'grn_line_id': line.id,
                            # 'batch_line_id': batch.id,
                            'warehouse_id': line.grn_id.warehouse_id.id,
                            'grn_id': line.grn_id.id,
                            'asn_id': line.grn_id.asn_id.id,
                        }
                        inventory_report = self.env['inventory.report'].create(inventory_report_vals)
                        transaction = self.env['wms.transactions'].create(vals)


    def create_putaway(self):
        for rec in self:
            putaway_vals = {
                'grn_id': rec._origin.id,
                'asn_id': rec.asn_id.id,
                'grn_datetime': rec.grn_datetime,
                'customer_id': rec.customer_id.id,
                'vendor_id': rec.vendor_id.id,
                'asn_datetime': rec.asn_datetime,
                'asn_type': rec.asn_type,
                'external_ref': rec.external_ref,
                'po_number': rec.po_number,
                'warehouse_id': rec.warehouse_id.id,
                # 'labour_id': rec.labour_id.id,
            }
            putaway = self.env['wms.putaway'].create(putaway_vals)
            for line in rec.grn_line_ids:

                if line.serial_no and line.serlized_bo:

                    if line.recieved_qty != 0:

                        # serial_splict = str(serial.id).split("virtual_")
                        # print(serial_splict)
                        # if len(serial_splict) > 1:
                        #     print(serial_splict[1])
                        #     serial_last = serial_splict[1].split("'")
                        #     print(serial_last[0])
                        serial_putaway_quantity = self.env['putaway.quantity'].search([('name','=',1)],limit=1)
                        if not serial_putaway_quantity:
                            serial_putaway_quantity = self.env['putaway.quantity'].create({'name':1})
                        line_vals = {
                            'sku_id': line.sku_id.id,
                            'batch_no': line.batch_no,
                            'serial_no': line.serial_no,
                            'bussines_unit_id': line.bussines_unit_id.id,
                            'recieved_qty': 1,
                            'wms_putaway_id': putaway.id,
                            'grn_line_id': line.id,
                            'putaway_quantity': 1,
                            'putaway_locations_no': 1,
                        }
                        putaway_lines = self.env['wms.putaway.lines'].create(line_vals)
                    # pdb.set_trace()
                else:
                    if line.recieved_qty != 0:
                        line_vals = {
                            'sku_id': line.sku_id.id,
                            'batch_no': line.batch_no,
                            # 'batch_id': batch.id,
                            # 'serial_no': serial.name,
                            'bussines_unit_id': line.bussines_unit_id.id,
                            'recieved_qty': line.recieved_qty,
                            'wms_putaway_id': putaway.id,
                            'grn_line_id': line.id,
                        }
                        putaway_lines = self.env['wms.putaway.lines'].create(line_vals)






    # def closed_grn(self):
    #     for rec in self:
    #         rec.state = 'close'
    #         rec.asn_id.state = 'close'
    #         lot_sequence = self.env['ir.sequence'].next_by_code('seq.wms.lot.no')
    #         current_date = fields.date.today()
    #         # lot_seq_with_date = str(current_date.year)+'-'+str(current_date.month)+'-'+str(current_date.day)+'-'+lot_sequence
    #         lot_seq_with_date = str(current_date.year) + str(current_date.month) + str(current_date.day) + lot_sequence
    #         for line in rec.grn_line_ids:
    #             line.lot = lot_seq_with_date
    #             line.asn_line_id.recieved_qty = line.recieved_qty


    def cancel_grn(self):
        for rec in self:
            rec.state = 'cancel'
            rec.asn_id.state = 'cancel_grn'



    @api.onchange('asn_id')
    def _onchange_asn(self):
        line_lst=[]
        serial_number_lst = []
        self.external_ref = self.asn_id.ext_ref
        self.po_number = self.asn_id.po_number
        self.asn_datetime = self.asn_id.asn_datetime
        self.customer_id = self.asn_id.customer_id_id.id
        self.vendor_id = self.asn_id.vendor_id.id
        self.asn_type = self.asn_id.asn_type
        for grn_line in self.grn_line_ids:
            grn_line.unlink()

        for line in self.asn_id.asn_line_ids:
            line_lst.append((0, 0, {
                'sku_id': line.sku_id.id,
                'description': line.description,
                'uom_id': line.uom_id.id,
                'pack_key_id': line.pack_key_id.id,
                'batch_no': line.batch_no,
                'manf_date': line.manf_date,
                'expiry_date': line.expiry_date,
                'bussines_unit_id': line.bussines_unit_id.id,
                'remarks': line.remarks,
                'expected_qty': line.expected_qty,
                # 'serial_no':serial_number_lst,
                'asn_line_id': line.id
                 }))
            for serial in line.lsc_serial_number:
                vals = {
                    'name': serial.name,
                    'grn_line_id': self.id,
                }
                self.env['lsc.grn.serial'].create(vals)

        self.grn_line_ids = line_lst

    @api.model
    def create(self, vals):
        if 'asn_type' in vals:
            if vals.get('asn_type') == 'purchase':
                vals['name'] = self.env['ir.sequence'].next_by_code('grn.seq.purchase')
            elif vals.get('asn_type') == 'cust_return':
                vals['name'] = self.env['ir.sequence'].next_by_code('grn.seq.cust_return')
        return super(GrnWms, self).create(vals)


    def unlink(self):
        # state_description_values = {elem[0]: elem[1] for elem in self._fields['state']._description_selection(self.env)}
        # for grn in self.filtered(lambda holiday: holiday.state not in ['draft', 'cancel', 'confirm']):
        #     raise UserError(('You cannot delete an allocation request which is in %s state.') % (state_description_values.get(grn.state),))
        for rec in self:
            putaway = self.env["wms.putaway"].search([('grn_id','=',rec.id)])

            if putaway:
                if putaway.state == 'validate':
                    raise ValidationError('You Can not delete Grn which have Validated Putaway!')
                else:
                    raise ValidationError('You Can not delete Grn which have Putaway! You have to delete it first!')
        for grn in self:
            grn.asn_id.state = 'new'
            grn.grn_line_ids.unlink()
        return super(GrnWms, self).unlink()



class GrnLines(models.Model):
    _name = 'lsc.grn.lines'
    _description = "GRN Lines"

    def _get_default_locaiton(self):
        print(self.grn_id.warehouse_id.location.location_name)
        if self.grn_id.warehouse_id.location.id:
            return self.grn_id.warehouse_id.location.id

    grn_id = fields.Many2one('lsc.grn','rel',ondelete='cascade',)
    state = fields.Selection(
        [('new', 'New'), ('in_receiving', 'In Receiving'), ('partial_received', 'Partial Received'), ('received', 'Received'),
         ('close', 'Close'), ('cancel', 'Cancel')], string='Status',compute='compute_line_status')
    line_no = fields.Integer("S.NO",compute='_compute_get_number')
    sku_id = fields.Many2one('lsc.sku.master',"SKU")
    description = fields.Char(related='sku_id.description',store=True)
    uom_id = fields.Many2one('uom.uom', 'UOM')
    pack_key_id = fields.Many2one('lsc.pack.key', 'Pack Key')
    batch_no = fields.Char("Batch No")
    serial_no = fields.Char("Serial Number")
    # serial_no = fields.One2many('lsc.grn.serial','grn_line_id',ondelete='cascade',store=True, index=True,string="Serial Number",readonly=False)
    manf_date = fields.Date("Manufacturing Date")
    expiry_date = fields.Date("Expiry Date")
    bussines_unit_id = fields.Many2one('lsc.bussines.unit', string="Business Unit")
    balanced_qty = fields.Float('Balanced QTY',compute='get_balanced_quantity',store=True)
    expected_qty = fields.Float('Expected QTY')
    recieved_qty = fields.Float('Received QTY',store=True)
    remarks = fields.Char("Remarks")
    lot = fields.Char("Lot No")
    receiving_date = fields.Datetime(related='grn_id.receiving_date',store=True)
    asn_line_id = fields.Many2one('lsc.asn.lines',string='ASN Line ID')
    location = fields.Many2one(related='grn_id.warehouse_id.location',string='Location')
    # for_compute_serial = fields.Char(string='for_compute_serial',compute='for_compute_serial_compute')
    serlized_bo = fields.Boolean("Serlized Boolean", compute='compute_serialized_bo')
    # batch_line_ids = fields.One2many('batch.grn.lines','grn_line_id',string='Batch Lines',ondelete='cascade')
    # for_compute_received_qty = fields.Char(compute='get_compute_received_qty')
    # batch_expected_qty = fields.Float(compute='get_batch_expected_qty',store=True)
    bussiness_unit_mandatory = fields.Boolean(related='grn_id.customer_id.business_unit_mandatory', store=True)
    lot1 = fields.Char("Lot01")
    lot2 = fields.Char('Lot02')
    lot3 = fields.Char('Lot03')
    lot4 = fields.Char('Lot04')
    lot5 = fields.Char('Lot05')
    customer_id = fields.Many2one(related='grn_id.customer_id',
                                  domain="[('customer_rank','>', 0),('wms','=',True),('id','in',current_user_customers)]",
                                  string='Customer', store=True)
    lot7_ids = fields.Many2many('sku.lotable', string='Lot07', compute='get_lot_seven_ids')
    lot6_ids = fields.Many2many('customer.lotable.six', string='Lot06', compute='get_lot_six_ids')
    lot6 = fields.Many2one('sku.lotable.six',string='Lot06',domain="[('id','in',lot6_ids)]")
    lot7 = fields.Many2one('sku.lotable', string='Lot07',domain="[('id','in',lot7_ids)]")
    lot8 = fields.Datetime('Lot08')
    lot9 = fields.Datetime('Lot09')
    lot10 = fields.Datetime('Lot10')
    sku_type = fields.Selection(related='sku_id.sku_type',string="Sky Type",store=True)
    allocation_strategy = fields.Selection(related='sku_id.allocation_startegy',string="Allocation Strategy")
    # scan_serial = fields.Char(string='Scan Serial',readonly=False)
    # compute_scan_serial_no = fields.Char(string='compute scan serial',compute='get_compute_scan_serial_no')

    @api.depends('customer_id')
    def get_lot_seven_ids(self):
        for rec in self:
            lot7_ids = self.env['customer.lottable.seven'].search([('customer_id', '=', rec.customer_id.id)],
                                                                  limit=1).lottable_seven_ids.mapped('id')
            if lot7_ids:
                rec.lot7_ids = lot7_ids
            else:
                rec.lot7_ids = False

    @api.depends('customer_id')
    def get_lot_six_ids(self):
        for rec in self:
            lot6_ids = self.env['customer.lotable.six'].search([('customer_id', '=', rec.customer_id.id)],
                                                               limit=1).lottable_six_ids.mapped('id')
            if lot6_ids:
                rec.lot6_ids = lot6_ids
            else:
                rec.lot6_ids = False

    _sql_constraints = [('grn_sku_serial_unique', 'UNIQUE (sku_id,serial_no)',
                         'Duplicate Serial number with respect to sku in the GRN!')]

    # @api.model
    # def create(self, vals):
    #     # if 'sku_id' in vals:
    #     #     if vals['sku_id'].serilized == True:
    #     if 'serial_no' in vals and 'sku_id' in vals:
    #         if vals['serial_no'] != '':
    #             check_unique_serial_line = self.env['lsc.grn.lines'].search([('sku_id','=',vals['sku_id']),('serial_no','=',vals['serial_no'])])
    #             if check_unique_serial_line:
    #                 raise ValidationError(_('Serial No is already exists with respect to sku!.'))
    #     res = super(GrnLines, self).create(vals)
    #     return res

    # @api.depends('scan_serial')
    # def get_compute_scan_serial_no(self):
    #     for rec in self:
    #         if rec.scan_serial != False:
    #             vals = {
    #                 'grn_line_id': rec.id,
    #                 'name': rec.scan_serial,
    #             }
    #             # serial_line = self.env['lsc.grn.serial'].create(vals)
    #             rec.serial_no = [(0, 0, vals)]
    #             rec.compute_scan_serial_no = 'abc'
    #             rec.scan_serial = False
    #             return {'auto_focus': {'scan_serial': 1}}
    #         else:
    #             rec.compute_scan_serial_no = 'abc'
    #             return {'auto_focus': {'scan_serial': 1}}
    #     return {'auto_focus': {'scan_serial': 1}}

    # @api.depends('batch_line_ids.expected_qty')
    # def get_batch_expected_qty(self):
    #     for rec in self:
    #         rec.batch_expected_qty = sum(rec.batch_line_ids.mapped('expected_qty'))
    #         if rec.batch_expected_qty > rec.expected_qty:
    #             raise ValidationError(
    #                                   _('Expected Quantity can not be greater than Total Expected Quantity!.'))

    # @api.depends('batch_line_ids','batch_line_ids.recieved_qty','serial_no','serial_no.recieved_qty')
    # def get_compute_received_qty(self):
    #     for rec in self:
    #         rec.for_compute_received_qty = 'abcd'
    #         if rec.sku_id.serilized == False:
    #             rec.recieved_qty = sum(rec.batch_line_ids.mapped('recieved_qty'))
    #         if rec.sku_id.serilized == True and any(rec.serial_no.filtered(lambda b: b.recieved_qty < 0 or b.recieved_qty>1)):
    #             raise ValidationError('Received Quantity can be 0 or 1 only!')
    #         elif rec.sku_id.serilized == True and not any(rec.serial_no.filtered(lambda b: b.recieved_qty < 0 or b.recieved_qty>1)):
    #             rec.recieved_qty = sum(rec.serial_no.mapped('recieved_qty'))


    def _update_line_quantity(self,old,new,field_name):
        for line in self:
            msg = "<b>" + _("The GRN line has been updated.") + "</b><ul>"
            msg += "<li>SKU %s: <br/>" % line.sku_id.code
            msg += _(
                "%(field_name)s: %(old_qty)s -> %(new_qty)s",
                field_name=field_name,
                old_qty=old,
                new_qty=new
            ) + "<br/>"
            # if line.product_id.type in ('consu', 'product'):
            #     msg += _("Delivered Quantity: %s", line.qty_delivered) + "<br/>"
            # msg += _("Invoiced Quantity: %s", line.qty_invoiced) + "<br/>"
        msg += "</ul>"
        self.grn_id.message_post(body=msg)

    def write(self, values):
        previous_received_quantity = self.recieved_qty
        previous_uom_id = self.uom_id
        previous_description = self.description
        previous_pack_key_id = self.pack_key_id
        previous_serial_no = self.serial_no
        previous_expiry_date = self.expiry_date
        previous_manf_date = self.manf_date
        previous_bussines_unit_id = self.bussines_unit_id



        result = super(GrnLines, self).write(values)


        # if not any(self.grn_id.grn_line_ids.filtered(lambda b: b.recieved_qty != b.expected_qty)):
        #     if self.grn_id.state != 'received':
        #         # pdb.set_trace()
        #         self.grn_id.write({'state': 'received'})
        #         print(self.grn_id.state)
        #         self.grn_id.asn_id.state = 'received'
        #         self.grn_id.in_receiving_grn()

        if self.recieved_qty != previous_received_quantity:
            if 'recieved_qty' in values:
                self._update_line_quantity(old=previous_received_quantity,new=self.recieved_qty,field_name=self._fields['recieved_qty'].string)
        if self.uom_id != previous_uom_id:
            if 'uom_id' in values:
                self._update_line_quantity(old=previous_uom_id.name,new=self.uom_id.name,field_name=self._fields['uom_id'].string)
        if self.description != previous_description:
            if 'description' in values:
                self._update_line_quantity(old=previous_description,new=self.description,field_name=self._fields['description'].string)
        if self.pack_key_id != previous_pack_key_id:
            if 'pack_key_id' in values:
                self._update_line_quantity(old=previous_pack_key_id,new=self.pack_key_id,field_name=self._fields['pack_key_id'].string)
        if self.serial_no != previous_serial_no:
            if 'serial_no' in values:
                self._update_line_quantity(old=previous_serial_no,new=self.serial_no,field_name=self._fields['serial_no'].string)
        if self.expiry_date != previous_expiry_date:
            if 'expiry_date' in values:
                self._update_line_quantity(old=previous_expiry_date,new=self.expiry_date,field_name=self._fields['expiry_date'].string)
        if self.manf_date != previous_manf_date:
            if 'manf_date' in values:
                self._update_line_quantity(old=previous_manf_date,new=self.manf_date,field_name=self._fields['manf_date'].string)
        if self.bussines_unit_id != previous_bussines_unit_id:
            if 'bussines_unit_id' in values:
                self._update_line_quantity(old=previous_bussines_unit_id.name,new=self.bussines_unit_id.name,field_name=self._fields['bussines_unit_id'].string)
        # if self.lot1 != previous_lot1:
        #     if 'lot1' in values:
        #         self._update_line_quantity(old=previous_lot1,new=self.lot1,field_name=self._fields['lot1'].string)
        # if self.lot2 != previous_lot2:
        #     if 'lot2' in values:
        #         self._update_line_quantity(old=previous_lot2,new=self.lot2,field_name=self._fields['lot2'].string)
        # if self.lot3 != previous_lot3:
        #             if 'lot3' in values:
        #                 self._update_line_quantity(old=previous_lot3,new=self.lot3,field_name=self._fields['lot3'].string)
        # if self.lot4 != previous_lot4:
        #             if 'lot4' in values:
        #                 self._update_line_quantity(old=previous_lot4,new=self.lot4,field_name=self._fields['lot4'].string)
        # if self.lot5 != previous_lot5:
        #             if 'lot5' in values:
        #                 self._update_line_quantity(old=previous_lot5,new=self.lot5,field_name=self._fields['lot5'].string)
        # if self.lot6 != previous_lot6:
        #                     if 'lot6' in values:
        #                         self._update_line_quantity(old=previous_lot6,new=self.lot6,field_name=self._fields['lot6'].string)
        # if self.lot7 != previous_lot7:
        #                     if 'lot7' in values:
        #                         self._update_line_quantity(old=previous_lot7,new=self.lot7,field_name=self._fields['lot7'].string)
        # if self.lot8 != previous_lot8:
        #                     if 'lot8' in values:
        #                         self._update_line_quantity(old=previous_lot8,new=self.lot8,field_name=self._fields['lot8'].string)
        # if self.lot9 != previous_lot9:
        #                     if 'lot9' in values:
        #                         self._update_line_quantity(old=previous_lot9,new=self.lot9,field_name=self._fields['lot9'].string)
        # if self.lot10 != previous_lot10:
        #                     if 'lot10' in values:
        #                         self._update_line_quantity(old=previous_lot10,new=self.lot10,field_name=self._fields['lot10'].string)

        return result


    @api.depends('sku_id', 'sku_id.sku_type')
    def compute_serialized_bo(self):
        for rec in self:
            if rec.sku_id.sku_type == 'serilized':
                rec.serlized_bo = True
            else:
                rec.serlized_bo = False

    # @api.depends('recieved_qty')
    # def get_serial_no_compute(self):
    #     for rec in self:
    #         # rec._origin.serial_no.unlink()
    #         vals_list = []
    #         if rec.serlized_bo == True:
    #             if len(rec.serial_no) == rec._origin.recieved_qty:
    #                 rec.serial_no = rec.serial_no.ids
    #             elif rec.serial_no and len(rec.serial_no) < rec._origin.recieved_qty:
    #                 difference_lenth = abs(len(rec.serial_no) - rec._origin.recieved_qty)
    #                 length = difference_lenth
    #                 serials = self.env['lsc.grn.serial']
    #                 for i in range(int(length)):
    #                     vals = {
    #                         'grn_line_id': rec._origin.id,
    #                     }
    #                     vals_list.append((0, 0, vals))
    #                     serial = self.env['lsc.grn.serial'].create(vals)
    #                     serials += serial
    #                     # for r in rec.serial_no:
    #                     #     serials += r
    #                     # pdb.set_trace()
    #                 rec.serial_no = rec.serial_no.ids + serials.ids
    #             elif not rec.serial_no:
    #                 serials = self.env['lsc.grn.serial']
    #                 for i in range(int(rec._origin.recieved_qty)):
    #                     vals = {
    #                         'grn_line_id': rec._origin.id,
    #                     }
    #                     vals_list.append((0, 0, vals))
    #                     serial = self.env['lsc.grn.serial'].create(vals)
    #                     serials += serial
    #                 # rec._origin.serial_no = vals_list or False
    #                 # rec._origin.serial_no = [(6, 0, serials.ids)]
    #                 rec.serial_no = serials.ids
    #             else:
    #                 rec.serial_no = rec.serial_no.ids
    #         else:
    #             rec.serial_no = False



    @api.onchange('serial_no')
    def _onchange_serial_received(self):
        for rec in self:
            rec.recieved_qty = 1
            # if len(rec.serial_no) > rec.recieved_qty:
            #     raise ValidationError(
            #         _('Serial numbers can not be more than received quantity!.'))


    # @api.constrains('expected_qty','recieved_qty')
    # def _check_received(self):
    #     for rec in self:
    #         if rec.recieved_qty > rec.expected_qty:
    #             raise ValidationError(
    #                 _('Received Quantity can not be greater than expected Quantity!.'))
    # @api.constrains('expected_qty','batch_expected_qty')
    # def _check_batch_expected(self):
    #     for rec in self:
    #         if rec.expected_qty < rec.batch_expected_qty:
    #             raise ValidationError(
    #                 _('Expected Quantity can not be greater than Total Expected Quantity!.'))

    @api.depends('recieved_qty','balanced_qty','expected_qty')
    def compute_line_status(self):
        for rec in self:
            if rec.grn_id.state == 'new':
                rec.state = 'new'
                rec.asn_line_id.state = 'new'
            elif rec.grn_id.state == 'in_receiving' and rec.recieved_qty == 0:
                rec.state = 'in_receiving'
                rec.asn_line_id.state = 'in_receiving'
            elif rec.grn_id.state == 'in_receiving' and rec.recieved_qty > 0 and rec.recieved_qty != rec.expected_qty:
                rec.state = 'partial_received'
                rec.asn_line_id.state = 'partial_received'
            elif rec.recieved_qty == rec.expected_qty:
                rec.state = 'received'
                rec.asn_line_id.state = 'received'
            elif rec.grn_id.state == 'close' and rec.recieved_qty != rec.expected_qty:
                rec.state = 'close'
                rec.asn_line_id.state = 'close'
            else:
                rec.state = False
                rec.asn_line_id.state = False

    # @api.depends('sku_id','batch_no','manf_date','expiry_date','bussines_unit_id','receiving_date')
    # def assign_lot_no(self):
    #     for rec in self:
    #         # lot no = SKU + Batch + manufacturing date + Expiry Date + Business Unit + Receiving Date
    #         lot = None
    #         if rec.sku_id and rec.sku_id.code:
    #             lot = str(rec.sku_id.code)
    #         if rec.batch_no:
    #             lot += rec.batch_no
    #         if rec.manf_date:
    #             lot += str(rec.manf_date)
    #         if rec.bussines_unit_id and rec.bussines_unit_id.name:
    #             lot += rec.bussines_unit_id.name
    #         if rec.expiry_date:
    #             lot += str(rec.expiry_date)
    #         if rec.receiving_date:
    #             lot += str(rec.receiving_date)
    #
    #         # rec.sku_id.code or '' + rec.batch_no + str(rec.manf_date) + str(rec.expiry_date) + str(rec.receiving_date)
    #         rec.lot = lot or False
    # vals['doc_name_seq'] = self.env['ir.sequence'].next_by_code('seq.wms.lot.no') or _('New')

    @api.depends('expected_qty','recieved_qty')
    def get_balanced_quantity(self):
        for rec in self:
            if rec.expected_qty - rec.recieved_qty < 0:
                raise ValidationError('Received Quantity can not be greater than expected Quantity!')
            rec.balanced_qty = rec.expected_qty - rec.recieved_qty

    @api.onchange('recieved_qty','remarks','lot1','lot2','lot3','lot4','lot5','lot6','lot7','lot8','lot9','lot10',
                  'uom_id','sku_id','pack_key_id','bussines_unit_id','batch_no','manf_date','expiry_date')
    def save_line_by_line_grn(self):
        for rec in self:
            rec._origin.write({
                'recieved_qty': rec.recieved_qty,
                'remarks': rec.remarks,
                # 'lot1': rec.lot1,
                # 'lot2': rec.lot2,
                # 'lot3': rec.lot3,
                # 'lot4': rec.lot4,
                # 'lot5': rec.lot5,
                # 'lot6': rec.lot6,
                # 'lot7': rec.lot7,
                # 'lot8': rec.lot8,
                # 'lot9': rec.lot9,
                # 'lot10': rec.lot10,
                'uom_id': rec.uom_id,
                'sku_id': rec.sku_id,
                'pack_key_id': rec.pack_key_id,
                'bussines_unit_id': rec.bussines_unit_id,
                'batch_no': rec.batch_no,
                'manf_date': rec.manf_date,
                'expiry_date': rec.expiry_date,
            })

            # rec._origin.asn_line_id.write({
            #     'recieved_qty': rec.recieved_qty,
            #     'remarks': rec.remarks,
            #     'lot1': rec.lot1,
            #     'lot2': rec.lot2,
            #     'lot3': rec.lot3,
            #     'lot4': rec.lot4,
            #     'lot5': rec.lot5,
            #     'lot6': rec.lot6,
            #     'lot7': rec.lot7,
            #     'lot8': rec.lot8,
            #     'lot9': rec.lot9,
            #     'lot10': rec.lot10,
            #     'uom_id': rec.uom_id,
            #     'sku_id': rec.sku_id,
            #     'pack_key_id': rec.pack_key_id,
            #     'bussines_unit_id': rec.bussines_unit_id,
            #     'batch_no': rec.batch_no,
            #     'manf_date': rec.manf_date,
            #     'expiry_date': rec.expiry_date,
            # })


    @api.depends('grn_id')
    def _compute_get_number(self):
        for order in self.mapped('grn_id'):
            number = 1
            for line in order.grn_line_ids:
                line.line_no = number
                number += 1

    @api.onchange('sku_id')
    def _onchage_sku(self):
        self.description = self.sku_id.description
        self.pack_key_id = self.sku_id.pack_key.id
        self.uom_id = self.sku_id.uom_id.id
        self._origin.write({
            'sku_id': self.sku_id,
        })

        self._origin.asn_line_id.write({
            'sku_id': self.sku_id,
        })


class BatchGrnLines(models.Model):
    _name = 'batch.grn.lines'
    _description = "Batch GRN Lines"

    expected_qty = fields.Float('Expected QTY')
    recieved_qty = fields.Float('Received QTY')
    balanced_qty = fields.Float('Balanced QTY',compute='get_batch_blnc_qty', store=True)
    batch_no = fields.Char("Batch No")
    grn_line_id = fields.Many2one('lsc.grn.lines', 'Rel',ondelete='cascade',)
    lot1 = fields.Char("Lot01")
    lot2 = fields.Char('Lot02')
    lot3 = fields.Char('Lot03')
    lot4 = fields.Char('Lot04')
    lot5 = fields.Char('Lot05')
    lot6 = fields.Many2one('sku.lotable.six',string='Lot06')
    lot7 = fields.Many2one('sku.lotable',string='Lot07')
    lot8 = fields.Datetime('Lot08')
    lot9 = fields.Datetime('Lot09')
    lot10 = fields.Datetime('Lot10')
    sku_id = fields.Many2one(related='grn_line_id.sku_id', string="SKU", store=True)
    sku_type = fields.Selection(related='sku_id.sku_type',string="Sky Type",store=True)

    @api.depends('expected_qty','recieved_qty')
    def get_batch_blnc_qty(self):
        for rec in self:
            rec.balanced_qty = rec.expected_qty-rec.recieved_qty

    # @api.depends('expected_qty', 'recieved_qty')
    # def get_balanced_quantity(self):
    #     for rec in self:
    #         received_qty_sum = sum(rec.grn_line_id.batch_line_ids.mapped('recieved_qty'))
    #         rec.balanced_qty = rec.expected_qty - received_qty_sum

class GrnSerial(models.Model):
    _name = 'lsc.grn.serial'
    _description = 'lsc.grn.serial'

    grn_line_id = fields.Many2one('lsc.grn.lines','Rel',ondelete='cascade',)
    expected_qty = fields.Integer('Expected QTY', default='1')
    recieved_qty = fields.Integer('Received QTY', default='1')
    name = fields.Char("Serial No",required=True)
    lot1 = fields.Char("Lot01")
    lot2 = fields.Char('Lot02')
    lot3 = fields.Char('Lot03')
    lot4 = fields.Char('Lot04')
    lot5 = fields.Char('Lot05')
    lot6 = fields.Many2one('sku.lotable.six',string='Lot06')
    sku_id = fields.Many2one(related='grn_line_id.sku_id', string="SKU", store=True)
    lot7 = fields.Many2one('sku.lotable',string='Lot07')
    lot8 = fields.Datetime('Lot08')
    lot9 = fields.Datetime('Lot09')
    lot10 = fields.Datetime('Lot10')


    # _sql_constraints = [('grn_uniq_ser', 'unique (name)',
    #                      'Duplicate Serial number in the ASN')]

    @api.constrains('recieved_qty')
    def contraint_On_serial(self):
        for rec in self:
            if rec.recieved_qty < 0 or rec.recieved_qty>1:
                raise ValidationError('Received Quantity can be 0 or 1 only!')


    @api.onchange('name')
    def grn_serial_name(self):
        for rec in self:
            rec._origin.write({
                'name': rec.name,
            })