from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError

# class user_roles(models.Model):
#     _name = 'user.roles'
#     _inherit = ['mail.thread', 'mail.activity.mixin', 'mail.render.mixin']
#     _description = "user_roles"
#
#     user_id = fields.Many2one('res.users',string='User',required=1)
#     customer_ids = fields.Many2many('res.partner', domain="[('customer_rank','>', 0),('wms','=',True)]",
#                                      string='Customer', tracking=True)
#     customer_all = fields.Boolean(string='Customer All?')
#     warehouse_ids = fields.Many2many('lsc.warehouse.master',string='Warehouse')
#     warehouse_all = fields.Boolean(string='Warehouse All?')
#     sub_warehouse_ids = fields.Many2many('lsc.sub.warehouse',string='Sub warehouse')
#     sub_warehouse_all = fields.Boolean(string='Sub Warehouse All?')
#     rule_ids = fields.Many2many(string='Rules')


    # @api.depends()
    # def customize_record_rule(self):
    #     for rec in self:
    #     domain = str([('customer_id_id', 'in', rec.customer_id.mapped(ids))])
    #     rule = self.env['ir.rule'].create({
    #         'name': 'Wms Asn Record Rule',
    #         'model_id': self.env['ir.model'].search([('model', '=', 'lsc.asn')]).id,
    #         'domain_force': "['|', ('x_studio_company_id', '=', False), ('x_studio_company_id', 'in', company_ids)]"
    #     })

class ResUser(models.Model):
    _inherit = 'res.users'

    customer_ids = fields.Many2many('res.partner', domain="[('customer_rank','>', 0),('wms','=',True)]",
                                   string='Customer', tracking=True)
    customer_all = fields.Boolean(string='Customer All?')
    warehouse_ids = fields.Many2many('lsc.warehouse.master', string='Warehouse')
    warehouse_all = fields.Boolean(string='Warehouse All?')
    sub_warehouse_ids = fields.Many2many('lsc.sub.warehouse', string='Sub warehouse')
    sub_warehouse_all = fields.Boolean(string='Sub Warehouse All?')
    rule_ids = fields.Many2many(string='Rules')


    @api.onchange('customer_all')
    def onchange_customer_all(self):
        if self.customer_all == True:
            self.customer_ids = self.env['res.partner'].search([('customer_rank','>', 0),('wms','=',True)]).ids
            print(self.customer_all,"Is True")
            print(self.customer_ids.ids)
        if self.customer_all == False:
            print(self.customer_all, "Is False")
            self.customer_ids = False

    @api.onchange('warehouse_all')
    def onchange_warehouse_all(self):
        if self.warehouse_all == True:
            self.warehouse_ids = self.env['lsc.warehouse.master'].search([]).ids
        if self.warehouse_all == False:
            self.warehouse_ids = False

    @api.onchange('sub_warehouse_all')
    def onchange_sub_warehouse_all(self):
        if self.sub_warehouse_all == True:
            self.sub_warehouse_ids = self.env['lsc.sub.warehouse'].search([]).ids
        if self.sub_warehouse_all == False:
            self.sub_warehouse_ids = False
