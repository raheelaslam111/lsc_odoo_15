# -*- coding: utf-8 -*-
import pdb

from odoo import models, fields, api,_
from odoo.exceptions import UserError, ValidationError


class customer_lotable_six(models.Model):
    _name = 'customer.lotable.six'
    _description = 'customer lotable six'

    customer_id = fields.Many2one('res.partner',string="WMS Customer",required=1,domain="[('customer_rank','>',0),('wms','=',True)]",tracking=True)
    lottable_six_ids = fields.Many2many('sku.lotable.six', string="Lottable Six",required=1)

# class wms_lottables_six(models.Model):
#     _name = 'wms.lottables.six'
#     _description = 'wms_lottables six'
#
#     name = fields.Char(string='name')



class customer_lottable_seven(models.Model):
    _name = 'customer.lottable.seven'
    _description = 'customer.lottable.seven'

    customer_id = fields.Many2one('res.partner',string="WMS Customer",required=1,domain="[('customer_rank','>',0),('wms','=',True)]",tracking=True)
    lottable_seven_ids = fields.Many2many('sku.lotable', string="Lottable Seven",required=1)

# class wms_lottables_seven(models.Model):
#     _name = 'wms.lottables.seven'
#     _description = 'wms_lottables seven'
#
#     name = fields.Char(string='name')




