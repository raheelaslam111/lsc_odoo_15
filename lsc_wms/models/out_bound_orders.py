from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError
import pdb


class OutboundOrder(models.Model):
    _name = 'lsc.outbound.orders'
    _inherit = ['mail.thread', 'mail.activity.mixin', 'mail.render.mixin']
    _description = "Outbound orders"

    name = fields.Char("Order ID")
    order_type = fields.Selection([
        ('delivery', 'Delivery'),
        ('vendor_return', 'Vendor Return'),
        ('stock_trnasfer', 'Stock Transfer')], string="Order Type", required=1)
    external_ref = fields.Char("External Ref")
    invoice_number = fields.Char("Invoice Number")
    order_date_time = fields.Datetime("Order Date time", track_visibility='onchange', default=fields.Datetime.now())
    customer_id = fields.Many2one('res.partner', "Customer", track_visibility='onchange',
                                  domain="[('partner_type','=','customer'),('wms','=',True)]")
    supplier_id = fields.Many2one('res.partner', "Supplier/Vendor", track_visibility='onchange',
                                  domain="[('customer_id','=',customer_id)]")
    remarks = fields.Text("Remarks", track_visibility='onchange', )
    udf1 = fields.Char("Out-UDF1", track_visibility='onchange', )
    udf2 = fields.Char("Out-UDF2", track_visibility='onchange', )
    udf3 = fields.Char("Out-UDF3", track_visibility='onchange', )
    order_ids = fields.One2many('lsc.order.line', 'order_id', string="Order Lines")
    state = fields.Selection([('created', 'Created'),
                              ('allocated_partially', 'Part Allocated'),
                              ('allocated', 'Allocated'),
                              ('picked', 'Picked'), ('part_picked', 'Part Picked'), ('shipped', 'Shipped'),
                              ('shipped_part', 'Part Shipped '),
                              ('closed', 'Closed'), ('cancel', 'Cancel')], track_visibility='onchange', string='state',
                             default='created')

    allocation_id = fields.Many2one('lsc.allocation', string="Allocation Ref",copy=False)
    warehouse_id = fields.Many2one('lsc.sub.warehouse', store=True, string='Warehouse',required=True)
    comments = fields.Char('Cancel Comment')
    cancel_bo = fields.Boolean('Cancel bo', compute='_cancel_compute', store=True, copy=False)
    line_bo = fields.Boolean("Line bo", compute='_compute_lines', )

    @api.depends('order_ids')
    def _compute_lines(self):
        for rec in self:
            if any(disptch_line.state == 'draft' or disptch_line.state == 'par_allocate' for disptch_line in
                   rec.order_ids):
                rec.line_bo = False
            else:
                rec.line_bo = True

    @api.depends('allocation_id')
    def _cancel_compute(self):
        for rec in self:
            if rec.allocation_id:
                if rec.allocation_id.state == 'cancel':
                    rec.cancel_bo = True
                else:
                    rec.cancel_bo = False
            else:
                rec.cancel_bo = True

    def cancel_wizard(self):
        return {
            'name': 'Cancel Comment',
            'type': 'ir.actions.act_window',
            'res_model': 'lsc.cancel.comments',
            'view_mode': 'form',
            'view_id': False,
            'target': 'new'}

    def cancel(self):
        for line in self.order_ids:
            line.total_allocated = 0
            line.balance = line.qty
        self.state = 'cancel'

    # @api.constrains('order_ids')
    # def check_prodcut_line(self):
    #     """for restriction purpose"""
    #     product_list = []
    #
    #     for data in self.order:
    #         if not data.product_id in product_list:
    #             product_list.append(data.product_id)
    #         else:
    #             if data.product_id:
    #                 raise UserError(_("Duplicate products in Requst line not allowed !"))

    def action_allocation_open(self):
        self.ensure_one()
        action = self.env["ir.actions.actions"]._for_xml_id("lsc_wms.action_lsc_allocation")
        scraps = self.env['lsc.allocation'].search([('order_id', '=', self.id)])
        action['domain'] = [('id', 'in', scraps.ids)]
        return action

    def create_dispatch(self):
        bill_lst = []
        order_line = self.order_ids.filtered(
            lambda x: x.state in ('draft', 'par_allocate'))

        for line in order_line:
            if line.sku_id.sku_type != 'serilized' and line.serial_no:
                raise ValidationError(_("The Sku is not serialized! %s", line.code))
            if line.sku_id.sku_type == 'serilized':
                if line.serial_no:
                    print('serial no')

                    qty_hand = self.env['inventory.report'].search([
                        ('sku_id', '=', line.sku_id.id),
                        ('serial_no', '=', line.serial_no), ('warehouse_id', '=', self.warehouse_id.id),
                        ('location_type.location_status', '=', 'good'), ('bussines_unit_id', '=', line.bussines_id.id),
                        ('available_qty', '>', 0)])
                    if qty_hand:
                        if qty_hand.available_qty > 0:
                            line_lst = self.create_dispatch_line(line.sku_id, qty_hand, line.uom_id.id,
                                                                 qty_hand.location.id, line.description, line.qty,
                                                                 line.serial_no, line.bussines_id.id, qty_hand.lot,
                                                                 qty_hand.lot1, qty_hand.lot2,
                                                                 qty_hand.lot3, qty_hand.lot4, qty_hand.lot5,
                                                                 qty_hand.lot6,
                                                                 qty_hand.lot7, qty_hand.lot8, qty_hand.lot9,
                                                                 qty_hand.lot10, qty_hand.batch_no, line.id)
                            bill_lst.append(line_lst)
                            qty_hand.allocated_qty = 1
                            qty_hand.available_qty = 0
                            msg = 'The allocation has been done form: <a href=# data-oe-model=lsc.outbound.orders data-oe-id=%d>%s</a>' % (
                                self.id, self.name)

                            qty_hand.message_post(body=msg)
                            line.state = 'allocated'
                            line._update_line_quantity(line.sku_id.code)





                if not line.serial_no:
                    if line.lot_no:
                        if line.sku_id.allocation_startegy == 'fifo':
                            qty_hand = self.env['inventory.report'].search([
                                ('sku_id', '=', line.sku_id.id), ('warehouse_id', '=', self.warehouse_id.id),
                                ('lot', '=', line.lot_no), ('location_type.location_status', '=', 'good'),
                                ('available_qty', '>', 0), ('bussines_unit_id', '=', line.bussines_id.id)],
                                order="id asc"
                            )
                        if line.sku_id.allocation_startegy == 'LEFO':
                            qty_hand = self.env['inventory.report'].search([
                                ('sku_id', '=', line.sku_id.id),
                                ('lot', '=', line.lot_no), ('warehouse_id', '=', self.warehouse_id.id),
                                ('available_qty', '>', 0), ('bussines_unit_id', '=', line.bussines_id.id),
                                ('location_type.location_status', '=', 'good')], order="id desc"
                            )

                        total_qty = sum(qty_hand.mapped('available_qty'))
                        bill_lst += (self.calculate_qty(total_qty, line, qty_hand))


                    if not line.lot_no:
                        bill_lst += self.fifo_lefo(line, batch_bo=False, serilized_bo=True)
            if line.batch_bo == True:
                if line.batch_number:
                    if line.sku_id.allocation_startegy == 'fifo':
                        qty_hand = self.env['inventory.report'].search([
                            ('sku_id', '=', line.sku_id.id),
                            ('batch_no', '=', line.batch_number), ('warehouse_id', '=', self.warehouse_id.id),
                            ('location_type.location_status', '=', 'good'),
                            ('bussines_unit_id', '=', line.bussines_id.id), ('available_qty', '>', 0.0),
                        ], order="id asc"
                        )
                        bill_lst += self.batch_lefo_fifo(line, qty_hand)
                        print(qty_hand, "FIF0")
                    elif line.sku_id.allocation_startegy == 'LEFO':
                        qty_hand = self.env['inventory.report'].search([
                            ('sku_id', '=', line.sku_id.id),
                            ('batch_no', '=', line.batch_number), ('warehouse_id', '=', self.warehouse_id.id),
                            ('location_type.location_status', '=', 'good'), ('available_qty', '>', 0.0),
                            ('bussines_unit_id', '=', line.bussines_id.id)], order="id desc"
                        )
                        print(qty_hand, "QTY")
                        bill_lst += self.batch_lefo_fifo(line, qty_hand)
                elif line.lot_no and not line.batch_number:
                    if line.sku_id.allocation_startegy == 'fifo':
                        qty_hand = self.env['inventory.report'].search([
                            ('sku_id', '=', line.sku_id.id),
                            ('lot', '=', line.lot_no), ('warehouse_id', '=', self.warehouse_id.id),
                            ('location_type.location_status', '=', 'good'),
                            ('bussines_unit_id', '=', line.bussines_id.id), ('available_qty', '>', 0.0),
                        ], order="id asc"
                        )
                        bill_lst += self.batch_lefo_fifo(line, qty_hand)

                    elif line.sku_id.allocation_startegy == 'LEFO':
                        qty_hand = self.env['inventory.report'].search([
                            ('sku_id', '=', line.sku_id.id),
                            ('lot', '=', line.lot_no), ('warehouse_id', '=', self.warehouse_id.id),
                            ('location_type.location_status', '=', 'good'), ('available_qty', '>', 0.0),
                            ('bussines_unit_id', '=', line.bussines_id.id)], order="id desc"
                        )
                        bill_lst += self.batch_lefo_fifo(line, qty_hand)
                elif not line.lot_no and not line.batch_number:
                    bill_lst += self.fifo_lefo(line, batch_bo=True, serilized_bo=False)
            else:
                if line.lot_no:
                    print(line.sku_id.allocation_startegy)
                    if line.sku_id.allocation_startegy == 'fifo':
                        qty_hand = self.env['inventory.report'].search([
                            ('sku_id', '=', line.sku_id.id),
                            ('lot', '=', line.lot_no), ('warehouse_id', '=', self.warehouse_id.id),
                            ('location_type.location_status', '=', 'good'),
                            ('bussines_unit_id', '=', line.bussines_id.id), ('available_qty', '>', 0.0),
                        ], order="id asc"
                        )
                        bill_lst += self.batch_lefo_fifo(line, qty_hand)

                    elif line.sku_id.allocation_startegy == 'LEFO':
                        qty_hand = self.env['inventory.report'].search([
                            ('sku_id', '=', line.sku_id.id),
                            ('lot', '=', line.lot_no), ('warehouse_id', '=', self.warehouse_id.id),
                            ('location_type.location_status', '=', 'good'),
                            ('available_qty', '>', 0.0),
                            ('bussines_unit_id', '=', line.bussines_id.id)], order="id desc"
                        )
                        bill_lst += self.batch_lefo_fifo(line, qty_hand)
                else:
                    bill_lst += self.fifo_lefo(line, batch_bo=False, serilized_bo=False)


        if not self.allocation_id or self.allocation_id.state == 'cancel':
            # self.create_bill(bill_lst)
            if bill_lst:
                dispatch = self.env['lsc.allocation'].create({
                    'order_type': self.order_type,
                    # 'dispatch_date': fields.Date.today(),
                    'order_id': self.id,
                    'external_ref': self.external_ref,
                    'invoice_number': self.invoice_number,
                    'order_date_time': self.order_date_time,
                    'customer_id': self.customer_id.id,
                    'supplier_id': self.supplier_id.id,
                    'remarks': self.remarks,
                    # 'udf1': self.udf1,
                    # 'udf2': self.udf2,
                    # 'udf3': self.udf3,

                    'order_ids': bill_lst
                })
                self.allocation_id = dispatch.id
        else:
            self.allocation_id.order_ids = bill_lst
        if any(disptch_line.state == 'draft' or disptch_line.state == 'par_allocate' for disptch_line in
               self.order_ids):
            self.state = 'allocated_partially'
            self.allocation_id.state = 'allocated_partially'
            self.allocation_id.readonly_button = False
        else:
            self.state = 'allocated'
            self.allocation_id.state = 'allocated'
            self.allocation_id.readonly_button = False

    def fifo_lefo(self, line, batch_bo, serilized_bo):
        bill_lst = []
        if line.sku_id.allocation_startegy == 'fifo':
            if line.lot1 and line.lot2 and line.lot3:

                qty_hand_lot = self.env['inventory.report'].search([
                    ('sku_id', '=', line.sku_id.id),
                    ('lot1', '=', line.lot1), ('warehouse_id', '=', self.warehouse_id.id), ('lot2', '=', line.lot2),
                    ('lot3', '=', line.lot3),
                    ('available_qty', '>', 0), ('location_type.location_status', '=', 'good'),
                    ('bussines_unit_id', '=', line.bussines_id.id)], order="id asc")
                if serilized_bo == True:
                    total_qty = sum(qty_hand_lot.mapped('available_qty'))
                    bill_lst += (self.calculate_qty(total_qty, line, qty_hand_lot))
                elif batch_bo == True:
                    bill_lst += self.batch_lefo_fifo(line, qty_hand_lot)
                else:
                    bill_lst += self.batch_lefo_fifo(line, qty_hand_lot)

            elif line.lot1 and not line.lot2 and not line.lot3:
                qty_hand_lot = self.env['inventory.report'].search([
                    ('sku_id', '=', line.sku_id.id),
                    ('lot1', '=', line.lot1), ('warehouse_id', '=', self.warehouse_id.id),
                    ('location_type.location_status', '=', 'good'), ('lot2', '=', line.lot2), ('lot3', '=', line.lot3),
                    ('available_qty', '>', 0), ('bussines_unit_id', '=', line.bussines_id.id)], order="id asc")
                if serilized_bo == True:
                    total_qty = sum(qty_hand_lot.mapped('available_qty'))
                    bill_lst += (self.calculate_qty(total_qty, line, qty_hand_lot))
                elif batch_bo == True:
                    bill_lst += self.batch_lefo_fifo(line, qty_hand_lot)
                else:
                    bill_lst += self.batch_lefo_fifo(line, qty_hand_lot)


            elif line.lot1 and line.lot2 and not line.lot3:
                qty_hand_lot1_2 = self.env['inventory.report'].search([
                    ('sku_id', '=', line.sku_id.id),
                    ('lot1', '=', line.lot1), ('warehouse_id', '=', self.warehouse_id.id),
                    ('location_type.location_status', '=', 'good'), ('lot2', '=', line.lot2),
                    ('available_qty', '>', 0), ('bussines_unit_id', '=', line.bussines_id.id)], order="id asc")
                if serilized_bo == True:
                    total_qty = sum(qty_hand_lot1_2.mapped('available_qty'))
                    bill_lst += (self.calculate_qty(total_qty, line, qty_hand_lot1_2))
                elif batch_bo == True:
                    bill_lst += self.batch_lefo_fifo(line, qty_hand_lot1_2)
                else:
                    bill_lst += self.batch_lefo_fifo(line, qty_hand_lot1_2)


            elif line.lot1 and line.lot3 and not line.lot2:
                qty_hand_lot1_3 = self.env['inventory.report'].search([
                    ('sku_id', '=', line.sku_id.id), ('warehouse_id', '=', self.warehouse_id.id),
                    ('lot1', '=', line.lot1), ('lot3', '=', line.lot3),
                    ('available_qty', '>', 0), ('location_type.location_status', '=', 'good'),
                    ('bussines_unit_id', '=', line.bussines_id.id)], order="id asc")
                if serilized_bo == True:
                    total_qty = sum(qty_hand_lot1_3.mapped('available_qty'))
                    bill_lst += (self.calculate_qty(total_qty, line, qty_hand_lot1_3))
                elif batch_bo == True:
                    bill_lst += self.batch_lefo_fifo(line, qty_hand_lot1_3)
                else:
                    bill_lst += self.batch_lefo_fifo(line, qty_hand_lot1_3)

            elif line.lot2 and not line.lot1 and not line.lot3:
                qty_hand_lot_2 = self.env['inventory.report'].search([
                    ('sku_id', '=', line.sku_id.id), ('warehouse_id', '=', self.warehouse_id.id),
                    ('lot2', '=', line.lot2),
                    ('available_qty', '>', 0), ('location_type.location_status', '=', 'good'),
                    ('bussines_unit_id', '=', line.bussines_id.id)], order="id asc")
                if serilized_bo == True:
                    total_qty = sum(qty_hand_lot_2.mapped('available_qty'))
                    bill_lst += (self.calculate_qty(total_qty, line, qty_hand_lot_2))
                elif batch_bo == True:
                    bill_lst += self.batch_lefo_fifo(line, qty_hand_lot_2)
                else:
                    bill_lst += self.batch_lefo_fifo(line, qty_hand_lot_2)

            elif line.lot2 and line.lot3 and not line.lot1:
                qty_hand_lot_2_3 = self.env['inventory.report'].search([
                    ('sku_id', '=', line.sku_id.id),
                    ('lot2', '=', line.lot2), ('warehouse_id', '=', self.warehouse_id.id), ('lot3', '=', line.lot3),
                    ('available_qty', '>', 0), ('location_type.location_status', '=', 'good'),
                    ('bussines_unit_id', '=', line.bussines_id.id)], order="id asc")
                if serilized_bo == True:
                    total_qty = sum(qty_hand_lot_2_3.mapped('available_qty'))
                    bill_lst += (self.calculate_qty(total_qty, line, qty_hand_lot_2_3))
                elif batch_bo == True:
                    bill_lst += self.batch_lefo_fifo(line, qty_hand_lot_2_3)
                else:
                    bill_lst += self.batch_lefo_fifo(line, qty_hand_lot_2_3)
            elif line.lot3 and not line.lot1 and not line.lot2:
                qty_hand_lot_3 = self.env['inventory.report'].search([
                    ('sku_id', '=', line.sku_id.id), ('warehouse_id', '=', self.warehouse_id.id),
                    ('lot3', '=', line.lot3),
                    ('available_qty', '>', 0), ('location_type.location_status', '=', 'good'),
                    ('bussines_unit_id', '=', line.bussines_id.id)], order="id asc")
                if serilized_bo == True:
                    total_qty = sum(qty_hand_lot_3.mapped('available_qty'))
                    bill_lst += (self.calculate_qty(total_qty, line, qty_hand_lot_3))
                elif batch_bo == True:
                    bill_lst += self.batch_lefo_fifo(line, qty_hand_lot_3)
                else:
                    bill_lst += self.batch_lefo_fifo(line, qty_hand_lot_3)


            else:
                qty_hand_lot_all = self.env['inventory.report'].search([
                    ('sku_id', '=', line.sku_id.id), ('warehouse_id', '=', self.warehouse_id.id),

                    ('available_qty', '>', 0), ('location_type.location_status', '=', 'good'),
                    ('bussines_unit_id', '=', line.bussines_id.id)], order="id asc")
                if serilized_bo == True:
                    total_qty = sum(qty_hand_lot_all.mapped('available_qty'))
                    bill_lst += (self.calculate_qty(total_qty, line, qty_hand_lot_all))
                elif batch_bo == True:
                    bill_lst += self.batch_lefo_fifo(line, qty_hand_lot_all)
                else:
                    bill_lst += self.batch_lefo_fifo(line, qty_hand_lot_all)

        elif line.sku_id.allocation_startegy == 'LEFO':
            if line.lot1 and line.lot2 and line.lot3:

                qty_hand_lot = self.env['inventory.report'].search([
                    ('sku_id', '=', line.sku_id.id), ('warehouse_id', '=', self.warehouse_id.id),
                    ('lot1', '=', line.lot1), ('lot2', '=', line.lot2), ('lot3', '=', line.lot3),
                    ('available_qty', '>', 0), ('location_type.location_status', '=', 'good'),
                    ('bussines_unit_id', '=', line.bussines_id.id)], order="id desc")
                if serilized_bo == True:
                    total_qty = sum(qty_hand_lot.mapped('available_qty'))
                    bill_lst += (self.calculate_qty(total_qty, line, qty_hand_lot))
                elif batch_bo == True:
                    bill_lst += self.batch_lefo_fifo(line, qty_hand_lot)
                else:
                    bill_lst += self.batch_lefo_fifo(line, qty_hand_lot)
            elif line.lot1 and not line.lot2 and not line.lot3:
                qty_hand_lot1_lot2 = self.env['inventory.report'].search([
                    ('sku_id', '=', line.sku_id.id), ('warehouse_id', '=', self.warehouse_id.id),
                    ('lot1', '=', line.lot1), ('location_type.location_status', '=', 'good'), ('lot2', '=', line.lot2),
                    ('lot3', '=', line.lot3),
                    ('available_qty', '>', 0), ('bussines_unit_id', '=', line.bussines_id.id)], order="id desc")
                if serilized_bo == True:
                    total_qty = sum(qty_hand_lot1_lot2.mapped('available_qty'))
                    bill_lst += (self.calculate_qty(total_qty, line, qty_hand_lot1_lot2))
                elif batch_bo == True:
                    bill_lst += self.batch_lefo_fifo(line, qty_hand_lot1_lot2)
                else:
                    bill_lst += self.batch_lefo_fifo(line, qty_hand_lot1_lot2)
            elif line.lot1 and line.lot2 and not line.lot3:
                qty_hand_lot1_2 = self.env['inventory.report'].search([
                    ('sku_id', '=', line.sku_id.id), ('warehouse_id', '=', self.warehouse_id.id),
                    ('lot1', '=', line.lot1), ('lot2', '=', line.lot2),
                    ('available_qty', '>', 0), ('location_type.location_status', '=', 'good'),
                    ('bussines_unit_id', '=', line.bussines_id.id)], order="id desc")
                if serilized_bo == True:
                    total_qty = sum(qty_hand_lot1_2.mapped('available_qty'))
                    bill_lst += (self.calculate_qty(total_qty, line, qty_hand_lot1_2))
                elif batch_bo == True:
                    bill_lst += self.batch_lefo_fifo(line, qty_hand_lot1_2)
                else:
                    bill_lst += self.batch_lefo_fifo(line, qty_hand_lot1_2)
            elif line.lot1 and line.lot3 and not line.lot2:
                qty_hand_lot1_3 = self.env['inventory.report'].search([
                    ('sku_id', '=', line.sku_id.id), ('warehouse_id', '=', self.warehouse_id.id),
                    ('lot1', '=', line.lot1), ('location_type.location_status', '=', 'good'), ('lot3', '=', line.lot3),
                    ('available_qty', '>', 0), ('bussines_unit_id', '=', line.bussines_id.id)], order="id desc")
                if serilized_bo == True:
                    total_qty = sum(qty_hand_lot1_3.mapped('available_qty'))
                    bill_lst += (self.calculate_qty(total_qty, line, qty_hand_lot1_3))
                elif batch_bo == True:
                    bill_lst += self.batch_lefo_fifo(line, qty_hand_lot1_3)
                else:
                    bill_lst += self.batch_lefo_fifo(line, qty_hand_lot1_3)
            elif line.lot2 and not line.lot1 and not line.lot3:
                qty_hand_lot_2 = self.env['inventory.report'].search([
                    ('sku_id', '=', line.sku_id.id), ('warehouse_id', '=', self.warehouse_id.id),
                    ('lot2', '=', line.lot2),
                    ('available_qty', '>', 0), ('location_type.location_status', '=', 'good'),
                    ('bussines_unit_id', '=', line.bussines_id.id)], order="id desc")
                if serilized_bo == True:
                    total_qty = sum(qty_hand_lot_2.mapped('available_qty'))
                    bill_lst += (self.calculate_qty(total_qty, line, qty_hand_lot_2))
                elif batch_bo == True:
                    bill_lst += self.batch_lefo_fifo(line, qty_hand_lot_2)
                else:
                    bill_lst += self.batch_lefo_fifo(line, qty_hand_lot_2)

            elif line.lot2 and line.lot3 and not line.lot1:
                qty_hand_lot_2_3 = self.env['inventory.report'].search([
                    ('sku_id', '=', line.sku_id.id), ('warehouse_id', '=', self.warehouse_id.id),
                    ('lot2', '=', line.lot2), ('lot3', '=', line.lot3),
                    ('available_qty', '>', 0), ('location_type.location_status', '=', 'good'),
                    ('bussines_unit_id', '=', line.bussines_id.id)], order="id desc")
                if serilized_bo == True:
                    total_qty = sum(qty_hand_lot_2_3.mapped('available_qty'))
                    bill_lst += (self.calculate_qty(total_qty, line, qty_hand_lot_2_3))
                elif batch_bo == True:
                    bill_lst += self.batch_lefo_fifo(line, qty_hand_lot_2_3)
                else:
                    bill_lst += self.batch_lefo_fifo(line, qty_hand_lot_2_3)
            elif line.lot3 and not line.lot1 and not line.lot2:
                qty_hand_lot_3 = self.env['inventory.report'].search([
                    ('sku_id', '=', line.sku_id.id), ('warehouse_id', '=', self.warehouse_id.id),
                    ('lot3', '=', line.lot3),
                    ('available_qty', '>', 0), ('location_type.location_status', '=', 'good'),
                    ('bussines_unit_id', '=', line.bussines_id.id)], order="id desc")
                if serilized_bo == True:
                    total_qty = sum(qty_hand_lot_3.mapped('available_qty'))
                    bill_lst += (self.calculate_qty(total_qty, line, qty_hand_lot_3))
                elif batch_bo == True:
                    bill_lst += self.batch_lefo_fifo(line, qty_hand_lot_3)
                else:
                    bill_lst += self.batch_lefo_fifo(line, qty_hand_lot_3)
            else:
                qty_hand_lot_all_lefo = self.env['inventory.report'].search([
                    ('sku_id', '=', line.sku_id.id), ('warehouse_id', '=', self.warehouse_id.id),

                    ('available_qty', '>', 0), ('location_type.location_status', '=', 'good'),
                    ('bussines_unit_id', '=', line.bussines_id.id)], order="id desc")
                if serilized_bo == True:
                    total_qty = sum(qty_hand_lot_all_lefo.mapped('available_qty'))
                    bill_lst += (self.calculate_qty(total_qty, line, qty_hand_lot_all_lefo))
                elif batch_bo == True:
                    bill_lst += self.batch_lefo_fifo(line, qty_hand_lot_all_lefo)
                else:
                    bill_lst += self.batch_lefo_fifo(line, qty_hand_lot_all_lefo)
        return bill_lst

    def batch_lefo_fifo(self, line, qty_hand):
        line_bal = line.balance
        # 110
        print(line_bal, "Line Bale")
        bill_lst = []
        # 110
        # 100
        for qty in qty_hand:

            remaining_qty = 0.0
            if line_bal > 0.0:
                if qty.available_qty >= line_bal:
                    #     100=100
                    print("Greater QTY Called")
                    # 110+0=110
                    line.total_allocated += line_bal
                    # 100+10
                    remaining_qty = qty.available_qty - line_bal
                    # 100-100=0
                    # 110-110
                    qty.available_qty = remaining_qty
                    qty.allocated_qty += line_bal
                    # 0
                    # =0
                    # qty.available_qty = line.balance

                    line_lst = self.create_dispatch_line(line.sku_id, qty, line.uom_id.id, qty.location.id,
                                                         line.description, line_bal,
                                                         qty.serial_no, qty.bussines_unit_id.id, qty.lot,
                                                         qty.lot1, qty.lot2,
                                                         qty.lot3, qty.lot4, qty.lot5,
                                                         qty.lot6,
                                                         qty.lot7, qty.lot8, qty.lot9,
                                                         qty.lot10, qty.batch_no, line.id)
                    msg = 'The allocation has been done form: <a href=# data-oe-model=lsc.outbound.orders data-oe-id=%d>%s</a>' % (
                        self.id, self.name)
                    line_bal = 0.0
                    qty.message_post(body=msg)
                    bill_lst.append(line_lst)
                    # line.total_allocated+=line.balance

                elif qty.available_qty < line_bal:
                    # 10<110
                    print("Less QTY Called")
                    available = qty.available_qty
                    remaining_qty = line_bal - qty.available_qty
                    # =110-10=100
                    line.total_allocated += qty.available_qty
                    # 0+10=10
                    #  40=0.0
                    # 30=0.0
                    qty.available_qty = 0.0
                    qty.allocated_qty += available
                    # 10
                    line_bal = remaining_qty
                    #    =100
                    line_lst = self.create_dispatch_line(line.sku_id, qty, line.uom_id.id, qty.location.id,
                                                         line.description, available,
                                                         qty.serial_no, qty.bussines_unit_id.id, qty.lot,
                                                         qty.lot1, qty.lot2,
                                                         qty.lot3, qty.lot4, qty.lot5,
                                                         qty.lot6,
                                                         qty.lot7, qty.lot8, qty.lot9,
                                                         qty.lot10, qty.batch_no, line.id)

                    msg = 'The allocation has been done form: <a href=# data-oe-model=lsc.outbound.orders data-oe-id=%d>%s</a>' % (
                        self.id, self.name)

                    qty.message_post(body=msg)
                    bill_lst.append(line_lst)
        line.balance = line_bal
        if line.balance == 0.0:
            line.state = 'allocated'
        elif line.balance == line_bal:
            line.state = 'draft'
        else:
            line.state = 'par_allocated'
        return bill_lst

    def calculate_qty(self, total_qty, line, qty_hand):
        bill_lst = []
        if total_qty > 0:
            if total_qty < line.balance:
                line.total_allocated = total_qty
                line.balance = line.balance - total_qty
                for qty in qty_hand:
                    qty.write({
                        'allocated_qty': 0,
                        'available_qty': 1
                    })
                    msg = 'The allocation has been done form: <a href=# data-oe-model=lsc.outbound.orders data-oe-id=%d>%s</a>' % (
                        self.id, self.name)
                    qty.message_post(body=msg)
                    line_lst = self.create_dispatch_line(line.sku_id, qty, line.uom_id.id, qty.location.id,
                                                         line.description,
                                                         1,
                                                         qty.serial_no, line.bussines_id.id, qty.lot,
                                                         qty.lot1, qty.lot2,
                                                         qty.lot3, qty.lot4, qty.lot5,
                                                         qty.lot6,
                                                         qty.lot7, qty.lot8, qty.lot9,
                                                         qty.lot10, qty.batch_no, line.id)
                    bill_lst.append(line_lst)
                line._update_line_quantity(line.sku_id.code)
                line.state = 'par_allocate'
            elif total_qty == line.balance:
                line.total_allocated = total_qty
                line.balance = 0
                for qty in qty_hand:
                    qty.write({
                        'allocated_qty': 1,
                        'available_qty': 0
                    })
                    msg = 'The allocation has been done form: <a href=# data-oe-model=lsc.outbound.orders data-oe-id=%d>%s</a>' % (
                        self.id, self.name)
                    qty.message_post(body=msg)
                    line_lst = self.create_dispatch_line(line.sku_id, qty, line.uom_id.id, qty.location.id,
                                                         line.description,
                                                         1,
                                                         qty.serial_no, line.bussines_id.id, qty.lot,
                                                         qty.lot1, qty.lot2,
                                                         qty.lot3, qty.lot4, qty.lot5,
                                                         qty.lot6,
                                                         qty.lot7, qty.lot8, qty.lot9,
                                                         qty.lot10, qty.batch_no, line.id)
                    bill_lst.append(line_lst)
                line._update_line_quantity(line.sku_id.code)
                line.state = 'allocated'
                # for i in range(0,)
            elif total_qty > line.balance:
                blnc = int(line.balance)
                on_hand_limit = qty_hand[:blnc]
                line.total_allocated = line.qty
                line.balance = 0
                # line.state = 'allocated'
                for qty_on in on_hand_limit:
                    line_lst = self.create_dispatch_line(line.sku_id, qty_on, line.uom_id.id, qty_on.location.id,
                                                         line.description,
                                                         1,
                                                         qty_on.serial_no, line.bussines_id.id, qty_on.lot,
                                                         qty_on.lot1, qty_on.lot2,
                                                         qty_on.lot3, qty_on.lot4, qty_on.lot5,
                                                         qty_on.lot6,
                                                         qty_on.lot7, qty_on.lot8, qty_on.lot9,
                                                         qty_on.lot10, qty_on.batch_no, line.id)
                    bill_lst.append(line_lst)
                    line._update_line_quantity(line.sku_id.code)
                    qty_on.write({
                        'allocated_qty': 1,
                        'available_qty': 0
                    })
                    msg = 'The allocation has been done form: <a href=# data-oe-model=lsc.outbound.orders data-oe-id=%d>%s</a>' % (
                        self.id, self.name)
                    qty_on.message_post(body=msg)
                line._update_line_quantity(line.sku_id.code)
                line.state = 'allocated'

        return bill_lst

    def create_dispatch_line(self, sku, qty_hand, uom, location, desc, qty, serial, bu,
                             lot, lot1, lot2, lot3, lot4, lot5,
                             lot6, lot7, lot8, lot9, lot10, batch_id, order_line):

        line_list = ((0, 0, {
            'sku_id': sku.id,
            'qty': qty,
            'serial_no': serial,
            'description': desc,
            'location': location,
            'bussines_id': bu,
            'uom_id': uom,
            'inv_report_id': qty_hand.ids,
            'batch_number': batch_id,
            'allocation_qty': 1.0,
            'balance':qty,
            # 'serial_no': serial,
            'order_line_id': order_line,
            'lot': lot,
            'lot1': lot1,
            'lot2': lot2,
            'lot3': lot3,
            'lot4': lot4,
            'lot5': lot5,
            'lot6': lot6.id,
            'lot7': lot7.id,
            'lot8': lot8,
            'lot9': lot9,
            'lot10': lot10,
            'order_id': self.id or False,
            'warehouse_id': self.warehouse_id.id,
        }))
        return line_list

    def create_bill(self, line_list):

        dispatch = self.env['lsc.allocation'].create({
            'order_type': self.order_type,
            # 'dispatch_date': fields.Date.today(),
            'order_id': self.id,
            'external_ref': self.external_ref,
            'invoice_number': self.invoice_number,
            'order_date_time': self.order_date_time,
            'customer_id': self.customer_id.id,
            'supplier_id': self.supplier_id.id,
            'remarks': self.remarks,
            # 'udf1': self.udf1,
            # 'udf2': self.udf2,
            # 'udf3': self.udf3,

            'order_ids': line_list

        })
        self.allocation_id = dispatch.id

    @api.model
    def create(self, vals):
        out_bound_seq = self.env['ir.sequence'].next_by_code('wms.out.seq')
        if vals['order_type'] == 'delivery':
            vals['name'] = 'OB-DE' + out_bound_seq
        if vals['order_type'] == 'vendor_return':
            vals['name'] = 'OB-VR' + out_bound_seq

        if vals['order_type'] == 'stock_trnasfer':
            vals['name'] = 'OB-ST' + out_bound_seq

        res = super(OutboundOrder, self).create(vals)
        return res

    @api.constrains('external_ref')
    def _contraints_on_ext_ref(self):
        for rec in self:
            if rec.customer_id:
                if rec.customer_id.unique_customer_reference == True:
                    if rec.external_ref:
                        other_ref = self.search([('external_ref', '=', rec.external_ref),
                                                 ('customer_id', '=', rec.customer_id.id), ('id', '!=', rec.id)])
                        if other_ref:
                            raise ValidationError("Same External Ref found against same customer")


class OrderLines(models.Model):
    _name = 'lsc.order.line'

    order_id = fields.Many2one('lsc.outbound.orders', "Rel", ondelete='cascade', )
    line_no = fields.Integer("Line no", compute='_compute_get_number')
    sku_id = fields.Many2one('lsc.sku.master', "SKU")
    description = fields.Char("Description")
    uom_id = fields.Many2one('uom.uom', "UOM")
    qty = fields.Float("Quantity")
    pack_key_id = fields.Many2one('lsc.pack.key', "Pack key")
    batch_number = fields.Char("Batch No")
    serilizad_bo = fields.Boolean("Serilized Bo")
    batch_bo = fields.Boolean("Batch BO")
    serial_no = fields.Char("Serial Number")
    bussines_id = fields.Many2one('lsc.bussines.unit', string="Business Unit")
    balance = fields.Float("Balance")
    remarks = fields.Char("Remarks")
    lot1 = fields.Char("Lottable01")
    lot2 = fields.Char('Lottable02')
    lot3 = fields.Char('Lottable03')
    lot4 = fields.Char('Lottable04')
    lot5 = fields.Char('Lottable05')
    lot6 = fields.Many2one('sku.lotable.six',string='Lottable06')
    lot7 = fields.Many2one('sku.lotable',string='Lot07')
    lot8 = fields.Datetime('Lottable08')
    lot9 = fields.Datetime('Lottable09')
    lot10 = fields.Datetime('Lottable10')
    lot_no = fields.Char("LOT#")
    state = fields.Selection(
        [('draft', 'Draft'), ('par_allocate', 'Part Allocated'), ('allocated', 'Allocated'),('picked','picked'),('part_picked','Part Picked'), ('shipped', 'Shipped'),
         ('part_shipped', 'Part shipped')],default='draft')
    bussines_bo  =fields.Boolean()
    total_allocated = fields.Float("Total Allocated")
    no_serial = fields.Boolean("serial no")
    location_id = fields.Many2one('lsc.location.master', "Location")

    @ api.onchange('serial_no')
    def onchange_serial(self):
        if self.serilizad_bo == True:
            if self.serial_no:
                self.no_serial = False
                self.qty = 1
            else:
                self.no_serial = True

    @api.onchange("qty", "total_allocated")
    def _balance(self):
        self.balance = self.qty - self.total_allocated

    @api.depends('order_id')
    def _compute_get_number(self):
        for order in self.mapped('order_id'):
            number = 1
            for line in order.order_ids:
                line.line_no = number
                number += 1

    @api.onchange('sku_id')
    def _onchang_sku(self):
        self.description = self.sku_id.description
        self.pack_key_id = self.sku_id.pack_key.id
        self.uom_id = self.sku_id.uom_id.id
        if self.serial_no:
            self.no_serial = False
        else:
            self.no_serial = True
        if self.sku_id.business_id:
            self.bussines_bo = True
        else:
            self.bussines_bo = False

        if self.sku_id.sku_type == 'serilized':
            print('serilized')
            self.serilizad_bo = True
            self.qty = 1.0
        if self.sku_id.sku_type == 'batch_wise':
            self.batch_bo = True
        if self.sku_id.business_id.ids:
            if len(self.sku_id.business_id.ids) == 1:
                self.bussines_id = self.sku_id.business_id[0].id
            else:
                self.bussines_id = False

        return {'domain': {'bussines_id': [('id', '=', self.sku_id.business_id.ids)]}}

    def _update_line_quantity(self, values):
        msg = "<b>" + _("The sku (%s) has been allocated with qty 1.") % (values) + "</b><ul>"
        msg += _(
            "state change from: draft -> allocated",
        ) + "<br/>"
        msg += "</ul>"
        self.order_id.message_post(body=msg)

    @api.constrains('sku_id', 'serial_no', 'bussines_id')
    def contraint_sku(self):
        for rec in self:
            sku_ids = self.search([('sku_id', '=', rec.sku_id.id), ('serial_no', '=', rec.serial_no),
                                   ('bussines_id', '=', rec.bussines_id.id),
                                   ('serilizad_bo', '=', True), ('id', '!=', rec.id),
                                   ('order_id', '=', rec.order_id.id)])
            print(sku_ids)
            codes = []
            for sk_name in sku_ids:
                if sk_name.sku_id.code not in codes:
                    codes.append(sk_name.sku_id.code)

            if len(sku_ids) > 0:
                raise ValidationError("one sku with same serial no and business id can be select one time sku ref")
