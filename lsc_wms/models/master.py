# -*- coding: utf-8 -*-
import pdb

from odoo import models, fields, api,_
from odoo.exceptions import Warning, UserError
from odoo.exceptions import UserError, ValidationError
import re
import json
import requests

class SiteMaster(models.Model):
    _name = 'lsc.site'

    _description = 'Site Master'
    _rec_name = 'name'
    name = fields.Char('Name')

class WarehouseMaster(models.Model):
    _name = 'lsc.warehouse.master'

    _description = 'Warehouse Master'
    _rec_name = 'name'
    code = fields.Char("ID")
    name = fields.Char("Name")
    address = fields.Char("Address")
    site_id = fields.Many2one('lsc.site',"Site")

class SubWareHouse(models.Model):
    _name = 'lsc.sub.warehouse'
    _rec_name = 'name'

    name = fields.Char("Name")
    warehouse_id = fields.Many2one('lsc.warehouse.master')
    location = fields.Many2one('lsc.location.master',string='Default Stage Location')



class ResPartner(models.Model):
    _inherit = 'res.partner'

    def _default_get_current_user_customer(self):
        if self.env.user.customer_ids:
            return self.env.user.customer_ids.ids

    warhouse_ids = fields.Many2many('lsc.sub.warehouse',string="Warehouse Mapping")
    customer_id = fields.Many2one('res.partner',domain="[('wms','=',True),('customer_rank','>', 0),('id','in', current_user_customers)]",string='Customer')
    selection_fields = fields.Selection([('man_date','Man Date + Shelf Life'),('expiry_date','Expiry Date')])
    wms = fields.Boolean("WMS")
    is_subcon = fields.Boolean("Is Subcon")
    is_mixed_sku = fields.Boolean("Mixed Sku?")
    business_unit_mandatory = fields.Boolean("Business Unit Mandatory?")
    unique_customer_reference = fields.Boolean("Unique Customer Reference")
    parent_customer = fields.Many2one('res.partner' ,domain="[('customer_rank','>', 0)]",string="Parent Customer")
    allocation_strategy = fields.Selection([('fifo', 'FIFO'), ('lifo', 'LIFO'), ('fefo', 'FEFO')],string='Allocation Strategy')
    partner_type = fields.Selection([('none', 'None'),('customer', 'Customer'), ('vendor', 'Vendor')],
                                           string='Partner Type')
    current_user_customers = fields.Many2many('res.partner', string='Current User Customer',default=_default_get_current_user_customer,
                                              compute='get_current_user_customer')

    def get_current_user_customer(self):
        for rec in self:
            if self.env.user.customer_ids:
                rec.current_user_customers = self.env.user.customer_ids.ids
            else:
                rec.current_user_customers = False


    def data_from_json(self,method,args):
        data = {
            'method': method,
            'input_type': 'JSON',
            'response_type': 'JSON',
            'rest_data': json.dumps(args)
        }
        res = requests.post('https://suitecrmdemo.dtbc.eu/service/v2/rest.php', data=data)
        return res.json()

    # def suitcrm(self):
    #
    #     data_auth= {'user_auth':{
    #         'user_name':'Demo',
    #         'password':'F0258B6685684C113BAD94D91B8FA02A'},
    #         'application_name':'My SuiteCRM Rest Client',
    #         'name_value_list':{}
    #     }
    #     session_id = self.data_from_json('login',data_auth)
    #     data_auth={
    #         'session':session_id['id'],
    #         'module_name':'Leads',
    #         'query':'1=1',
    #         'off_set':0,
    #         'order_by':'',
    #          'select fields':('phone_work','first_name','last_name'),
    #         'link_name_to_field_array':({
    #             'name':'contacts',
    #             'value':('phone_work','first_name','last_name')
    #         }),
    #         'max_results':10000,
    #         'deleted':0
    #     }
    #     data = self.data_from_json('get_entry_list',data_auth)
    #     entry_lst = data.get('entry_list')
    #     if isinstance(entry_lst,list):
    #         name_lst=[]
    #         for entry in entry_lst:
    #             name_lst.append(entry.get('name_value_list'))
    #         print(name_lst)
    #     emai_template = self.env.ref('lsc_wms.email_template_crm_contact')
    #     # tem_id = self.env['email.template'].browse(emai_template)
    #     context = self.env.context.copy()
    #     context.update({
    #         'data':name_lst
    #     })
    #     print(context)
    #     emai_template.with_context(context).send_mail(self.id,force_send=True)
    @api.onchange('vat')
    def vat_regex(self):
        """it give the format to Vat number"""
        if self.is_subcon==False:
            if (self.vat):
                result = re.match('^[0-9]?', self.vat, flags=0)
                if (len(self.vat) < 13):
                    raise UserError(_('Vat should be 13 digit numbers'))
                if (result):
                    print("Match")
                else:
                    print("Failed")
                    raise UserError(_('Invalid Vat Entry'))

class SkuMaster(models.Model):
    _name = 'lsc.sku.master'
    _rec_name = 'code'
    _inherit = ['mail.thread', 'mail.activity.mixin', 'mail.render.mixin']

    def _default_get_current_user_customer(self):
        if self.env.user.customer_ids:
            return self.env.user.customer_ids.ids

    code = fields.Char("Code",required=1,tracking=True)
    description = fields.Char("Description",required=1,tracking=True)
    image = fields.Binary("Image", tracking=True)
    uom_id = fields.Many2one('uom.uom',"Base UOM")
    cusomter_id = fields.Many2one('res.partner',string="WMS Customer",required=1,domain="[('customer_rank','>',0),('wms','=',True)]",tracking=True)
    business_unit_mandatory = fields.Boolean(related='cusomter_id.business_unit_mandatory',string="Business Unit Mandatory?")
    length = fields.Float("Length (cm)", tracking=True)
    width = fields.Float("Length (cm)", tracking=True)
    height = fields.Float("Height (cm)", tracking=True)
    shelf_life = fields.Integer('Shelf Life (Days)', tracking=True)
    weight = fields.Float("Weight (kg)", tracking=True)
    currency_id = fields.Many2one("res.currency","Currency",default=lambda self: self.env['res.currency'].search([('name','=','SAR')], limit=1))
    unit_price = fields.Float("Unit Price", tracking=True)
    pack_key = fields.Many2one('lsc.pack.key',"Pack key", tracking=True)
    sku_type = fields.Selection([('none', 'None'), ('serilized', 'Serialized'), ('batch_wise', 'Batch Wise')],default='none',string='Sku Type', tracking=True,required=1)
    serilized = fields.Boolean("Serilized")
    batch_wise = fields.Boolean("Batch Wise?")
    allocation_startegy = fields.Selection([('fifo','FIFO'),('LEFO','LIFO')],required=1)


    extra_field_ids = fields.One2many('lsc.extra.field','sku_id',ondelete='cascade', index=True, string="Extra Field")
    rotate_by = fields.Selection([('man_p_shelf','Man + Shelf life'),('expiry_date','Expiry Date')],tracking=True,required=1)
    business_id = fields.Many2many('lsc.bussines.unit',string="Business Unit", tracking=True)
    categ_id = fields.Many2one('lsc.sku.category',string='Category', tracking=True)
    sub_category = fields.Many2one('lsc.sku.subcategory',string="Sub Category", tracking=True)
    ischildsku = fields.Boolean("Is Child sku")
    parent_id = fields.Many2one('lsc.sku.master',"Parent Sku")
    active = fields.Boolean('active',default=True)
    qty_available_ids = fields.One2many('sku.qty.available','sku_id',string='Available Quantity')
    available_qty_sum = fields.Float(string='Available Quantity',compute='get_available_qty_total')
    # partner_id = fields.Many2one('res.partner', "WMS Customer", domain="[('wms','=',True)]")
    # sku_lotable_ids = fields.One2many('sku.lotable','sku_id',string='Sku Lottables', tracking=True)
    # sku_lotable_six_ids = fields.One2many('sku.lotable.six','sku_id',string='Lottable Six', tracking=True)
    stock_onhand_ids = fields.One2many('stock.onhand.general','sku_id',string='Stock Onhand',compute='get_stock_onhand_general')
    edit_state = fields.Selection([('edit_requested', 'Edit Requested')])

    changed_image = fields.Binary("Image")
    changed_shelf_life = fields.Integer('Shelf Life (Days)')
    changed_weight = fields.Float("Weight (kg)")
    changed_unit_price = fields.Float("Unit Price")
    changed_length = fields.Float("Length (cm)")
    changed_height = fields.Float("Height (cm)")
    changed_business_id = fields.Many2many('lsc.bussines.unit','sku_business_unit','business_unit_sku', string="Business Unit")
    changed_categ_id = fields.Many2one('lsc.sku.category', string='Category')
    changed_sub_category = fields.Many2one('lsc.sku.subcategory', string="Sub Category")
    changed_pack_key = fields.Many2one('lsc.pack.key', "Pack key")
    # changed_sku_lotable_ids = fields.One2many('sku.lotable', 'sku_id', string='Sku Lottables')
    edit_request_user = fields.Many2one('res.users',string='Edit Request User')
    current_user_customers = fields.Many2many('res.partner',string='Current User Customer',default=_default_get_current_user_customer,compute='get_current_user_customer')

    def get_current_user_customer(self):
        for rec in self:
            if self.env.user.customer_ids:
                rec.current_user_customers = self.env.user.customer_ids.ids
            else:
                rec.current_user_customers = False


    def edit_request(self):
        for rec in self:
            return {
                'name': 'Sku Edit Wizard',
                'type': 'ir.actions.act_window',
                'res_model': 'sku.edit.wizard',
                'view_mode': 'form',
                'view_id': self.env.ref('lsc_wms.sku_master_edit_wizard_form').id,
                'context': dict(
                    default_sku_id=self.id,
                    default_changed_image=self.image,
                    default_changed_shelf_life=self.shelf_life,
                    default_changed_weight=self.weight,
                    default_changed_unit_price=self.unit_price,
                    default_changed_length=self.length,
                    default_changed_height=self.height,
                    default_changed_business_id=self.business_id.ids,
                    default_changed_categ_id=self.categ_id.id,
                    default_changed_sub_category=self.sub_category.id,
                    default_changed_pack_key=self.pack_key.id,
                    # default_changed_sku_lotable_ids=self.sku_lotable_ids.ids,
                ),
                'target': 'new'}
            # users = self.env.ref('lsc_wms.group_inventory_manager').users
            # for user in users:
            #     activity_vals = {
            #         'activity_type_id': self.env.ref('mail.mail_activity_data_todo').id,
            #     }
            #     rec.activity_schedule(
            #         note='Please Review Sku Master for edit request!',
            #         summary='Request Edit for Sku',
            #         user_id=user.id, **activity_vals)
            # rec.edit_state = 'edit_requested'

    def edit_request_approve(self):
        for rec in self:
            vals = {

            }
            if rec.image != rec.changed_image:
                vals.update({'image': rec.changed_image})
            if rec.shelf_life != rec.changed_shelf_life:
                vals.update({'shelf_life': rec.changed_shelf_life})
            if rec.weight != rec.changed_weight:
                vals.update({'weight': rec.changed_weight})
            if rec.unit_price != rec.changed_unit_price:
                vals.update({'unit_price': rec.changed_unit_price})
            if rec.length != rec.changed_length:
                vals.update({'length': rec.changed_length})
            if rec.height != rec.changed_height:
                vals.update({'height': rec.changed_height})
            if rec.business_id != rec.changed_business_id:
                vals.update({'business_id': rec.changed_business_id})
            if rec.categ_id != rec.changed_categ_id:
                vals.update({'categ_id': rec.changed_categ_id})
            if rec.sub_category != rec.changed_sub_category:
                vals.update({'sub_category': rec.changed_sub_category})
            if rec.pack_key != rec.changed_pack_key:
                vals.update({'pack_key': rec.changed_pack_key})
            # if rec.sku_lotable_ids != rec.changed_sku_lotable_ids:
            #     vals.update({'sku_lotable_ids': rec.changed_sku_lotable_ids})
            rec.with_context(edit_sku_master_context=True).write(vals)
            rec.edit_state = False

            activity_vals = {
                'activity_type_id': self.env.ref('mail.mail_activity_data_todo').id,
            }
            rec.activity_schedule(
                note='Your changes is Approved Cross check the changes',
                summary='Changes is Approved',
                user_id=rec.edit_request_user.id, **activity_vals)

    def edit_request_cancel(self):
        for rec in self:
            rec.edit_state = False
            activity_vals = {
                'activity_type_id': self.env.ref('mail.mail_activity_data_todo').id,
            }
            rec.activity_schedule(
                note='Your changes is Not Approved Kindly contact with Inventory manager! if any issues. Thankyou.',
                summary='Changes is Not Approved',
                user_id=rec.edit_request_user.id, **activity_vals)


    def write(self, vals):
        current_business_id = self.business_id
        current_shelf_life = self.shelf_life
        current_weight = self.weight
        current_unit_price = self.unit_price
        current_categ_id = self.categ_id
        current_sub_category = self.sub_category
        current_width = self.width
        current_height = self.height
        current_cusomter_id = self.cusomter_id
        current_code = self.code
        current_edit_state = self.edit_state
        res = super(SkuMaster, self).write(vals)
        # pdb.set_trace()
        if not self.env.user.has_group('base.group_system'):
            if not self._context.get('edit_sku_master_context'):
                grn = self.env['lsc.grn.lines'].search([('sku_id','=',self.id)])
                if len(grn) > 0:
                    # raise ValidationError('You can not Change the SKU which have the Grn! you have to put edit request!')
                    for business_unit in current_business_id:
                        if business_unit.id not in self.business_id.ids:
                            raise ValidationError('You can not Change the business Unit of SKU which have the Grn!')
                    if current_business_id.ids not in self.business_id.ids:
                        raise ValidationError('You can not Change the business Unit of SKU which have the Grn!')
                    if current_shelf_life != self.shelf_life:
                        raise ValidationError('You can not Change the Shelf Unit of SKU which have the Grn!')
                    if current_weight != self.weight:
                        raise ValidationError('You can not Change the Weight of SKU which have the Grn!')
                    if current_unit_price != self.unit_price:
                        raise ValidationError('You can not Change the Unit Price of SKU which have the Grn!')
                    if current_categ_id != self.categ_id:
                        raise ValidationError('You can not Change the Category of SKU which have the Grn!')
                    if current_sub_category != self.sub_category:
                        raise ValidationError('You can not Change the Sub Category of SKU which have the Grn!')
                    if current_width != self.width:
                        raise ValidationError('You can not Change the Width of SKU which have the Grn!')
                    if current_height != self.height:
                        raise ValidationError('You can not Change the Height of SKU which have the Grn!')
                    if current_cusomter_id != self.cusomter_id:
                        raise ValidationError('You can not Change the Customer of SKU which have the Grn!')
                    if current_code != self.code:
                        raise ValidationError('You can not Change the Height of SKU which have the Grn!')
        # else:
        #     if self.edit_state == 'edit_process' and current_edit_state == 'edit_process':
        #         self.edit_state = False

        return res

    # def get_sku_stock_onhand(self):
    #     for rec in self:
    #         current_stock_onhand_id = self.env['stock.onhand.general'].search([()])

    def get_stock_onhand_general(self):
        for rec in self:
            onhand_genral_sku = self.env['stock.onhand.general']
            data_dict = {}
            stock_onhand = self.env['inventory.report'].search([('sku_id','=',rec.id)])
            previous_onhand_genral = self.env['stock.onhand.general'].search([('sku_id','=',rec.id)])
            previous_onhand_genral.unlink()
            for line in stock_onhand:
                key = (line.bussines_unit_id)
                if key in data_dict:
                    values = {
                        'sku_qty': data_dict[key].get('sku_qty')+line.available_qty,
                    }
                    data_dict[key].update(values)
                else:
                    data_dict[key] = {
                        'key': key,
                        'business_unit': line.bussines_unit_id,
                        'sku_qty': line.available_qty,
                        'uom': line.uom_id,
                        'description': line.description,
                    }
            for d in data_dict:
                data_dict[d].get('lines')
                vals = {
                    'sku_id': rec.id,
                    'available_qty':data_dict[d].get('sku_qty'),
                    'bussines_unit_id':data_dict[d].get('business_unit').id,
                    'uom_id':rec.uom_id.id or False,
                    'description':data_dict[d].get('description'),
                }
                onhand_genral = self.env['stock.onhand.general'].create(vals)
                onhand_genral_sku += onhand_genral
            rec.stock_onhand_ids = onhand_genral_sku.ids or False

    def get_available_qty_total(self):
        for rec in self:
            rec.available_qty_sum = sum(rec.stock_onhand_ids.mapped('available_qty')) or 0


    def view_sku_qty_available(self):
        return {
            'name': _('Onhand General'),
            'view_mode': 'tree,form',
            'res_model': 'stock.onhand.general',
            'view_id': False,
            'type': 'ir.actions.act_window',
            'domain': [('id', 'in', self.stock_onhand_ids.ids)],
            'context': {
                'default_sku_id': self.id,
            }
        }


    # def name_get(self):
        # result = []
        # for rec in self:
        #     result.append((rec.id, rec.pallet_no))
        # return result


class SkuEditWizard(models.TransientModel):
    _name = 'sku.edit.wizard'

    sku_id = fields.Many2one('lsc.sku.master',string='SKU Master')
    changed_image = fields.Binary("Image")
    changed_shelf_life = fields.Integer('Shelf Life (Days)')
    changed_weight = fields.Float("Weight (kg)")
    changed_unit_price = fields.Float("Unit Price")
    changed_length = fields.Float("Length (cm)")
    changed_height = fields.Float("Height (cm)")
    changed_business_id = fields.Many2many('lsc.bussines.unit', string="Business Unit")
    changed_categ_id = fields.Many2one('lsc.sku.category', string='Category')
    changed_sub_category = fields.Many2one('lsc.sku.subcategory', string="Sub Category")
    changed_pack_key = fields.Many2one('lsc.pack.key', "Pack key")
    # changed_sku_lotable_ids = fields.Many2many('sku.lotable', string='Sku Lottables')

    def send_edit_request(self):
        for rec in self:
            rec.sku_id.changed_image = rec.changed_image
            rec.sku_id.changed_shelf_life = rec.changed_shelf_life
            rec.sku_id.changed_weight = rec.changed_weight
            rec.sku_id.changed_unit_price = rec.changed_unit_price
            rec.sku_id.changed_length = rec.changed_length
            rec.sku_id.changed_height = rec.changed_height
            rec.sku_id.changed_business_id = rec.changed_business_id
            rec.sku_id.changed_categ_id = rec.changed_categ_id
            rec.sku_id.changed_sub_category = rec.changed_sub_category
            rec.sku_id.changed_pack_key = rec.changed_pack_key
            # rec.sku_id.changed_sku_lotable_ids = rec.changed_sku_lotable_ids
            rec.sku_id.edit_request_user = self.env.user.id

            users = self.env.ref('lsc_wms.group_inventory_manager').users
            note = _('Please Review Sku Master for edit request!')

            if rec.sku_id.image != rec.sku_id.changed_image:
                note += _('<p> &nbsp; - Image Change from %s to %s </p>') % (
                rec.sku_id.image, rec.sku_id.changed_image)
            if rec.sku_id.shelf_life != rec.sku_id.changed_shelf_life:
                note += _('<p> &nbsp; - Shelf Life Change from %s to %s </p>') % (
                    rec.sku_id.shelf_life, rec.sku_id.changed_shelf_life)
            if rec.sku_id.weight != rec.sku_id.changed_weight:
                note += _('<p> &nbsp; - Weight Change from %s to %s </p>') % (
                    rec.sku_id.weight, rec.sku_id.changed_weight)
            if rec.sku_id.unit_price != rec.sku_id.changed_unit_price:
                note += _('<p> &nbsp; - Unit Price Change from %s to %s </p>') % (
                    rec.sku_id.unit_price, rec.sku_id.changed_unit_price)
            if rec.sku_id.length != rec.sku_id.changed_length:
                note += _('<p> &nbsp; - Length Change from %s to %s </p>') % (
                    rec.sku_id.length, rec.sku_id.changed_length)
            if rec.sku_id.height != rec.sku_id.changed_height:
                note += _('<p> &nbsp; - Height Change from %s to %s </p>') % (
                    rec.sku_id.height, rec.sku_id.changed_height)
            if rec.sku_id.business_id != rec.sku_id.changed_business_id:
                note += _('<p> &nbsp; - Business Unit Change from %s to %s </p>') % (
                    rec.sku_id.business_id.mapped('name'), rec.sku_id.changed_business_id.mapped('name'))
            if rec.sku_id.categ_id != rec.sku_id.changed_categ_id:
                note += _('<p> &nbsp; - Category Change from %s to %s </p>') % (
                    rec.sku_id.categ_id.name, rec.sku_id.changed_categ_id.name)
            if rec.sku_id.sub_category != rec.sku_id.changed_sub_category:
                note += _('<p> &nbsp; - Sub Category Change from %s to %s </p>') % (
                    rec.sku_id.sub_category.name, rec.sku_id.changed_sub_category.name)
            if rec.sku_id.pack_key != rec.sku_id.changed_pack_key:
                note += _('<p> &nbsp; - Pack Key Change from %s to %s </p>') % (
                    rec.sku_id.pack_key.pack_key, rec.sku_id.changed_pack_key.pack_key)
            # if rec.sku_id.sku_lotable_ids != rec.sku_id.changed_sku_lotable_ids:
            #     note += _('<p> &nbsp; - Lottables Change from %s to %s </p>') % (
            #         rec.sku_id.sku_lotable_ids.mapped('name'), rec.sku_id.changed_sku_lotable_ids.mapped('name'))


            # note = _(
            #     'Please Review Sku Master for edit request!',
            #     'Image: %(image)s to %(changed_image)s',
            #     'shelf_life: %(shelf_life)s to %(changed_shelf_life)s',
            #     'weight: %(weight)s to %(changed_weight)s',
            #     'unit_price: %(unit_price)s to %(changed_unit_price)s',
            #     image=rec.sku_id.image,changed_image=rec.sku_id.changed_image,
            #     shelf_life=rec.sku_id.shelf_life, changed_shelf_life=rec.sku_id.changed_image,
            #     weight=rec.sku_id.weight, changed_weight=rec.sku_id.changed_weight,
            #     unit_price=rec.sku_id.unit_price, changed_unit_price=rec.sku_id.changed_unit_price,
            # )
            for user in users:
                activity_vals = {
                    'activity_type_id': self.env.ref('mail.mail_activity_data_todo').id,
                }
                rec.sku_id.activity_schedule(
                    note=note,
                    summary='Request Edit for Sku',
                    user_id=user.id, **activity_vals)
            rec.sku_id.edit_state = 'edit_requested'

    @api.onchange('changed_business_id')
    def onchange_business_units_edit(self):
        for business in self.sku_id.business_id:
            if business.id not in self.changed_business_id.ids:
                raise ValidationError('You can not delete the business Unit of SKU which have the Grn!, But you can add more if you want!')

    # @api.onchange('changed_sku_lotable_ids')
    # def onchange_changed_sku_lotable_ids(self):
    #     for lottable in self.sku_id.sku_lotable_ids:
    #         if lottable.id not in self.changed_sku_lotable_ids.ids:
    #             raise ValidationError('You can not Delete the Lottables of SKU which have the Grn!, But you can add more if you want!')


class sku_lotable(models.Model):
    _name = 'sku.lotable'
    _description = 'sku lotable'

    name = fields.Char(string='Name')
    # sku_id = fields.Many2one('lsc.sku.master', "SKU",required=1)

class sku_lotable_six(models.Model):
    _name = 'sku.lotable.six'
    _description = 'sku lotable six'

    name = fields.Char(string='Name')
    # sku_id = fields.Many2one('lsc.sku.master', "SKU",required=1)


class sku_qty_available(models.Model):
    _name = 'sku.qty.available'
    _description = 'sku.qty.available'

    location_id = fields.Many2one('lsc.location.master',string='Location')
    available_qty = fields.Float(string='Available Quantity')
    uom_id = fields.Many2one('uom.uom', 'UOM')
    sku_id = fields.Many2one('lsc.sku.master',string='Sku')


class ExtraFields(models.Model):
    _name = 'lsc.extra.field'
    _rec_name = 'name'
    name = fields.Char("Field name")
    sku_id = fields.Many2one('lsc.sku.master',"rel")
    value = fields.Char("Value")

class LocationMaster(models.Model):
    _name = 'lsc.location.master'
    _description = 'Location Type Master'
    _rec_name = 'location_name'

    def _default_get_current_user_customer(self):
        if self.env.user.customer_ids:
            return self.env.user.customer_ids.ids

    def _default_get_current_user_sub_warehouse(self):
        if self.env.user.sub_warehouse_ids:
            return self.env.user.sub_warehouse_ids.ids

    active = fields.Boolean('Active', default=True)
    code = fields.Char("Code")
    location_name = fields.Char("Location Name")
    location_categ = fields.Selection([('Aisle','Aisle'),('Bin','Bin'),('Rack','Rack'),('Position','Position')],"Location Category",required=1)
    capacity = fields.Char("Capacity(weight)")
    location_type = fields.Many2one('lsc.location.type.master',"Location Type",required=1)
    location_status = fields.Selection(related='location_type.location_status',
                                       string="Location Status",store=True)
    length = fields.Float("Length")
    width = fields.Float("width")
    height = fields.Float("Height")
    location_heirarchy = fields.Many2one('lsc.loc.hierarchy',"Locaton heirarchy")
    capacity_mx_unit  = fields.Float("Capacity(Max Unit)")
    warhouse_id = fields.Many2one('lsc.sub.warehouse','Sub warehouse',required=1,domain="[('id','in',current_user_sub_warehouse)]")
    above_location_type = fields.Many2one('lsc.level',"Above Location Type", required=1)
    above_location = fields.Many2one('lsc.location.master',"Above Location",domain="[('warhouse_id','=',warhouse_id)]")
    current_user_customers = fields.Many2many('res.partner', string='Current User Customer',default=_default_get_current_user_customer,
                                              compute='get_current_user_customer')

    current_user_sub_warehouse = fields.Many2many('lsc.sub.warehouse', string='Sub warehouse',default=_default_get_current_user_sub_warehouse,
                                                  compute='get_current_user_sub_warehouse')

    def get_current_user_sub_warehouse(self):
        for rec in self:
            if self.env.user.sub_warehouse_ids:
                rec.current_user_sub_warehouse = self.env.user.sub_warehouse_ids.ids
            else:
                rec.current_user_sub_warehouse = False

    def get_current_user_customer(self):
        for rec in self:
            if self.env.user.customer_ids:
                rec.current_user_customers = self.env.user.customer_ids.ids
            else:
                rec.current_user_customers = False

    @api.onchange('location_categ')
    def _onchange_heierchay(self):
        if self.location_categ=='Bin':
            if self.location_heirarchy.level_five.name=='Bin':
                self.above_location_type=self.location_heirarchy.level_four.id
            elif self.location_heirarchy.level_four.name=='Bin':
                self.above_location_type = self.location_heirarchy.level_three.id
            elif self.location_heirarchy.level_three.name=='Bin':
                self.above_location_type = self.location_heirarchy.level_two.id
            elif self.location_heirarchy.level_two.name=='Bin':
                self.above_location_type = self.location_heirarchy.level_one.id
        if self.location_categ=='Aisle':
            if self.location_heirarchy.level_five.name=='Aisle':
                self.above_location_type=self.location_heirarchy.level_four.id
            elif self.location_heirarchy.level_four.name=='Aisle':
                self.above_location_type = self.location_heirarchy.level_three.id
            elif self.location_heirarchy.level_three.name=='Aisle':
                self.above_location_type = self.location_heirarchy.level_two.id
            elif self.location_heirarchy.level_two.name=='Aisle':
                self.above_location_type = self.location_heirarchy.level_one.id
        if self.location_categ=='Rack':
            if self.location_heirarchy.level_five.name=='Rack':
                self.above_location_type=self.location_heirarchy.level_four.id
            elif self.location_heirarchy.level_four.name=='Rack':
                self.above_location_type = self.location_heirarchy.level_three.id
            elif self.location_heirarchy.level_three.name=='Rack':
                self.above_location_type = self.location_heirarchy.level_two.id
            elif self.location_heirarchy.level_two.name=='Rack':
                self.above_location_type = self.location_heirarchy.level_one.id
        if self.location_categ=='Position':
            if self.location_heirarchy.level_five.name=='Position':
                self.above_location_type=self.location_heirarchy.level_four.id
            elif self.location_heirarchy.level_four.name=='Position':
                self.above_location_type = self.location_heirarchy.level_three.id
            elif self.location_heirarchy.level_three.name=='Position':
                self.above_location_type = self.location_heirarchy.level_two.id
            elif self.location_heirarchy.level_two.name=='Position':
                self.above_location_type = self.location_heirarchy.level_one.id
    def name_get(self):
        result = []
        for loc in self:
            if loc.location_name and loc.code:
                name = loc.code+'-'+loc.location_name
                result.append((loc.id, name))
            else:
                name = loc.location_name
                result.append((loc.id, name))
        return result

class LabroryMaster(models.Model):
    _name = 'lsc.labour.master'
    _description = 'Labour Master'
    _rec_name = 'name'

    def _default_get_current_user_sub_warehouse(self):
        if self.env.user.sub_warehouse_ids:
            return self.env.user.sub_warehouse_ids.ids

    name = fields.Char("Name")
    category = fields.Char('Category')
    warehouse_id = fields.Many2one('lsc.sub.warehouse', string='Sub Warehouse', required=1,domain="[('id','in',current_user_sub_warehouse)]")
    warehouse_main_id = fields.Many2one(related='warehouse_id.warehouse_id', string='Main Warehouse')
    current_user_sub_warehouse = fields.Many2many('lsc.sub.warehouse', string='Sub warehouse',default=_default_get_current_user_sub_warehouse,compute='get_current_user_sub_warehouse')

    def get_current_user_sub_warehouse(self):
        for rec in self:
            if self.env.user.sub_warehouse_ids:
                rec.current_user_sub_warehouse = self.env.user.sub_warehouse_ids.ids
            else:
                rec.current_user_sub_warehouse = False

class LocationTypeMaster(models.Model):
    _name = 'lsc.location.type.master'

    _description = "Location Type Master"
    _rec_name = 'loc_type_name'
    loc_type_name = fields.Char("Location Type Name")
    location_status = fields.Selection([('good','Good'),('blocked','Blocked'),('QC','QC')],string="Location Status")

class Level(models.Model):
    _name = 'lsc.level'
    _rec_name = 'name'
    name= fields.Char("Level")
class PackKey(models.Model):
    _name = 'lsc.pack.key'
    _rec_name = 'pack_key'
    pack_key = fields.Char("Pack Key")
    level_one = fields.Many2one('uom.uom','Level 1')
    level_two = fields.Many2one('uom.uom','Level 2')
    level_three = fields.Many2one('uom.uom','Level 3')
    level_four = fields.Many2one('uom.uom','Level 4')
    level_five = fields.Many2one('uom.uom',"Level 5")
    l2_l1 = fields.Integer("L2 to L1")
    l3_l1 = fields.Integer("L3 to L1")
    l4_l1 = fields.Integer("L4 to L1")

    @api.model
    def create(self, vals):
        res = super(PackKey, self).create(vals)
        name=''
        if res.level_one:
            name+=res.level_one.name[0]+'1'
        if res.level_two:
            name+='-'+res.level_two.name[0]+str(res.l2_l1)
        if res.level_three:
            name+='-'+res.level_three.name[0]+str(res.l3_l1)

        if res.level_four:
            name+='-'+res.level_four.name[0]+str(res.l4_l1)
        res.pack_key =name
            # res.level_one.name[0]+'1'+'-' + res.level_two.name[0]+str(res.l2_l1)+'-'+res.level_three.name[0]+str(res.l3_l1)+'-'+res.level_four.name[0].l4_l1

        return res

class BussinesUnit(models.Model):
    _name = 'lsc.bussines.unit'
    _rec_name = 'name'

    def _default_get_current_user_customer(self):
        if self.env.user.customer_ids:
            return self.env.user.customer_ids.ids

    name = fields.Char("Bussines Name")
    partner_id = fields.Many2one('res.partner',"Customer ID",domain="[('wms','=',True),('customer_rank','>', 0),('id','in', current_user_customers)]")
    current_user_customers = fields.Many2many('res.partner', string='Current User Customer',default=_default_get_current_user_customer,
                                              compute='get_current_user_customer')

    def get_current_user_customer(self):
        for rec in self:
            if self.env.user.customer_ids:
                rec.current_user_customers = self.env.user.customer_ids.ids
            else:
                rec.current_user_customers = False

class wms_batch_no(models.Model):
    _name = 'wms.batch.no'
    _description = 'wms_batch_no'

    name = fields.Char("Name")

class SkuCategory(models.Model):
    _name = 'lsc.sku.category'
    _rec_name = 'name'

    def _default_get_current_user_customer(self):
        if self.env.user.customer_ids:
            return self.env.user.customer_ids.ids

    def _default_get_current_user_sub_warehouse(self):
        if self.env.user.sub_warehouse_ids:
            return self.env.user.sub_warehouse_ids.ids

    name = fields.Char("Category Name")
    partner_id = fields.Many2one('res.partner',"WMS Customer",domain="[('customer_rank','>',0),('wms','=',True),('id','in',current_user_customers)]")
    subwarehouse_id = fields.Many2one('lsc.sub.warehouse',"Sub warehouse",domain="[('id','in',current_user_sub_warehouse)]")
    current_user_sub_warehouse = fields.Many2many('lsc.sub.warehouse', string='Sub warehouse',default=_default_get_current_user_sub_warehouse,
                                                  compute='get_current_user_sub_warehouse')
    current_user_customers = fields.Many2many('res.partner', string='Current User Customer',default=_default_get_current_user_customer,
                                              compute='get_current_user_customer')

    def get_current_user_customer(self):
        for rec in self:
            if self.env.user.customer_ids:
                rec.current_user_customers = self.env.user.customer_ids.ids
            else:
                rec.current_user_customers = False

    def get_current_user_sub_warehouse(self):
        for rec in self:
            if self.env.user.sub_warehouse_ids:
                rec.current_user_sub_warehouse = self.env.user.sub_warehouse_ids.ids
            else:
                rec.current_user_sub_warehouse = False

    # @api.constrains('subwarehouse_id')
    # def _constrain_on_warehouse(self):
    #     if self.subwarehouse_id!=self.partner_id.warehouse_id:
    #         raise ValidationError("Select the Correct warehouse attached to the customer")
    # @api.onchange('partner_id')
    # def _onchange_domain(self):
    #     self.subwarehouse_id = self.partner_id.warhouse_id.id
    #     return {'domain': {'subwarehouse_id': [('id', '=',self.partner_id.warhouse_id.id)]}}
class SubCategory(models.Model):
    _name = 'lsc.sku.subcategory'
    _rec_name = 'name'

    name = fields.Char("Sub Category Name")

    category_id = fields.Many2one('lsc.sku.category',"Category")

class LocationHeirchy(models.Model):
    _name = 'lsc.loc.hierarchy'

    def _default_get_current_user_customer(self):
        if self.env.user.customer_ids:
            return self.env.user.customer_ids.ids

    partner_id = fields.Many2one('res.partner',"Customer ID",domain="[('customer_rank','>', 0),('id','in', current_user_customers)]")
    level_one = fields.Many2one('lsc.level', 'Level 1')
    level_two = fields.Many2one('lsc.level', 'Level 2')
    level_three = fields.Many2one('lsc.level', 'Level 3')
    level_four = fields.Many2one('lsc.level', 'Level 4')
    level_five = fields.Many2one('lsc.level', 'Level 5')
    current_user_customers = fields.Many2many('res.partner', string='Current User Customer',default=_default_get_current_user_customer,
                                              compute='get_current_user_customer')

    def get_current_user_customer(self):
        for rec in self:
            if self.env.user.customer_ids:
                rec.current_user_customers = self.env.user.customer_ids.ids
            else:
                rec.current_user_customers = False


    def name_get(self):
        result = []
        for loc in self:
            name = 'Hierarchy'+' '+'of'+' '+ loc.partner_id.name
            result.append((loc.id, name))
        return result

class LocationTwoCustomer(models.Model):
    _name = 'lsc.location.customer'

    def _default_get_current_user_customer(self):
        if self.env.user.customer_ids:
            return self.env.user.customer_ids.ids

    location_id = fields.Many2one('lsc.location.master',"Location")
    partner_id = fields.Many2one('res.partner',domain="[('customer_rank','>', 0),('id','in', current_user_customers)]")
    current_user_customers = fields.Many2many('res.partner', string='Current User Customer',default=_default_get_current_user_customer,
                                              compute='get_current_user_customer')

    def get_current_user_customer(self):
        for rec in self:
            if self.env.user.customer_ids:
                rec.current_user_customers = self.env.user.customer_ids.ids
            else:
                rec.current_user_customers = False