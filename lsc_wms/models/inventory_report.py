# -*- coding: utf-8 -*-
import pdb

from odoo import models, fields, api
from odoo.exceptions import UserError, ValidationError

class stock_onhand_general(models.Model):
    _name = 'stock.onhand.general'
    _description = 'stock_onhand_general'


    sku_id = fields.Many2one('lsc.sku.master',"SKU")
    description = fields.Char(string='Description')
    uom_id = fields.Many2one('uom.uom', 'UOM')
    available_qty = fields.Integer('Quantity')
    blocked_qty = fields.Integer('Blocked Quantity')
    damaged_qty = fields.Integer('Blocked Quantity')
    bussines_unit_id = fields.Many2one('lsc.bussines.unit', string="Business Unit")

    def compute(self):
        data_dict = {}
        # for line in rec.grn_line_ids:
        #     if line.sku_type == 'serilized' and line.serial_no == False:
        #         raise ValidationError("You must have to enter serial no for serialized sku!")
        #     manf_date = None
        #     if line.manf_date:
        #         manf_date = line.manf_date
        #     expiry_date = None
        #     if line.expiry_date:
        #         expiry_date = line.expiry_date
        #     receiving_date = None
        #     if line.receiving_date:
        #         receiving_date = line.receiving_date.date()
        #     key = (str(line.sku_id.id) + str(line.batch_no) + str(manf_date) + str(expiry_date) + str(
        #         line.bussines_unit_id.id) + str(receiving_date))
        #     if key in data_dict:
        #         values = {
        #             'product_uom_qty': data_dict[key].get('lines').append(line),
        #         }
        #         data_dict[key].update(values)
        #     else:
        #         data_dict[key] = {
        #             'key': key,
        #             'lines': [line],
        #         }
class inventory_report(models.Model):
    _name = 'inventory.report'
    _inherit = ['mail.thread', 'mail.activity.mixin', 'mail.render.mixin']
    _description = 'inventory_report'


    customer_id = fields.Many2one('res.partner',string='Customer',domain="[('wms','=',True),('customer_rank','>', 0)]")
    warhouse_ids = fields.Many2many(related='customer_id.warhouse_ids', string="Warehouse Mapping")
    warehouse_id = fields.Many2one('lsc.sub.warehouse',store=True,string='Warehouse')
    warehouse_main_id = fields.Many2one(related='warehouse_id.warehouse_id', string='Main Warehouse')
    sku_id = fields.Many2one('lsc.sku.master',"SKU")
    categ_id = fields.Many2one(related='sku_id.categ_id', string='SKU Category', store=True)
    description = fields.Char(string='Description')
    uom_id = fields.Many2one('uom.uom', 'UOM')
    pack_key_id = fields.Many2one('lsc.pack.key', 'Pack Key')
    batch_no = fields.Char("Batch No")
    serial_no = fields.Char("Serial No")
    bussines_unit_id = fields.Many2one('lsc.bussines.unit', string="Business Unit")
    manf_date = fields.Date("Manufacturing Date")
    expiry_date = fields.Date("Expiry Date")
    location = fields.Many2one('lsc.location.master',string='Location')
    location_type = fields.Many2one(related='location.location_type',string='Location Type',store=True)
    available_qty = fields.Integer('Available Quantity')
    allocated_qty = fields.Integer('Allocated Quantity')
    picked_qty = fields.Float('Picked QTY')
    onhold_qty = fields.Integer('Hold Quantity')
    hold_status = fields.Integer('Hold Status')
    total_quantity = fields.Integer('Total Quantity',compute='get_total_quanity')
    remarks = fields.Char("Remarks")
    lot1 = fields.Char("Lottable01")
    lot2 = fields.Char('Lottable02')
    lot3 = fields.Char('Lottable03')
    lot4 = fields.Char('Lottable04')
    lot5 = fields.Char('Lottable05')
    lot6 = fields.Many2one('sku.lotable.six',string='Lottable06')
    lot7 = fields.Many2one('sku.lotable',string='Lot07')
    lot8 = fields.Datetime('Lottable08')
    lot9 = fields.Datetime('Lottable09')
    lot10 = fields.Datetime('Lottable10')
    source_stock_onhand = fields.Many2one('inventory.report', string='Source Stock Onhand')
    # serial_no_id = fields.Many2one('lsc.grn.serial', store=True, index=True,
    #                             string="Serial Number", readonly=False)
    grn_line_id = fields.Many2one('lsc.grn.lines', string='Grn Line')
    # batch_line_id = fields.Many2one('batch.grn.lines', string='Batch Line')
    # serial_line_id = fields.Many2one('lsc.grn.serial',string="Serial Line")

    lot = fields.Char("Lot No")
    grn_id = fields.Many2one('lsc.grn', 'Grn')
    asn_id = fields.Many2one('lsc.asn', 'Asn')
    ext_ref = fields.Char(related='asn_id.ext_ref',string="ASN External Ref",store=True)
    po_number = fields.Char(related='asn_id.po_number',string="ASN PO Number",store=True)
    remarks = fields.Text(related='asn_id.remarks',string="ASN Remarks",store=True)
    undf1 = fields.Char(related='asn_id.undf1',string="In-UDF1",store=True)
    undf2 = fields.Char(related='asn_id.undf2',string="In-UDF2",store=True)
    undf3 = fields.Datetime(related='asn_id.undf3',string="In-UDF3",store=True)
    undf4 = fields.Char(related='asn_id.undf4',string="In-UDF4",store=True)
    vendor_id = fields.Many2one(related='asn_id.vendor_id', domain="[('customer_id','=',customer_id_id )]", string='vendor',store=True)
    for_computation = fields.Char(string='For Computation',compute='stockonhand_for_computation')

    def stockonhand_for_computation(self):
        for rec in self:
            total_quantity = rec.available_qty+rec.allocated_qty+rec.onhold_qty+rec.picked_qty
            if total_quantity == 0:
                rec.unlink()
        for rec in self:
            rec.for_computation = 'for computation'

    def get_total_quanity(self):
        for rec in self:
            rec.total_quantity = rec.available_qty+rec.allocated_qty+rec.onhold_qty+rec.picked_qty
            # self.env.search([
            #     ('customer_id','=',rec.customer_id.id),
            #     ('sku_id','=',rec.sku_id.id),
            #     ('serial_no','=',rec.serial_no),
            #     ('batch_no','=',rec.batch_no),
            #     ('location_type.loc_type_name','in',['HOLD',]),
            #     ('customer_id','=',rec.customer_id.id),
            #                  ])

    # def write(self, vals):
    #     res = super(inventory_report, self).write(vals)
    #     for rec in self:
    #         if self.total_quantity == 0:
    #             self.unlink()
    #     return res
    #
    # @api.model
    # def create(self, vals):
    #     res = super(inventory_report, self).create(vals)
    #     for rec in res:
    #         if rec.available_qty+rec.allocated_qty+rec.onhold_qty+rec.picked_qty == 0:
    #             raise ValidationError('You can not create the record with stock Onhand total quantity is zero!')
    #     return res


