# -*- coding: utf-8 -*-
import pdb

from odoo import models, fields, api
from odoo.exceptions import UserError, ValidationError

class wms_transactions(models.Model):
    _name = 'wms.transactions'
    _inherit = ['mail.thread', 'mail.activity.mixin', 'mail.render.mixin']

    _description = 'wms transactions'

    transaction_type = fields.Selection(
        [('grn', 'GRN'), ('putaway', 'Putaway'), ('transfers', 'Transfers'), ('adjustment', 'Adjustment'), ('outbound', 'OutBound')], string='Transaction Type')
    document_no = fields.Char(string='Document No')
    date = fields.Date(string='Date')
    customer_id = fields.Many2one('res.partner',string='Customer')
    warehouse_id = fields.Many2one('lsc.sub.warehouse',store=True,string='Warehouse')
    warehouse_main_id = fields.Many2one(related='warehouse_id.warehouse_id', string='Main Warehouse')
    sku_id = fields.Many2one('lsc.sku.master',"SKU")
    description = fields.Char(string='Description')
    uom_id = fields.Many2one('uom.uom', 'UOM')
    pack_key_id = fields.Many2one('lsc.pack.key', 'Pack Key')
    batch_no = fields.Char("Batch No")
    manf_date = fields.Date("Manufacturing Date")
    expiry_date = fields.Date("Expiry Date")
    bussines_unit_id = fields.Many2one('lsc.bussines.unit', string="Business Unit")
    quantity = fields.Float(string="Quantity")
    lot1 = fields.Char("Lottable01")
    lot2 = fields.Char('Lottable02')
    lot3 = fields.Char('Lottable03')
    lot4 = fields.Char('Lottable04')
    lot5 = fields.Char('Lottable05')
    lot6 = fields.Many2one('sku.lotable.six',string='Lottable06')
    lot7 = fields.Many2one('sku.lotable',string='Lot07')
    lot8 = fields.Datetime('Lottable08')
    lot9 = fields.Datetime('Lottable09')
    lot10 = fields.Datetime('Lottable10')
    remarks = fields.Char("Remarks")
    lot = fields.Char("Lot No")
    serial_no = fields.Char("Serial No")
    location = fields.Many2one('lsc.location.master','Location')


