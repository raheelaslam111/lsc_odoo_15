# -*- coding: utf-8 -*-
import pdb

from odoo import models, fields, api
from odoo.exceptions import UserError, ValidationError


class wms_transfers(models.Model):
    _name = 'wms.transfers'
    _inherit = ['mail.thread', 'mail.activity.mixin', 'mail.render.mixin']
    _description = 'wms Transfers'
    _rec_name = 'document_no'

    document_no = fields.Char(string='Document Number')
    transaction_date = fields.Date(string='Transaction Date')
    customer_id = fields.Many2one('res.partner',string='Customer')
    warehouse_id = fields.Many2one('lsc.sub.warehouse',store=True,string='Warehouse')
    warehouse_main_id = fields.Many2one(related='warehouse_id.warehouse_id', string='Main Warehouse')
    state = fields.Selection([('draft', 'Draft'), ('approved', 'Approved'), ('hold', 'Hold')], string="Status",
                             default='draft')
    labour_id = fields.Many2one('lsc.labour.master', track_visibility='onchange', string="Labour")
    transfer_lines_ids = fields.One2many('wms.transfers.line','wms_transfer_id',string='Transfer Moves')

    @api.model
    def create(self, vals):
        transfer_seq = self.env['ir.sequence'].next_by_code('seq.wms.transfer')
        vals['document_no'] = transfer_seq
        res = super(wms_transfers, self).create(vals)
        return res


    @api.model
    def open_transfer_wizard(self):
        return {
            'name': "Transfer",
            'type': 'ir.actions.act_window',
            'view_mode': 'form',
            'view_type': 'form',
            'res_model': 'wms.transfers.wizard',
            'views': [(False, 'form')],
            'target': 'new',
        }


    def inventory_transfer(self):
        for rec in self:
            for line in rec.transfer_lines_ids:
                if any(rec.transfer_lines_ids.filtered(lambda b: b.to_qty < 1)):
                    raise ValidationError('To Quantity in lines is Zero!')
                if any(rec.transfer_lines_ids.filtered(lambda b: b.to_location == False)):
                    raise ValidationError('Add To Location in Lines!')
                domain = [('id', '!=', line.inventory_report_id.id)]
                to_location_inventory_line = self.env['inventory.report']
                if rec.customer_id:
                    domain += [('customer_id', '=', rec.customer_id.id)]
                if line.sku_id:
                    domain += [('sku_id', '=', line.sku_id.id)]
                if line.batch_no:
                    domain += [('batch_no', '=', line.batch_no)]
                if line.serial_no:
                    domain += [('serial_no', '=', line.serial_no)]
                if line.bussines_unit_id:
                    domain += [('bussines_unit_id', '=', line.bussines_unit_id.id)]
                if line.from_location:
                    domain += [('location', '=', line.to_location.id)]
                if domain != []:
                    line_search = self.env['inventory.report'].search(domain)
                    to_location_inventory_line = line_search
                if to_location_inventory_line:
                    if len(to_location_inventory_line) > 1:
                        print('multiple lines of inventory')
                        raise ValidationError('multiple lines of inventory!')
                    elif len(to_location_inventory_line) == 1:
                        if line.inventory_report_id.available_qty - line.to_qty == 0:
                            to_location_inventory_line[0].available_qty += line.to_qty
                            line.inventory_report_id.unlink()
                            transaction_vals = {
                                'quantity': -(line.to_qty),
                                'remarks': line.remarks,
                                'lot1': line.lot1,
                                'lot2': line.lot2,
                                'lot3': line.lot3,
                                'lot4': line.lot4,
                                'lot5': line.lot5,
                                'lot6': line.lot6.id,
                                'lot7': line.lot7.id,
                                'lot8': line.lot8,
                                'lot9': line.lot9,
                                'lot10': line.lot10,
                                'serial_no': line.serial_no or False,
                                'description': line.description,
                                'uom_id': line.uom_id.id,
                                'sku_id': line.sku_id.id,
                                'pack_key_id': line.pack_key_id.id,
                                'bussines_unit_id': line.bussines_unit_id.id,
                                'batch_no': line.batch_no or False,
                                'manf_date': line.manf_date,
                                'expiry_date': line.expiry_date,
                                'lot': line.lot,
                                'location': line.from_location.id,
                                'transaction_type': 'transfers',
                                'document_no': rec.document_no,
                                'customer_id': rec.customer_id.id,
                                'date': fields.date.today(),
                                'warehouse_id': line.warehouse_id.id,
                            }
                            transaction = self.env['wms.transactions'].create(transaction_vals)
                            transaction_vals = {
                                'quantity': (line.to_qty),
                                'remarks': line.remarks,
                                'lot1': line.lot1,
                                'lot2': line.lot2,
                                'lot3': line.lot3,
                                'lot4': line.lot4,
                                'lot5': line.lot5,
                                'lot6': line.lot6.id,
                                'lot7': line.lot7.id,
                                'lot8': line.lot8,
                                'lot9': line.lot9,
                                'lot10': line.lot10,
                                'serial_no': line.serial_no or False,
                                'description': line.description,
                                'uom_id': line.uom_id.id,
                                'sku_id': line.sku_id.id,
                                'pack_key_id': line.pack_key_id.id,
                                'bussines_unit_id': line.bussines_unit_id.id,
                                'batch_no': line.batch_no or False,
                                'manf_date': line.manf_date,
                                'expiry_date': line.expiry_date,
                                'lot': line.lot,
                                'location': line.to_location.id,
                                'transaction_type': 'transfers',
                                'document_no': rec.document_no,
                                'customer_id': rec.customer_id.id,
                                'date': fields.date.today(),
                                'warehouse_id': line.warehouse_id.id,
                            }
                            transaction = self.env['wms.transactions'].create(transaction_vals)
                        else:
                            to_location_inventory_line[0].available_qty += line.to_qty
                            # line.inventory_report_id.available_qty = int(line.inventory_report_id.available_qty - line.to_qty)
                            line.inventory_report_id.write(
                                {'available_qty': int(line.inventory_report_id.available_qty - line.to_qty)})
                            transaction_vals = {
                                'quantity': -(int(line.to_qty)),
                                'remarks': line.remarks,
                                'lot1': line.lot1,
                                'lot2': line.lot2,
                                'lot3': line.lot3,
                                'lot4': line.lot4,
                                'lot5': line.lot5,
                                'lot6': line.lot6.id,
                                'lot7': line.lot7.id,
                                'lot8': line.lot8,
                                'lot9': line.lot9,
                                'lot10': line.lot10,
                                'serial_no': line.serial_no or False,
                                'description': line.description,
                                'uom_id': line.uom_id.id,
                                'sku_id': line.sku_id.id,
                                'pack_key_id': line.pack_key_id.id,
                                'bussines_unit_id': line.bussines_unit_id.id,
                                'batch_no': line.batch_no or False,
                                'manf_date': line.manf_date,
                                'expiry_date': line.expiry_date,
                                'lot': line.lot,
                                'location': line.from_location.id,
                                'transaction_type': 'transfers',
                                'document_no': rec.document_no,
                                'customer_id': rec.customer_id.id,
                                'date': fields.date.today(),
                                'warehouse_id': line.warehouse_id.id,
                            }
                            transaction = self.env['wms.transactions'].create(transaction_vals)
                            transaction_vals = {
                                'quantity': (int(line.to_qty)),
                                'remarks': line.remarks,
                                'lot1': line.lot1,
                                'lot2': line.lot2,
                                'lot3': line.lot3,
                                'lot4': line.lot4,
                                'lot5': line.lot5,
                                'lot6': line.lot6.id,
                                'lot7': line.lot7.id,
                                'lot8': line.lot8,
                                'lot9': line.lot9,
                                'lot10': line.lot10,
                                'serial_no': line.serial_no or False,
                                'description': line.description,
                                'uom_id': line.uom_id.id,
                                'sku_id': line.sku_id.id,
                                'pack_key_id': line.pack_key_id.id,
                                'bussines_unit_id': line.bussines_unit_id.id,
                                'batch_no': line.batch_no or False,
                                'manf_date': line.manf_date,
                                'expiry_date': line.expiry_date,
                                'lot': line.lot,
                                'location': line.to_location.id,
                                'transaction_type': 'transfers',
                                'document_no': rec.document_no,
                                'customer_id': rec.customer_id.id,
                                'date': fields.date.today(),
                                'warehouse_id': line.warehouse_id.id,
                            }
                            transaction = self.env['wms.transactions'].create(transaction_vals)
                    else:
                        print('Create Line in inventory report')
                        # to_location_inventory_line[0].available_qty += line.to_qty
                        # line.inventory_report_id.available_qty = int(line.inventory_report_id.available_qty - line.to_qty)
                else:
                    if not line.to_location:
                        raise ValidationError('To location is empty!')
                    if line.serial_no:
                        serial_no = line.serial_no or False
                    else:
                        serial_no = False
                    inventory_report_vals = {
                        'customer_id': rec.customer_id.id,
                        'sku_id': line.sku_id.id,
                        'location': line.to_location.id,
                        'available_qty': line.to_qty,
                        'description': line.description,
                        'uom_id': line.uom_id.id,
                        'pack_key_id': line.pack_key_id.id,
                        'serial_no': serial_no,
                        'lot': line.lot,
                        'bussines_unit_id': line.bussines_unit_id.id,
                        'batch_no': line.batch_no or False,
                        'remarks': line.remarks,
                        'lot1': line.lot1,
                        'lot2': line.lot2,
                        'lot3': line.lot3,
                        'lot4': line.lot4,
                        'lot5': line.lot5,
                        'lot6': line.lot6.id,
                        'lot7': line.lot7.id,
                        'lot8': line.lot8,
                        'lot9': line.lot9,
                        'lot10': line.lot10,
                        'manf_date': line.manf_date,
                        'expiry_date': line.expiry_date,
                        'warehouse_id': line.warehouse_id.id,
                    }
                    inventory_report = self.env['inventory.report'].create(inventory_report_vals)
                    transaction_vals = {
                        'quantity': -(line.to_qty),
                        'remarks': line.remarks,
                        'lot1': line.lot1,
                        'lot2': line.lot2,
                        'lot3': line.lot3,
                        'lot4': line.lot4,
                        'lot5': line.lot5,
                        'lot6': line.lot6.id,
                        'lot7': line.lot7.id,
                        'lot8': line.lot8,
                        'lot9': line.lot9,
                        'lot10': line.lot10,
                        'serial_no': line.serial_no or False,
                        'description': line.description,
                        'uom_id': line.uom_id.id,
                        'sku_id': line.sku_id.id,
                        'pack_key_id': line.pack_key_id.id,
                        'bussines_unit_id': line.bussines_unit_id.id,
                        'batch_no': line.batch_no or False,
                        'manf_date': line.manf_date,
                        'expiry_date': line.expiry_date,
                        'lot': line.lot,
                        'location': line.from_location.id,
                        'transaction_type': 'transfers',
                        'document_no': rec.document_no,
                        'customer_id': rec.customer_id.id,
                        'date': fields.date.today(),
                        'warehouse_id': line.warehouse_id.id,
                    }
                    transaction = self.env['wms.transactions'].create(transaction_vals)
                    transaction_vals = {
                        'quantity': (line.to_qty),
                        'remarks': line.remarks,
                        'lot1': line.lot1,
                        'lot2': line.lot2,
                        'lot3': line.lot3,
                        'lot4': line.lot4,
                        'lot5': line.lot5,
                        'lot6': line.lot6.id,
                        'lot7': line.lot7.id,
                        'lot8': line.lot8,
                        'lot9': line.lot9,
                        'lot10': line.lot10,
                        'serial_no': line.serial_no or False,
                        'description': line.description,
                        'uom_id': line.uom_id.id,
                        'sku_id': line.sku_id.id,
                        'pack_key_id': line.pack_key_id.id,
                        'bussines_unit_id': line.bussines_unit_id.id,
                        'batch_no': line.batch_no or False,
                        'manf_date': line.manf_date,
                        'expiry_date': line.expiry_date,
                        'lot': line.lot,
                        'location': line.to_location.id,
                        'transaction_type': 'transfers',
                        'document_no': rec.document_no,
                        'customer_id': rec.customer_id.id,
                        'date': fields.date.today(),
                        'warehouse_id': line.warehouse_id.id,
                    }
                    transaction = self.env['wms.transactions'].create(transaction_vals)
                    if line.to_qty == line.from_qty:
                        line.inventory_report_id.unlink()
                    else:
                        line.inventory_report_id.available_qty = int(line.inventory_report_id.available_qty - line.to_qty)
            rec.transaction_date = fields.date.today()
            rec.state = 'approved'

    def hold_unhold(self):
        for rec in self:
            if rec.state != 'hold':
                rec.state = 'hold'
            else:
                rec.state = 'draft'

class wms_transfers_line(models.Model):
    _name = 'wms.transfers.line'
    _inherit = ['mail.thread', 'mail.activity.mixin', 'mail.render.mixin']
    _description = 'wms.transfers.line'

    wms_transfer_id = fields.Many2one('wms.transfers',string='Document Number')
    transaction_date = fields.Date(related='wms_transfer_id.transaction_date',string='Transaction Date',store=True)
    customer_id = fields.Many2one(related='wms_transfer_id.customer_id', string='Customer',store=True)
    warehouse_id = fields.Many2one('lsc.sub.warehouse', store=True, string='Warehouse')
    sku_id = fields.Many2one('lsc.sku.master', string="SKU")
    description = fields.Char(string='Description')
    uom_id = fields.Many2one('uom.uom', string='UOM')
    batch_no = fields.Char(string="Batch No")
    serial_no = fields.Char(string="Serial No")
    bussines_unit_id = fields.Many2one('lsc.bussines.unit', string="Business Unit")
    location_type = fields.Many2one(related='from_location.location_type', string='Location Type', store=True)
    # available_qty = fields.Float(string='Available Quantity')
    manf_date = fields.Date(string="Manufacturing Date")
    expiry_date = fields.Date("Expiry Date")
    lot = fields.Char("Lot No")
    lot1 = fields.Char("Lottable01")
    lot2 = fields.Char('Lottable02')
    lot3 = fields.Char('Lottable03')
    lot4 = fields.Char('Lottable04')
    lot5 = fields.Char('Lottable05')
    lot6 = fields.Many2one('sku.lotable.six','Lottable06')
    lot7 = fields.Many2one('sku.lotable',string='Lot07')
    lot8 = fields.Datetime('Lottable08')
    lot9 = fields.Datetime('Lottable09')
    lot10 = fields.Datetime('Lottable10')
    remarks = fields.Char("Remarks")

    from_location = fields.Many2one('lsc.location.master', string='From Location')
    from_location_id = fields.Integer(related='from_location.id', string='From Location id',store=True)
    to_location = fields.Many2one('lsc.location.master', string='To Location',domain="[('id','!=',from_location_id),('location_status','=','good')]")
    from_qty = fields.Float(string='From Quantity', store=True)
    to_qty = fields.Float(string='To Quantity')
    state = fields.Selection(related='wms_transfer_id.state',string='Status')
    pack_key_id = fields.Many2one('lsc.pack.key', 'Pack Key')
    inventory_report_id = fields.Many2one('inventory.report', string='Inventory Report ID')

    @api.constrains('to_qty', 'serial_no')
    def _check_to_quantity_transfer(self):
        if self.serial_no and self.to_qty > 1:
            raise ValidationError('You can not add Quantity more than one with serial no!')

    def show_detail(self):
        self.ensure_one()
        return {
            'name': "WMS Transfer Form",
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'wms.transfers',
            'res_id': self.wms_transfer_id.id,
        }

class wms_transfers_wizard(models.TransientModel):
    _name = 'wms.transfers.wizard'
    _inherit = ['mail.thread', 'mail.activity.mixin', 'mail.render.mixin']
    _description = 'wms_transfers_wizard'

    customer_id = fields.Many2one('res.partner', string='Customer',domain="[('wms','=',True),('customer_rank','=',1)]",required=True)
    warehouse_id = fields.Many2one('lsc.sub.warehouse', store=True, string='Sub Warehouse',required=True)
    sku_id = fields.Many2one('lsc.sku.master', string="SKU")
    batch_no = fields.Char(string="Batch No")
    serial_no = fields.Char(string="Serial No")
    bussines_unit_id = fields.Many2one('lsc.bussines.unit', string="Business Unit")
    from_location = fields.Many2one('lsc.location.master',string='From Location')
    wizard_line_ids = fields.One2many('transfers.wizard.lines','transfer_wizard_id',string='Inventory Report Lines',readonly=False)

    @api.onchange('sku_id', 'batch_no', 'serial_no', 'bussines_unit_id', 'from_location','customer_id','warehouse_id')
    def get_inventory_report_lines(self):
        for rec in self:
            domain = []
            if rec.customer_id:
                domain += [('customer_id', '=', rec.customer_id.id)]
            if rec.sku_id:
                domain += [('sku_id', '=', rec.sku_id.id)]
            if rec.batch_no:
                domain += [('batch_no', '=', rec.batch_no)]
            if rec.serial_no:
                domain += [('serial_no', '=', rec.serial_no)]
            if rec.bussines_unit_id:
                domain += [('bussines_unit_id', '=', rec.bussines_unit_id.id)]
            if rec.from_location:
                domain += [('location', '=', rec.from_location.id)]
            if rec.warehouse_id:
                domain += [('warehouse_id', '=', rec.warehouse_id.id)]
            if domain != []:
                # wizard_lines_previous = self.env['transfers.wizard.lines'].search([])
                # wizard_lines_previous.unlink()
                from_locaiton_inventory_line = self.env['inventory.report'].search(domain)
                # rec.wizard_line_ids = from_locaiton_inventory_line.ids
                wizard_lines = self.env['transfers.wizard.lines']
                for line in from_locaiton_inventory_line.filtered(lambda b: b.available_qty > 0):
                    vals = {
                        'customer_id': line.customer_id.id,
                        'sku_id': line.sku_id.id,
                        'serial_no': line.serial_no or False,
                        'bussines_unit_id': line.bussines_unit_id.id,
                        'batch_no': line.batch_no or False,
                        'from_location': line.location.id or False,
                        'transfer_wizard_id': rec.id,
                        'inventory_report_id': line.id,
                        'qty': line.available_qty,
                    }
                    wizard_line = self.env['transfers.wizard.lines'].create(vals)
                    wizard_lines += wizard_line

                rec.wizard_line_ids = wizard_lines.ids
            else:
                rec.wizard_line_ids = False

    def map_inventory_report_lines(self):
        # pdb.set_trace()
        for rec in self:
            wizard_line = self.env['transfers.wizard.lines'].search([('id', 'in', rec.wizard_line_ids.ids)])
            for line in wizard_line:
                print(line.select)
            # pdb.set_trace()

            transfer_vals = {
                'customer_id': rec.customer_id.id or False,
                'warehouse_id': rec.warehouse_id.id or False,
            }
            wms_inventory = self.env['wms.transfers'].create(transfer_vals)
            for line in rec.wizard_line_ids.filtered(lambda b: b.select == True):
                transfer_line_vals = {

                    'sku_id': line.inventory_report_id.sku_id.id,
                    'from_location': line.inventory_report_id.location.id,
                    'from_qty': line.inventory_report_id.available_qty,
                    'description': line.inventory_report_id.description,
                    'uom_id': line.inventory_report_id.uom_id.id,
                    'serial_no': line.inventory_report_id.serial_no or False,
                    'lot': line.inventory_report_id.lot,
                    'bussines_unit_id': line.inventory_report_id.bussines_unit_id.id,
                    'batch_no': line.inventory_report_id.batch_no or False,
                    'remarks': line.inventory_report_id.remarks,
                    'pack_key_id': line.inventory_report_id.pack_key_id.id,
                    'lot1': line.inventory_report_id.lot1,
                    'lot2': line.inventory_report_id.lot2,
                    'lot3': line.inventory_report_id.lot3,
                    'lot4': line.inventory_report_id.lot4,
                    'lot5': line.inventory_report_id.lot5,
                    'lot6': line.inventory_report_id.lot6.id,
                    'lot7': line.inventory_report_id.lot7.id,
                    'lot8': line.inventory_report_id.lot8,
                    'lot9': line.inventory_report_id.lot9,
                    'lot10': line.inventory_report_id.lot10,
                    'manf_date': line.inventory_report_id.manf_date,
                    'expiry_date': line.inventory_report_id.expiry_date,
                    'inventory_report_id': line.inventory_report_id.id,
                    'wms_transfer_id': wms_inventory.id,
                    'warehouse_id': line.inventory_report_id.warehouse_id.id,
                }
                wms_inventory_line = self.env['wms.transfers.line'].create(transfer_line_vals)



class transfers_wizard_lines(models.TransientModel):
    _name = 'transfers.wizard.lines'
    _description = 'transfers_wizard_lines'

    customer_id = fields.Many2one('res.partner', string='Customer')
    sku_id = fields.Many2one('lsc.sku.master', string="SKU")
    batch_no = fields.Char(string="Batch No")
    serial_no = fields.Char(string="Serial No")
    bussines_unit_id = fields.Many2one('lsc.bussines.unit', string="Business Unit")
    from_location = fields.Many2one('lsc.location.master', string='From Location')
    transfer_wizard_id = fields.Many2one('wms.transfers.wizard',string='Transfer Wizard Id')
    inventory_report_id = fields.Many2one('inventory.report',string='Inventory Report ID')
    select = fields.Boolean(string='Select')
    qty = fields.Float(string='Qty Available')
