from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError


class Allocation(models.Model):
    _name = 'lsc.allocation'
    _inherit = ['mail.thread', 'mail.activity.mixin', 'mail.render.mixin']
    _description = "Allocation"

    name = fields.Char("Order ID")
    order_type = fields.Selection([
        ('delivery', 'Delivery'),
        ('vendor_return', 'Vendor Return'),
        ('stock_trnasfer', 'Stock Transfer')], string="Order Type")
    external_ref = fields.Char("External Ref")
    invoice_number = fields.Char("Invoice Number")
    order_date_time = fields.Datetime("Order Date time")
    customer_id = fields.Many2one('res.partner', "Customer", domain="[('customer_rank','>',0)]")
    supplier_id = fields.Many2one('res.partner', "Supplier/Vendor", domain="[('supplier_rank','>',0)]")
    remarks = fields.Text("Remarks")
    order_id = fields.Many2one('lsc.outbound.orders', 'Order ID')
    order_ids = fields.One2many('lsc.allocation.line', 'order_id', string="Order Lines")
    state = fields.Selection([('created', 'Created'),
                              ('allocated_partially', 'Part Allocated'),
                              ('allocated', 'allocated'),
                              ('part_picked', 'Picked'),('picked','Picked'), ('part_shipped', 'Part Shipped'),('part_picked','Part Picked'), ('closed', 'Closed'),

                              ('cancel', 'Cancel')],track_visibility='onchange',
                             default='created')

    pick_id = fields.Many2one('lsc.pick')
    readonly_button = fields.Boolean("Invisible Button", default=False)
    dispatch_id = fields.Many2one('lsc.dispatch')
    dispatch_bo = fields.Boolean("Dispatch BO")
    cancel_comments = fields.Char('Cancel Comments', readonly=1)
    cancel_bo = fields.Boolean('Cancel Bo',compute='_compute_cancel',copy=False)
    labour_id = fields.Many2one('lsc.labour.master',track_visibility='onchange' ,string="Labour")
    lock_pick_bo = fields.Boolean('Lock Pick Boolean',compute='_compute_lock_pick')

    @api.depends('order_ids')
    def _compute_lock_pick(self):
        for rec in self:
            if any(disptch_line.readonly_picked==False for disptch_line in
                   rec.order_ids):
                rec.lock_pick_bo=False
            else:
                rec.lock_pick_bo=True
    @api.depends('dispatch_id')
    def _compute_cancel(self):
        for rec in self:
            if rec.dispatch_id:
                if rec.dispatch_id.state=='cancel':
                    rec.cancel_bo=True
                else:
                    rec.cancel_bo=False
            else:
                rec.cancel_bo=True

    def cancel_wizard(self):
        return {
            'name': 'Cancel Comment',
            'type': 'ir.actions.act_window',
            'res_model': 'lsc.cancel.comments',
            'view_mode': 'form',
            'view_id': False,
            'target': 'new'}

    @api.model
    def create(self, vals):
        alloc_seq = self.env['ir.sequence'].next_by_code('wms.alloc.seq')

        vals['name'] = alloc_seq

        res = super(Allocation, self).create(vals)
        return res

    def lock_pick_action(self):
        if any(line.picked_qty == 0.0 and line.reserve_click == False for line in self.order_ids):
            raise ValidationError("Please picked qty should not b zero please unreserve it first")

        self.readonly_button = True
        # for line in self.order_ids:

        line_lst=[]
        for line in self.order_ids:
            line.update({
                'readonly_picked': True
            })
            msg = 'the qty'+str(line.picked_qty)+' for sku '+str(line.sku_id.code)+'has been picked'
            line.order_id.message_post(body=msg)
            # create_lines = self.create_pick_line(line)
            if line.reserve_click == False and line.picked_qty != 0:
                create_lines = self.create_pick_line(line)
                line_lst.append(create_lines)
                line._state_assign()
                line.inv_report_id.allocated_qty -= line.picked_qty
                line.inv_report_id.picked_qty +=line.picked_qty
                transcation_report = self.env['wms.transactions'].create({
                    'transaction_type': 'outbound',
                    'document_no': line.order_id.name,
                    'date': fields.date.today(),
                    'customer_id': line.order_id.customer_id.id,
                    'warehouse_id': line.warehouse_id.id,
                    'sku_id': line.sku_id.id,
                    'description': line.description,
                    'uom_id': line.uom_id.id,
                    'pack_key_id': line.pack_key_id.id,
                    'batch_no': line.batch_number,
                    'manf_date': False,
                    'expiry_date': False,
                    'bussines_unit_id': line.bussines_id.id,
                    'quantity': -(line.picked_qty),
                    'lot1': line.lot1,
                    'lot2': line.lot2,
                    'lot3': line.lot3,
                    'lot4': line.lot4,
                    'lot5': line.lot5,
                    'lot6': line.lot6.id,
                    'lot7': line.lot7.id,
                    'lot8': line.lot8,
                    'lot9': line.lot9,
                    'lot10': line.lot10,
                    'remarks': line.remarks,
                    'lot': line.lot,
                    'serial_no': line.serial_no,
                    'location': line.location.id

                })
        if not self.dispatch_id or self.dispatch_id.state == 'cancel':
            create_pick_rec = self.env['lsc.dispatch'].create({
                'order_outbound_id': self.order_id.id,
                'order_id_alloc': self.id,
                'order_date_time': self.order_id.order_date_time,
                'customer_id': self.order_id.customer_id.id,
                'supplier_id': self.order_id.supplier_id.id,
                'external_ref': self.order_id.external_ref,
                'invoice_number': self.order_id.invoice_number,
                'order_type': self.order_id.order_type,
                'remarks': self.order_id.remarks,
                'udf1': self.order_id.udf1,
                'udf2': self.order_id.udf2,
                'udf3': self.order_id.udf3,

                'order_ids': line_lst,
                # 'state': 'draft'
            })
            self.dispatch_id = create_pick_rec.id
            self.dispatch_bo = True
        else:
            self.dispatch_id.order_ids = line_lst

        if any(disptch_line.state == 'part_picked' for disptch_line in self.order_ids):
            self.state = 'part_picked'
            # self.order_id.state = 'part_picked'
        elif all(disptch_line.state == 'picked' for disptch_line in self.order_ids):
            self.state = 'picked'
            # self.order_id.state = 'picked'

        if any(disptch_line.state == 'part_picked' for disptch_line in self.order_id.order_ids):
            self.order_id.state='part_picked'

        elif all(disptch_line.state == 'picked' for disptch_line in self.order_id.order_ids):
            self.order_id.state='picked'

            # action = self.env["ir.actions.actions"]._for_xml_id("lsc_wms.action_manager_comments")
            # # scraps = self.env['lsc.allocation'].search([('order_id', '=', self.id)])
            # # action['domain'] = [('id', 'in', scraps.ids)]
            # # return action
            # return {
            #     'type': 'ir.actions.act_window',
            #     'name': 'Message',
            #     'res_model': 'lsc.allocation.warning',
            #     'view_type': 'form',
            #     'view_mode': 'form',
            #     'target': 'new',
            #
            # }

    def cancel_wizard(self):
        return {
            'name': 'Cancel Comment',
            'type': 'ir.actions.act_window',
            'res_model': 'lsc.cancel.comments',
            'view_mode': 'form',
            'view_id': False,
            'target': 'new'
            # 'views': [(self.env.ref('account_asset.view_account_asset_form').id, 'form')],
            # 'res_id': line.id,
        }

    def cancel(self):
        for line in self.order_ids:
            transcation_report = self.env['wms.transactions'].create({
                'transaction_type': 'transfers',
                'document_no': line.order_id.name,
                'date': fields.date.today(),
                'customer_id': line.order_id.customer_id.id,
                'warehouse_id': line.warehouse_id.id,
                'sku_id': line.sku_id.id,
                'description': line.description,
                'uom_id': line.uom_id.id,
                'pack_key_id': line.pack_key_id.id,
                'batch_no': line.batch_number,
                'manf_date': False,
                'expiry_date': False,
                'bussines_unit_id': line.bussines_id.id,
                'quantity': line.picked_qty,
                'lot1': line.lot1,
                'lot2': line.lot2,
                'lot3': line.lot3,
                'lot4': line.lot4,
                'lot5': line.lot5,
                'lot6': line.lot6.id,
                'lot7': line.lot7.id,
                'lot8': line.lot8,
                'lot9': line.lot9,
                'lot10': line.lot10,
                'remarks': line.remarks,
                'lot': line.lot,
                'serial_no': line.serial_no,
                'location': line.location.id

            })
            if line.inv_report_id:
                # line.inv_report_id.allocated_qty =line.inv_report_id.allocated_qty- line.qty
                line.inv_report_id.available_qty += line.qty
                # self.readonly_button=False
                # self.state='created'
                # self.cancel_before=True
                # for order in self.order_ids:
                line.write({
                        'picked_qty':0.0,
                        'balance':line.qty,
                        'state':'draft'
                    })
                # if self.order_id:
                #     s
                msg = 'The  allocated qty  '+str(line.qty)+ ' ' + str(
                      ' has been updated ' \
                                    ' form: <a href=# data-oe-model=lsc.allocation data-oe-id=%d>%s</a> ' \
                                    ' due to cancel ') % (
                          self.id, self.name)
                line.inv_report_id.message_post(body=msg)
        self.dispatch_bo=True
        self.order_id.state='created'
        for line in self.order_id.order_ids:
            line.update({
                'total_allocated':0.0,
                'balance':line.qty,
                'state':'draft'
            })
        msg = 'The   ' + str(
            ' allocation  Created against this order has been cancel: <a href=# data-oe-model=lsc.allocation data-oe-id=%d>%s</a> '
        ) % (
                  self.id, self.name)
        self.order_id.message_post(body=msg)
        # self.order_id_alloc.message_post(body=msg)
        self.state = 'cancel'

    def create_dispatch(self):

        line_lst = []
        order_lines = self.order_ids.filtered(lambda b: b.picked_qty > 0)
        for line in order_lines:
            create_lines = self.create_pick_line(line)
            line._state_assign()
            # print(line.picked_qty)
            # balance = line.inv_report_id.allocated_qty- line.picked_qty
            # line.inv_report_id.allocated_qty =balance

            line_lst.append(create_lines)
        if not self.dispatch_id or self.dispatch_id.state=='cancel':
            create_pick_rec = self.env['lsc.dispatch'].create({
                'order_outbound_id': self.order_id.id,
                'order_id_alloc': self.id,
                'order_date_time': self.order_id.order_date_time,
                'customer_id': self.order_id.customer_id.id,
                'supplier_id': self.order_id.supplier_id.id,
                'external_ref': self.order_id.external_ref,
                'invoice_number': self.order_id.invoice_number,
                'order_type': self.order_id.order_type,
                'remarks': self.order_id.remarks,
                'udf1': self.order_id.udf1,
                'udf2': self.order_id.udf2,
                'udf3': self.order_id.udf3,

                'order_ids': line_lst,
                # 'state': 'draft'
            })
            self.dispatch_id = create_pick_rec.id
            self.dispatch_bo = True
        else:
            self.dispatch_id.order_ids = line_lst
        # if any(disptch_line.state =='part_picked'  for disptch_line in self.order_ids):
        #     self.state = 'allocated_partially'
        # else:
        #     self.state = 'picked'

    def create_pick_line(self, line):
        lines = ((0, 0, {
            'sku_id': line.sku_id.id,
            'description': line.description,
            'allocation_line_id': line.id,
            'inv_report_id':line.inv_report_id.ids,
            'uom_id': line.uom_id.id,
            'qty': line.picked_qty,
            'batch_number': line.batch_number if line.batch_number else False,
            'serial_no': line.serial_no,
            'lot': line.lot,
            'bussines_id': line.bussines_id.id,
            'remarks': line.remarks,
            'pack_key_id': line.pack_key_id,
            'lot1': line.lot1,
            'lot2': line.lot2,
            'lot3': line.lot3,
            'lot4': line.lot4,
            'lot5': line.lot5,
            'lot6': line.lot6.id,
            'lot7': line.lot7.id,
            'lot8': line.lot8,
            'lot9': line.lot9,
            'lot10': line.lot10,
            'lot': line.lot,
            'location': line.location.id,
            'warehouse_id': line.warehouse_id.id,
            # 'inv_report_id':line.inv_report_id.id,
            'order_line_id': line.order_line_id.id
        }))

        return lines

    def action_dispatch_open(self):
        self.ensure_one()
        action = self.env["ir.actions.actions"]._for_xml_id("lsc_wms.action_lsc_dispatch")
        scraps = self.env['lsc.dispatch'].search([('order_id_alloc', '=', self.id)])
        action['domain'] = [('id', 'in', scraps.ids)]
        return action


class AllocationLines(models.Model):
    _name = 'lsc.allocation.line'

    order_id = fields.Many2one('lsc.allocation', "Rel", ondelete='cascade', )
    line_no = fields.Integer("Line no")
    sku_id = fields.Many2one('lsc.sku.master', "SKU")
    description = fields.Char("Description")
    uom_id = fields.Many2one('uom.uom', "UOM")
    qty = fields.Float("Quantity")
    allocation_qty = fields.Float("Allocation QTY")
    batch_number = fields.Char("Batch No")
    # serilizad_bo = fields.Boolean("Serilized Bo")
    serial_no = fields.Char("Serial Number")
    bussines_id = fields.Many2one('lsc.bussines.unit', string="Business Unit")
    location = fields.Many2one('lsc.location.master', string='Location')
    balance = fields.Float("balance",)
    remarks = fields.Char("Remarks")
    reserve_click = fields.Boolean("reserved")
    lot1 = fields.Char("Lottable01")
    lot2 = fields.Char('Lottable02')
    lot3 = fields.Char('Lottable03')
    lot4 = fields.Char('Lottable04')
    lot5 = fields.Char('Lottable05')
    lot6 = fields.Many2one('sku.lotable.six',string='Lottable06')
    lot7 = fields.Many2one('sku.lotable', string='Lottable07')
    lot8 = fields.Datetime('Lottable08')
    lot9 = fields.Datetime('Lottable09')
    lot10 = fields.Datetime('Lottable10')
    lot = fields.Char("LOT#")
    warehouse_id = fields.Many2one('lsc.sub.warehouse', store=True, string='Warehouse')

    inv_report_id = fields.Many2many('inventory.report', string="Inventory Report")
    picked_qty = fields.Float('Picked QTY')
    order_line_id = fields.Many2one('lsc.order.line', "Order Line ID")
    order_qty= fields.Float('Order Qty',related='order_line_id.qty')
    state = fields.Selection([('draft', 'Draft'), ('part_picked', 'Part Picked'),('part_shipped','Part Shipped') ,('shipped','Shipped'),('picked', 'Picked')],
                        string="state", default='draft')
    readonly_picked = fields.Boolean("readonly Picked", default=False)
    pack_key_id = fields.Many2one('lsc.pack.key', 'Pack Key')

    @api.onchange('picked_qty')
    def _compute_balance(self):
        for rec in self:
            balance = rec.qty - rec.picked_qty
            rec.balance = balance

    def _state_assign(self):

        if  self.balance==0 and self.order_line_id.qty==self.picked_qty:
            self.state = 'picked'
            self.order_line_id.state = 'picked'
        elif self.balance>0 and self.balance==self.qty:
            self.state = 'draft'
            # self.order_line_id.state='part_picked'
        elif self.balance>0 and self.balance!=self.qty:
            self.state = 'part_picked'
            self.order_line_id.state='part_picked'

    def unreserve_stock(self):
        if self.balance > 0:
            # self.qty -= self.balance
            self.reserve_click = True
            self.inv_report_id.allocated_qty =self.inv_report_id.allocated_qty- self.balance
            self.inv_report_id.available_qty += self.balance
            self.order_line_id.total_allocated -= self.balance
            self.balance -= self.balance
            msg = 'The qty' + ' ' + str(
                self.balance) + ' has been unreserve  form: <a href=# data-oe-model=lsc.allocation.line data-oe-id=%d>%s</a>' % (
                      self.order_id.id, self.order_id.name)
            line_bal = 0.0
            self.inv_report_id.message_post(body=msg)
            self.order_line_id.balance = self.order_line_id.qty - self.order_line_id.total_allocated
        #     if self.order_line_id.total_allocated > 0:
        #         self.order_line_id.state = 'par_allocate'
        #     elif self.order_line_id.total_allocated == 0:
        #         self.balance = 0
        #         self.order_line_id.state = 'draft'
        #     if all(order_line.state == 'draft' for order_line in self.order_line_id.order_id.order_ids):
        #         self.order_line_id.order_id.state = 'created'
        #     if any(order_line.state == 'par_allocate' for order_line in self.order_line_id.order_id.order_ids):
        #         self.order_line_id.order_id.state = 'allocated_partially'
        # if self.balance == 0 and self.qty == 0 and self.picked_qty == 0:
            if len(self.order_id.order_ids) == 1:
                self.order_id.state = 'cancel'
                self.order_id.order_id.state = 'created'
            self.unlink()
