# -*- coding: utf-8 -*-
import pdb
from odoo import models, fields, exceptions, api, _
import xlrd
from xlrd import open_workbook
import logging
_logger = logging.getLogger(__name__)
from odoo.exceptions import ValidationError, UserError
try:
    import xlwt
except ImportError:
    _logger.debug('Cannot `import xlwt`.')
try:
    import cStringIO
except ImportError:
    _logger.debug('Cannot `import cStringIO`.')
try:
    import base64
except ImportError:
    _logger.debug('Cannot `import base64`.')

class AsnWms(models.Model):
    _name = 'lsc.asn'
    _inherit = ['mail.thread', 'mail.activity.mixin', 'mail.render.mixin']
    _description = 'Asn'
    _rec_name = 'name'

    def _default_get_current_user_customer(self):
        if self.env.user.customer_ids:
            return self.env.user.customer_ids.ids

    def _default_get_current_user_sub_warehouse(self):
        if self.env.user.sub_warehouse_ids:
            return self.env.user.sub_warehouse_ids.ids

    name = fields.Char('ASN ID', tracking=True)
    state = fields.Selection(
        [('new', 'New'), ('in_receiving', 'In Receiving'), ('received', 'Received'),
         ('close', 'Close'), ('cancel', 'Cancel')], default='new', string='State')
    asn_type = fields.Selection([('purchase','Purchase'),('cust_return','Customer Return'),('stock_transfer','Stock Transfer')],required=1,string="ASN Type", tracking=True)
    ext_ref = fields.Char("External Reference Number",required=1, tracking=True)
    po_number = fields.Char("PO Number", tracking=True)
    asn_datetime = fields.Datetime("ASN Date & Time",readonly=1,default=fields.Datetime.now(), tracking=True)
    customer_id_id = fields.Many2one('res.partner',required=1,domain="[('customer_rank','>', 0),('wms','=',True),('id','in',current_user_customers)]",string='Customer', tracking=True)
    warhouse_ids = fields.Many2many(related='customer_id_id.warhouse_ids', string="Warehouse Mapping")
    warehouse_id = fields.Many2one('lsc.sub.warehouse',string='Sub Warehouse',required=1,domain="[('id','in',current_user_sub_warehouse)]")
    warehouse_main_id = fields.Many2one(related='warehouse_id.warehouse_id',string='Main Warehouse')
    vendor_id = fields.Many2one('res.partner',domain="[('customer_id','=',customer_id_id )]",string='vendor', tracking=True)
    remarks = fields.Text("Remarks", tracking=True)
    undf1 = fields.Char("In-UDF1", tracking=True)
    undf2 = fields.Char("In-UDF2", tracking=True)
    undf3 = fields.Datetime("In-UDF3", tracking=True)
    undf4 = fields.Char("In-UDF4", tracking=True)
    asn_line_ids = fields.One2many('lsc.asn.lines','asn_id',ondelete='cascade', index=True,string="ASN line")
    grn_ids = fields.One2many('lsc.grn','asn_id',ondelete='cascade')
    len_grn_lines = fields.Integer(string='Length of GRN',compute='get_grn_length')
    len_asn_lines = fields.Integer(string='Length of Asn Lines',compute='get_asn_lines_length')
    import_asn_file = fields.Binary(string='Asn Import File')
    current_user_customers = fields.Many2many('res.partner', string='Current User Customer',
                                              default=_default_get_current_user_customer,
                                              compute='get_current_user_customer')

    current_user_sub_warehouse = fields.Many2many('lsc.sub.warehouse', string='Sub warehouse',
                                                  default=_default_get_current_user_sub_warehouse,
                                                  compute='get_current_user_sub_warehouse')

    def get_current_user_sub_warehouse(self):
        for rec in self:
            if self.env.user.sub_warehouse_ids:
                rec.current_user_sub_warehouse = self.env.user.sub_warehouse_ids.ids
            else:
                rec.current_user_sub_warehouse = False

    def get_current_user_customer(self):
        for rec in self:
            if self.env.user.customer_ids:
                rec.current_user_customers = self.env.user.customer_ids.ids
            else:
                rec.current_user_customers = False

    # _sql_constraints = [('customer_po_uniq', 'unique (po_number,customer_id_id)',
    #                      'PO number must be unique per customer!')]



    def import_asn_file_date(self):
        wb = xlrd.open_workbook(file_contents=base64.decodebytes(self.import_asn_file))

        for sheet in wb.sheets():
            # pdb.set_trace()
            for row in range(sheet.nrows):
                # print(row)

                if row == 0:
                    continue

                else:
                    # cl = 0
                    # for col in range(sheet.ncols):
                    #     print(range(sheet.ncols), 'length')
                    #     print(sheet.cell(row, col).value, 'value')
                    #     cl += 1

                    # if (row%3) == 0:
                    #     print(row,'this is the row', (row%3))
                    sku = sheet.cell(row, 0).value
                    business_unit = sheet.cell(row, 1).value
                    batch_no = sheet.cell(row, 2).value
                    serial_no = sheet.cell(row, 3).value
                    expected_qty = sheet.cell(row, 4).value
                    expire_date = sheet.cell(row, 5).value
                    manufacture_date = sheet.cell(row, 6).value
                    lot1 = sheet.cell(row, 7).value
                    lot2 = sheet.cell(row, 8).value
                    lot3 = sheet.cell(row, 9).value
                    lot4 = sheet.cell(row, 10).value
                    lot5 = sheet.cell(row, 11).value
                    lot6 = sheet.cell(row, 12).value
                    lot7 = sheet.cell(row, 13).value
                    lot8 = sheet.cell(row, 14).value
                    lot9 = sheet.cell(row, 15).value
                    lot10 = sheet.cell(row, 16).value
                    remarks = sheet.cell(row, 17).value


                    # print(product_quantity, date_order, date_planned)
                    # print(fields.Datetime.now())
                    vals = {
                        'expected_qty': int(expected_qty),
                        # 'expiry_date': expire_date,
                        # 'manf_date': manufacture_date,
                        'lot1': str(lot1),
                        'lot2': str(lot2),
                        'lot3': str(lot3),
                        'lot4': str(lot4),
                        'lot5': str(lot5),
                        'lot6': str(lot6),
                        # 'lot8': str(lot8),
                        # 'lot9': str(lot9),
                        # 'lot10': str(lot10),
                        'remarks': str(remarks),
                        'asn_id': self.id,
                    }
                    sku_master = self.env['lsc.sku.master'].search([('code', '=', str(sku))], limit=1)
                    vals.update({'sku_id': sku_master.id})
                    # for l7 in sku_master.sku_lotable_ids:
                    #     if l7.name == str(lot7):
                    #         vals.update({'lot7': l7.id})
                    for bu in sku_master.business_id:
                        if bu.name == str(business_unit):
                            vals.update({'bussines_unit_id': bu.id})
                    if sku_master.sku_type == "serilized":
                        vals.update({
                            'serial_no': str(serial_no),
                        })
                    if sku_master.sku_type == "batch_wise":
                        vals.update({
                            'batch_no': str(batch_no),
                        })
                    asn_line = self.env['lsc.asn.lines'].create(vals)

    def get_asn_lines_length(self):
        for rec in self:
            rec.len_asn_lines = len(rec.asn_line_ids)

    def unlink(self):
        for rec in self:
            putaway = self.env["wms.putaway"].search([('asn_id','=',rec.id)])
            if putaway:
                if putaway.state == 'validate':
                    raise ValidationError('You Can not delete Asn which have Validated Putaway!')
                else:
                    raise ValidationError('You Can not delete Asn which have Putaway! You have to delete it first!')
            grn = self.env["lsc.grn"].search([('asn_id', '=', rec.id)])
            if grn:
                raise ValidationError('You Can not delete Asn which have Grn! You have to delete it first!')
        for rec in self:
            rec.asn_line_ids.unlink()
        res = super(AsnWms, self).unlink()
        return res

    def get_grn_length(self):
        for rec in self:
            rec.len_grn_lines = len(rec.grn_ids)

    # @api.model
    # def create(self, values):
    #     if 'asn_line_ids' in values:
    #         for lines in values['asn_line_ids']:
    #             if

    def create_grn(self,confirmation_shown=False):
        # for line in self.asn_line_ids:
            # if line.serlized_bo == True and confirmation_shown==False:
                # if any(line.lsc_serial_number.filtered(lambda b: not b.name)) or len(line.lsc_serial_number) == 0:
                #     self.ensure_one()
                #     action = self.env["ir.actions.actions"]._for_xml_id("lsc_wms.asn_confirmation_wizard_action")
                #     action['context'] = {
                #                         'default_asn_id': self.id,
                #                     }
                #     return action
            # if line.serlized_bo == False and confirmation_shown==False:
            #     if any(line.batch_line_ids.filtered(lambda b: not b.batch_no)) or len(line.batch_line_ids) == 0:
            #         self.ensure_one()
            #         action = self.env["ir.actions.actions"]._for_xml_id("lsc_wms.asn_confirmation_wizard_action")
            #         action['context'] = {
            #             'default_asn_id': self.id,
            #         }
            #         return action
        grn_line = self.env['lsc.grn.lines']
        grn_batch = self.env['batch.grn.lines']
        grn_id = self.env['lsc.grn'].create({
            'asn_id':self.id,
            'external_ref':self.ext_ref,
            'po_number':self.po_number,
            'asn_datetime':self.asn_datetime,
            'customer_id':self.customer_id_id.id,
            'vendor_id':self.vendor_id.id,
            'warehouse_id':self.warehouse_id.id,
            'asn_type':self.asn_type
        })
        for line in self.asn_line_ids:
            grn_serial = self.env['lsc.grn.serial']
            if line.serlized_bo == False:
                created_grn_line = grn_line.create({
                    'sku_id':line.sku_id.id,
                    'description':line.description,
                    'uom_id':line.uom_id.id,
                    'pack_key_id':line.pack_key_id.id,
                    'batch_no':line.batch_no,
                    'manf_date':line.manf_date,
                    'expiry_date':line.expiry_date,
                    'bussines_unit_id':line.bussines_unit_id.id,
                    'remarks':line.remarks,
                    'expected_qty':line.expected_qty,
                    'grn_id':grn_id.id,
                    'asn_line_id':line.id,
                    'lot1': line.lot1,
                    'lot2': line.lot2,
                    'lot3': line.lot3,
                    'lot4': line.lot4,
                    'lot5': line.lot5,
                    'lot6': line.lot6.id,
                    'lot7': line.lot7.id,
                    'lot8': line.lot8,
                    'lot9': line.lot9,
                    'lot10': line.lot10,

                })
            else:
                for i in range(line.expected_qty):
                    created_grn_line = grn_line.create({
                        'sku_id': line.sku_id.id,
                        'description': line.description,
                        'uom_id': line.uom_id.id,
                        'pack_key_id': line.pack_key_id.id,
                        'serial_no': line.serial_no,
                        'manf_date': line.manf_date,
                        'expiry_date': line.expiry_date,
                        'bussines_unit_id': line.bussines_unit_id.id,
                        'remarks': line.remarks,
                        'expected_qty': 1,
                        'grn_id': grn_id.id,
                        'asn_line_id': line.id,
                        'lot1': line.lot1,
                        'lot2':line.lot2,
                        'lot3':line.lot3,
                        'lot4':line.lot4,
                        'lot5':line.lot5,
                        'lot6':line.lot6.id,
                        'lot7':line.lot7.id,
                        'lot8':line.lot8,
                        'lot9':line.lot9,
                        'lot10':line.lot10,
                    })
            # if line.serlized_bo == True:
            #     for serial in line.lsc_serial_number:
            #         vals = {
            #             'name':serial.name,
            #             'grn_line_id':created_grn_line.id,
            #             'lot1':serial.lot1,
            #             'lot2':serial.lot2,
            #             'lot3':serial.lot3,
            #             'lot4':serial.lot4,
            #             'lot5':serial.lot5,
            #             'lot6':serial.lot6,
            #             'lot7':serial.lot7.id,
            #             'lot8':serial.lot8,
            #             'lot9':serial.lot9,
            #             'lot10':serial.lot10,
            #         }
            #         grn_serial+= self.env['lsc.grn.serial'].create(vals)
            # created_grn_line.serial_no = grn_serial.ids
            # if line.serlized_bo == False:
            #     for batch in line.batch_line_ids:
            #         vals = {
            #             'batch_no': batch.batch_no.id,
            #             'expected_qty': batch.expected_qty,
            #             'lot1': batch.lot1,
            #             'lot2': batch.lot2,
            #             'lot3': batch.lot3,
            #             'lot4': batch.lot4,
            #             'lot5': batch.lot5,
            #             'lot6': batch.lot6,
            #             'lot7': batch.lot7.id,
            #             'lot8': batch.lot8,
            #             'lot9': batch.lot9,
            #             'lot10': batch.lot10,
            #             'grn_line_id': created_grn_line.id,
            #         }
            #         grn_batch += self.env['batch.grn.lines'].create(vals)
        self.state='new'

    def action_grn_open(self):
        self.ensure_one()
        action = self.env["ir.actions.actions"]._for_xml_id("lsc_wms.action_lsc_grn")
        scraps = self.env['lsc.grn'].search([('asn_id', '=', self.id)])
        action['domain'] = [('id', 'in', scraps.ids)]
        return action

    # @api.onchange('ext_ref')
    # def _onchange_unique_ext_ref(self):
    #     if self.customer_id_id and self.customer_id_id.unique_customer_reference == True:
    #         lsc = self.env['lsc.asn'].search([('ext_ref', '=', self.ext_ref)])
    #         if len(lsc) > 1:
    #             raise ValidationError('External Reference must be unique!')

    @api.constrains('ext_ref', 'customer_id_id.unique_customer_reference')
    def _check_unique_ext_ref(self):
        if self.customer_id_id and self.customer_id_id.unique_customer_reference == True:
            lsc = self.env['lsc.asn'].search([('ext_ref', '=', self.ext_ref)])
            if len(lsc) > 1:
                raise ValidationError('External Reference must be unique!')

    @api.constrains('po_number', 'customer_id_id.unique_customer_reference')
    def _check_unique_po_number(self):
        if self.customer_id_id and self.customer_id_id.unique_customer_reference == True:
            lsc = self.env['lsc.asn'].search([('po_number', '=', self.po_number)])
            if len(lsc) > 1:
                raise ValidationError('PO Number must be unique!')

    # def create_lines(self):
    @api.model
    def create(self, vals):
        if vals['asn_type'] == 'purchase':
            vals['name'] = self.env['ir.sequence'].next_by_code('asn.seq')
        if vals['asn_type'] == 'cust_return':
            vals['name'] = self.env['ir.sequence'].next_by_code('rasn.seq')
        res = super(AsnWms, self).create(vals)



        return res

    # def name_get(self):
    #     result = []
    #     for rec in self:
    #         result.append((rec.id, rec.employee_id.name + ' ' + rec.employee_id.employee_number))
    #     return result



class AsnLines(models.Model):
    _name = 'lsc.asn.lines'
    _description = 'ASN Lines'


    asn_id = fields.Many2one('lsc.asn')
    name = fields.Integer('S.N', compute='_compute_get_number',
        store=True)
    state = fields.Selection(
        [('new', 'New'), ('in_receiving', 'In Receiving'), ('partial_received', 'Partial Received'), ('received', 'Received'),
         ('close', 'Close'), ('cancel', 'Cancel')], default='new', string='Status')
    sku_id = fields.Many2one('lsc.sku.master',"SKU")
    description = fields.Char(related='sku_id.description',store=True)
    uom_id = fields.Many2one('uom.uom','UOM')
    expected_qty = fields.Integer("Expected QTY",required='1')
    pack_key_id = fields.Many2one('lsc.pack.key','Pack Key')
    batch_no = fields.Char("Batch No")
    serial_no = fields.Char("Serial Number")
    expected_qty = fields.Integer('Expected QTY')
    recieved_qty = fields.Float('Received QTY')
    bus_id = fields.Many2one('lsc.bussines.unit')
    sku_business_ids = fields.Many2many(related='sku_id.business_id', string="sku Business Unit")
    bussines_unit_id = fields.Many2one('lsc.bussines.unit',string="Business Unit")
    manf_date = fields.Date("Manufacturing Date")
    expiry_date = fields.Date("Expiry Date")
    remarks = fields.Char("Remarks")
    # lsc_serial_number = fields.One2many('lsc.serial.number','lines_id',string="Serial Number")
    serlized_bo  = fields.Boolean("Serlized Boolean",compute='compute_serialized_bo')
    # line_len = fields.Integer("Line len",compute='line_length',store=True)
    # batch_line_ids = fields.One2many('batch.asn.lines', 'asn_line_id', string='Batch Lines', ondelete='cascade')
    bussiness_unit_mandatory = fields.Boolean(related='asn_id.customer_id_id.business_unit_mandatory',store=True)
    lot1 = fields.Char("Lot01")
    lot2 = fields.Char('Lot02')
    lot3 = fields.Char('Lot03')
    lot4 = fields.Char('Lot04')
    lot5 = fields.Char('Lot05')
    customer_id = fields.Many2one(related='asn_id.customer_id_id',domain="[('customer_rank','>', 0),('wms','=',True),('id','in',current_user_customers)]",string='Customer', store=True)
    lot7_ids = fields.Many2many('sku.lotable', string='Lot07',compute='get_lot_seven_ids')
    lot6_ids = fields.Many2many('customer.lotable.six', string='Lot06',compute='get_lot_six_ids')
    # help = "lottables 67"
    lot6 = fields.Many2one('sku.lotable.six',string='Lot06',domain="[('id','in',lot6_ids)]")
    lot7 = fields.Many2one('sku.lotable', string='Lot07',domain="[('id','in',lot7_ids)]")
    lot8 = fields.Datetime('Lot08')
    lot9 = fields.Datetime('Lot09')
    lot10 = fields.Datetime('Lot10')
    sku_type = fields.Selection(related='sku_id.sku_type', string="Sku Type", store=True)
    asn_type = fields.Selection(related='asn_id.asn_type',string="ASN Type", store=True)

    @api.depends('customer_id')
    def get_lot_seven_ids(self):
        for rec in self:
            lot7_ids = self.env['customer.lottable.seven'].search([('customer_id','=',rec.customer_id.id)],limit=1).lottable_seven_ids.mapped('id')
            if lot7_ids:
                rec.lot7_ids = lot7_ids
            else:
                rec.lot7_ids = False

    @api.depends('customer_id')
    def get_lot_six_ids(self):
        for rec in self:
            lot6_ids = self.env['customer.lotable.six'].search([('customer_id', '=', rec.customer_id.id)],
                                                                  limit=1).lottable_six_ids.mapped('id')
            if lot6_ids:
                rec.lot6_ids = lot6_ids
            else:
                rec.lot6_ids = False

    @api.model
    def create(self, vals):
        # if 'sku_id' in vals:
        #     if vals['sku_id'].serilized == True:
        # print(vals['asn_id'])
        # pdb.set_trace()
        asn = self.env['lsc.asn'].search([('id', '=', vals['asn_id'])])
        if asn.asn_type == 'cust_return':
            if any(self.env['inventory.report'].search(
                    [('sku_id', '=', vals['sku_id']), ('serial_no', '=', vals['serial_no']),
                     ('available_qty', '>', 0)])):
                raise ValidationError(
                    _('Serial No is Already exist in Stock On-hand!.'))
            elif not any(self.env['wms.transactions'].search(
                    [('sku_id', '=', vals['sku_id']), ('serial_no', '=', vals['serial_no'])])):
                raise ValidationError(
                    _('There is no history of one of mentioned serial number in System!.'))
        # pdb.set_trace()
        if 'serial_no' in vals and 'sku_id' in vals and asn.asn_type != 'cust_return':
            if vals['serial_no']:
                check_unique_serial_line = self.env['wms.transactions'].search(
                    [('sku_id', '=', vals['sku_id']), ('serial_no', '=', vals['serial_no'])])
                if check_unique_serial_line:
                    raise ValidationError(_('Serial No is already exists with respect to sku!.'))
        res = super(AsnLines, self).create(vals)
        return res

    # @api.depends('expected_qty','batch_line_ids.expected_qty','serlized_bo')
    # def validation_expected_qty(self):

    # _sql_constraints = [('serial_sku_uniq_s', 'unique (sku_id,serial_no)',
    #                      'Duplicate Serial number in the ASN with respect to sku!')]

    _sql_constraints = [('batch_sku_uniq', 'unique (sku_id,batch_no)',
                         'Duplicate Batch number in the ASN with respect to sku!')]

    @api.constrains('serial_no', 'expected_qty')
    def contraint_On_serial(self):
        for rec in self:
            if rec.serial_no != False and rec.expected_qty >1:
                raise ValidationError('With Serial no the expected quantity can not be greater than 1.')


    def _update_line_quantity(self,old,new,field_name):
        for line in self:
            msg = "<b>" + _("The ASN line has been updated.") + "</b><ul>"
            msg += "<li>SKU %s: <br/>" % line.sku_id.code
            msg += _(
                "%(field_name)s: %(old_qty)s -> %(new_qty)s",
                field_name=field_name,
                old_qty=old,
                new_qty=new
            ) + "<br/>"
            # if line.product_id.type in ('consu', 'product'):
            #     msg += _("Delivered Quantity: %s", line.qty_delivered) + "<br/>"
            # msg += _("Invoiced Quantity: %s", line.qty_invoiced) + "<br/>"
        msg += "</ul>"
        self.asn_id.message_post(body=msg)

    def write(self, values):
        # previous_received_quantity = self.recieved_qty
        previous_uom_id = self.uom_id
        previous_description = self.description
        previous_pack_key_id = self.pack_key_id
        previous_serial_no = self.serial_no
        previous_expiry_date = self.expiry_date
        previous_manf_date = self.manf_date
        previous_bussines_unit_id = self.bussines_unit_id
        # previous_lot1 = self.lot1
        # previous_lot2 = self.lot2
        # previous_lot3 = self.lot3
        # previous_lot4 = self.lot4
        # previous_lot5 = self.lot5
        # previous_lot6 = self.lot6
        # previous_lot7 = self.lot7
        # previous_lot8 = self.lot8
        # previous_lot9 = self.lot9
        # previous_lot10 = self.lot10
        result = super(AsnLines, self).write(values)
        # if self.recieved_qty != previous_received_quantity:
        #     if 'recieved_qty' in values:
        #         self._update_line_quantity(old=previous_received_quantity,new=self.recieved_qty,field_name=self._fields['recieved_qty'].string)
        if self.uom_id != previous_uom_id:
            if 'uom_id' in values:
                self._update_line_quantity(old=previous_uom_id.name,new=self.uom_id.name,field_name=self._fields['uom_id'].string)
        if self.description != previous_description:
            if 'description' in values:
                self._update_line_quantity(old=previous_description,new=self.description,field_name=self._fields['description'].string)
        if self.pack_key_id != previous_pack_key_id:
            if 'pack_key_id' in values:
                self._update_line_quantity(old=previous_pack_key_id,new=self.pack_key_id,field_name=self._fields['pack_key_id'].string)
        if self.serial_no != previous_serial_no:
            if 'serial_no' in values:
                self._update_line_quantity(old=previous_serial_no,new=self.serial_no,field_name=self._fields['serial_no'].string)
        if self.expiry_date != previous_expiry_date:
            if 'expiry_date' in values:
                self._update_line_quantity(old=previous_expiry_date,new=self.expiry_date,field_name=self._fields['expiry_date'].string)
        if self.manf_date != previous_manf_date:
            if 'manf_date' in values:
                self._update_line_quantity(old=previous_manf_date,new=self.manf_date,field_name=self._fields['manf_date'].string)
        if self.bussines_unit_id != previous_bussines_unit_id:
            if 'bussines_unit_id' in values:
                self._update_line_quantity(old=previous_bussines_unit_id.name,new=self.bussines_unit_id.name,field_name=self._fields['bussines_unit_id'].string)
        # if self.lot1 != previous_lot1:
        #     if 'lot1' in values:
        #         self._update_line_quantity(old=previous_lot1,new=self.lot1,field_name=self._fields['lot1'].string)
        # if self.lot2 != previous_lot2:
        #     if 'lot2' in values:
        #         self._update_line_quantity(old=previous_lot2,new=self.lot2,field_name=self._fields['lot2'].string)
        # if self.lot3 != previous_lot3:
        #             if 'lot3' in values:
        #                 self._update_line_quantity(old=previous_lot3,new=self.lot3,field_name=self._fields['lot3'].string)
        # if self.lot4 != previous_lot4:
        #             if 'lot4' in values:
        #                 self._update_line_quantity(old=previous_lot4,new=self.lot4,field_name=self._fields['lot4'].string)
        # if self.lot5 != previous_lot5:
        #             if 'lot5' in values:
        #                 self._update_line_quantity(old=previous_lot5,new=self.lot5,field_name=self._fields['lot5'].string)
        # if self.lot6 != previous_lot6:
        #                     if 'lot6' in values:
        #                         self._update_line_quantity(old=previous_lot6,new=self.lot6,field_name=self._fields['lot6'].string)
        # if self.lot7 != previous_lot7:
        #                     if 'lot7' in values:
        #                         self._update_line_quantity(old=previous_lot7,new=self.lot7,field_name=self._fields['lot7'].string)
        # if self.lot8 != previous_lot8:
        #                     if 'lot8' in values:
        #                         self._update_line_quantity(old=previous_lot8,new=self.lot8,field_name=self._fields['lot8'].string)
        # if self.lot9 != previous_lot9:
        #                     if 'lot9' in values:
        #                         self._update_line_quantity(old=previous_lot9,new=self.lot9,field_name=self._fields['lot9'].string)
        # if self.lot10 != previous_lot10:
        #                     if 'lot10' in values:
        #                         self._update_line_quantity(old=previous_lot10,new=self.lot10,field_name=self._fields['lot10'].string)

        return result



    # @api.depends('expected_qty', 'recieved_qty')
    # def get_balanced_quantity(self):
    #     for rec in self:
    #         rec.balanced_qty = rec.expected_qty - rec.recieved_qty

    # @api.depends('lsc_serial_number')
    # def line_length(self):
    #     for rec in self:
    #         rec.line_len = len(rec.lsc_serial_number)


    # _sql_constraints = [
    #     (
    #         'check_serial',
    #         'CHECK(line_len>expected_qty)',
    #         'Serial number line length must be equal to expected QTY !'
    #     )]
    # @api.constrains('batch_line_ids','expected_qty','lsc_serial_number.expected_qty','serlized_bo')
    # def contraint_On_serial(self):
    #     for rec in self:
    #         print(sum(rec.batch_line_ids.mapped('expected_qty')))
    #         print(rec.expected_qty)
    #         print(rec.serlized_bo)
    #         if sum(rec.batch_line_ids.mapped('expected_qty'))!=rec.expected_qty and rec.serlized_bo == False:
    #             raise ValidationError('Total of Batch Expected Quantity must be equal to Sku Expected Quantity!')

    # @api.constrains('lsc_serial_number', 'expected_qty','batch_line_ids')
    # def contraint_On_serial(self):
    #     for rec in self:
    #         if len(rec.lsc_serial_number) > rec.expected_qty:
    #             raise ValidationError('Number of line on serial number have same as expected QTY')
    #         if sum(rec.batch_line_ids.mapped('expected_qty')) != rec.expected_qty and rec.serlized_bo == False:
    #             raise ValidationError('Total of Batch Expected Quantity must be equal to Sku Expected Quantity!')
    # @api.model
    # def create(self, vals):
    #     if 'lsc_serial_number' in vals:
    #         if len(vals['lsc_serial_number'])>vals['expected_qty']:
    #             raise ValidationError("Number of line on serial number have same as expected QTY")
    #     return super(AsnLines, self).create(vals)
    # # number = fields.Integer(
    # #     compute='_compute_get_number',
    # #     store=True,
    # # )
    # def write(self, vals):
    #     if 'expected_qty' not in vals:
    #         if 'lsc_serial_number' in vals:
    #             raise ValidationError("Number of line on serial number have same as expected QTYss")
    #
    #     elif 'expected_qty' in vals and 'lsc_serial_number' in vals:
    #           if len(self.lsc_serial_number)>self.expected_qty:
    #             raise ValidationError("Number of line on serial number have same as expected QTY")
    #     return super(AsnLines, self).write(vals)
    @api.depends('asn_id')
    def _compute_get_number(self):
        for order in self.mapped('asn_id'):
            number = 1
            for line in order.asn_line_ids:
                line.name = number
                number += 1


    @api.depends('sku_id', 'sku_id.sku_type')
    def compute_serialized_bo(self):
        for rec in self:
            rec.serlized_bo = False
            if rec.sku_id.sku_type == 'serilized':
                rec.serlized_bo = True
            else:
                rec.serlized_bo = False

    # commented by raheel todo: check the requirement and fix it
    @api.onchange('sku_id')
    def _onchage_sku(self):
        self.description = self.sku_id.description
        self.pack_key_id = self.sku_id.pack_key.id
        self.uom_id = self.sku_id.uom_id.id
        if self.sku_id.business_id.ids:
            if len(self.sku_id.business_id.ids) == 1:
                self.bussines_unit_id = self.sku_id.business_id[0].id
        else:
            self.bussines_unit_id = False
        # self.bussines_unit_id = self.sku_id.business_id.id
        # self.bus_id = self.sku_id.business_id.id

        # return {'domain': {'bussines_unit_id': [('id', '=', self.sku_id.business_id.ids)]}}

class BatchAsnLines(models.Model):
    _name = 'batch.asn.lines'
    _description = "Batch Asn Lines"

    # def _get_lot7(self):
    #     return self.sku_id.sku_lotable_ids.ids

    expected_qty = fields.Integer('Expected QTY')
    # recieved_qty = fields.Float('Received QTY')
    # balanced_qty = fields.Float('Balanced QTY',related='asn_line_id.balanced_qty', store=True)
    sku_id = fields.Many2one(related='asn_line_id.sku_id', string="SKU", store=True)
    batch_no = fields.Char("Batch No")
    asn_line_id = fields.Many2one('lsc.asn.lines', 'Rel',ondelete='cascade',)
    lot1 = fields.Char("Lot01")
    lot2 = fields.Char('Lot02')
    lot3 = fields.Char('Lot03')
    lot4 = fields.Char('Lot04')
    lot5 = fields.Char('Lot05')
    lot6 = fields.Many2one('sku.lotable.six',string='Lot06')
    lot7 = fields.Many2one('sku.lotable',string='Lot07')
    lot8 = fields.Datetime('Lot08')
    lot9 = fields.Datetime('Lot09')
    lot10 = fields.Datetime('Lot10')
    sku_type = fields.Selection(related='sku_id.sku_type',string="Sky Type",store=True)

class SerialNumber(models.Model):
    _name = 'lsc.serial.number'


    lines_id = fields.Many2one('lsc.asn.lines','Rel',ondelete='cascade',)
    sku_id = fields.Many2one(related='lines_id.sku_id',string="SKU",store=True)
    expected_qty = fields.Integer('Expected QTY',default='1')
    name = fields.Char("Serial Number")
    lot1 = fields.Char("Lot01")
    lot2 = fields.Char('Lot02')
    lot3 = fields.Char('Lot03')
    lot4 = fields.Char('Lot04')
    lot5 = fields.Char('Lot05')
    lot6 = fields.Many2one('sku.lotable.six',string='Lot06')
    lot7 = fields.Many2one('sku.lotable',string='Lot07')
    lot8 = fields.Datetime('Lot08')
    lot9 = fields.Datetime('Lot09')
    lot10 = fields.Datetime('Lot10')

    _sql_constraints = [('serial_number_uniq', 'unique (sku_id,name)',
                         'Duplicate Serial number in the ASN with respect to sku!')]


class asn_confirmation_wizard(models.TransientModel):
    _name = 'asn.confirmation.wizard'
    _description = 'asn_confirmation_wizard'

    asn_id = fields.Many2one('lsc.asn','Asn')
    text = fields.Html(default='<p><span style="color: rgb(156, 0, 0); font-family: Calibri; font-size: 36px; white-space: pre-wrap;">You have not entered Batch or Serial please confirm to Continue!</span><br></p>')

    def create_grn(self):
        self.asn_id.create_grn(confirmation_shown=True)