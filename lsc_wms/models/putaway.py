# -*- coding: utf-8 -*-
import pdb

from odoo import models, fields, api,_
from odoo.exceptions import UserError, ValidationError


class wms_putaway(models.Model):
    _name = 'wms.putaway'
    # _inherit = ['mail.thread', 'mail.activity.mixin', 'mail.render.mixin']
    _description = 'wms_putaway'

    name = fields.Char(string='Document No.', default=lambda self: self.env['ir.sequence'].next_by_code('wms.putaway.seq'))
    grn_id = fields.Many2one('lsc.grn', 'Grn')
    asn_id = fields.Many2one('lsc.asn','Asn')
    # document_no = fields.Char(string='Document No')
    grn_datetime = fields.Datetime(string='Grn DateTime')
    customer_id = fields.Many2one('res.partner', "Customer ")
    warehouse_id = fields.Many2one('lsc.sub.warehouse',store=True,string='Warehouse')
    warehouse_main_id = fields.Many2one(related='warehouse_id.warehouse_id', string='Main Warehouse')
    vendor_id = fields.Many2one('res.partner', "Supplier ")
    asn_datetime = fields.Datetime("ASN Date & time")
    asn_type = fields.Selection(
        [('purchase', 'Purchase'), ('cust_return', 'Customer Return'), ('stock_transfer', 'Stock Transfer')],
        required=1, string="ASN Type")
    external_ref = fields.Char("External Ref")
    po_number = fields.Char("PO Number")
    putaway_line_ids = fields.One2many('wms.putaway.lines','wms_putaway_id',string='Putaway Lines')
    state = fields.Selection([('draft', 'Draft'),('validate', 'Validate')], default='draft', string='State')
    labour_id = fields.Many2one('lsc.labour.master', string='Labour')

    def unlink(self):
        for rec in self:
            if rec.state == 'validate':
                raise ValidationError('You Can not delete Put-away which have Validate Status!')
        res = super(wms_putaway, self).unlink()
        return res

    def validate_putaway(self):
        for rec in self:
            # if any(rec.putaway_line_ids.filtered(lambda b: b.recieved_qty != sum(b.putaway_quantity_ids.mapped('name')))):
            for line in rec.putaway_line_ids:
                if sum(line.batch_location_ids.mapped('putaway_qty')) != line.recieved_qty and line.sku_id.sku_type == 'batch_wise' and line.putaway_locations_no>1:
                    raise ValidationError(_("Putaway quantity of batch line is not equal to batch Line quantity! %s", line.line_no))
                if any(line.batch_location_ids.filtered(lambda b: not b.putaway_location_id and line.sku_id.sku_type == 'batch_wise') and line.putaway_locations_no>1):
                    raise ValidationError(_("Location is not set on batch putaway line! %s", line.line_no))
                    # raise ValidationError(_("Either putaway quantity is not equal to received quantity or Location is not set in all lines! kindly check Line no: %s", line.line_no))
            if any(rec.putaway_line_ids.filtered(lambda b: not b.serial_no and b.sku_id.sku_type == 'serilized')):
                raise ValidationError(
                    "For Serilized sku you must have to enter serial number!")
            elif any(rec.putaway_line_ids.filtered(lambda b: b.serial_no and b.sku_id.sku_type == 'serilized' and not b.putaway_location_id)):
                raise ValidationError(
                    "For sku Line you must have to enter serial number!")
            for line in rec.putaway_line_ids:
                # if line.serial_no:
                #     lot1 = line.serial_no.lot1
                #     lot2 = line.serial_no.lot2
                #     lot3 = line.serial_no.lot3
                #     lot4 = line.serial_no.lot4
                #     lot5 = line.serial_no.lot5
                #     lot6 = line.serial_no.lot6
                #     lot7 = line.serial_no.lot7
                #     lot8 = line.serial_no.lot8
                #     lot9 = line.serial_no.lot9
                #     lot10 = line.serial_no.lot10
                # else:
                #     lot1 = line.batch_id.lot1
                #     lot2 = line.batch_id.lot2
                #     lot3 = line.batch_id.lot3
                #     lot4 = line.batch_id.lot4
                #     lot5 = line.batch_id.lot5
                #     lot6 = line.batch_id.lot6
                #     lot7 = line.batch_id.lot7
                #     lot8 = line.batch_id.lot8
                #     lot9 = line.batch_id.lot9
                #     lot10 = line.batch_id.lot10
                if not line.serial_no and line.sku_id.sku_type != 'serilized':
                    batch_line = self.env['inventory.report'].search([('grn_line_id', '=', line.grn_line_id.id)])
                    batch_line.unlink()
                    # for quantity, location in zip(line.putaway_quantity_ids, line.putaway_location_id):
                    if line.putaway_locations_no > 1:
                        for line_batch in line.batch_location_ids:
                            # print(quantity.name,location.location_name,'quantity,location')
                            transaction_vals = {
                                'quantity': line_batch.putaway_qty,
                                'remarks': line.grn_line_id.remarks,
                                'lot1': line.grn_line_id.lot1,
                                'lot2': line.grn_line_id.lot2,
                                'lot3': line.grn_line_id.lot3,
                                'lot4': line.grn_line_id.lot4,
                                'lot5': line.grn_line_id.lot5,
                                'lot6': line.grn_line_id.lot6.id,
                                'lot7': line.grn_line_id.lot7.id,
                                'lot8': line.grn_line_id.lot8,
                                'lot9': line.grn_line_id.lot9,
                                'lot10': line.grn_line_id.lot10,
                                'serial_no': line.serial_no or False,
                                'description': line.grn_line_id.description,
                                'uom_id': line.grn_line_id.uom_id.id,
                                'sku_id': line.grn_line_id.sku_id.id,
                                'pack_key_id': line.grn_line_id.pack_key_id.id,
                                'bussines_unit_id': line.grn_line_id.bussines_unit_id.id,
                                'batch_no': line.batch_no or False,
                                'manf_date': line.grn_line_id.manf_date,
                                'expiry_date': line.grn_line_id.expiry_date,
                                'lot': line.grn_line_id.lot,
                                'location': line_batch.putaway_location_id.id,
                                'transaction_type': 'putaway',
                                'document_no': rec.name,
                                'customer_id': rec.customer_id.id,
                                'warehouse_id': rec.warehouse_id.id,
                                'date': rec.grn_datetime.date(),
                            }
                            transaction = self.env['wms.transactions'].create(transaction_vals)
                            inventory_report_vals = {
                                'customer_id': rec.customer_id.id,
                                'warehouse_id': rec.warehouse_id.id,
                                'sku_id': line.grn_line_id.sku_id.id,
                                'location': line_batch.putaway_location_id.id,
                                'available_qty': line_batch.putaway_qty,
                                'description': line.grn_line_id.description,
                                'uom_id': line.grn_line_id.uom_id.id,
                                'pack_key_id': line.grn_line_id.pack_key_id.id,
                                'serial_no': line.serial_no or False,
                                'lot': line.grn_line_id.lot,
                                'bussines_unit_id': line.grn_line_id.bussines_unit_id.id,
                                'batch_no': line.batch_no or False,
                                'remarks': line.grn_line_id.remarks,
                                'lot1': line.grn_line_id.lot1,
                                'lot2': line.grn_line_id.lot2,
                                'lot3': line.grn_line_id.lot3,
                                'lot4': line.grn_line_id.lot4,
                                'lot5': line.grn_line_id.lot5,
                                'lot6': line.grn_line_id.lot6.id,
                                'lot7': line.grn_line_id.lot7.id,
                                'lot8': line.grn_line_id.lot8,
                                'lot9': line.grn_line_id.lot9,
                                'lot10': line.grn_line_id.lot10,
                                # 'batch_id': line.batch_id.id or False,
                                # 'serial_no_id': line.serial_no.id or False,
                                'manf_date': line.grn_line_id.manf_date,
                                'expiry_date': line.grn_line_id.expiry_date,
                                'grn_id': rec.grn_id.id,
                                'asn_id': rec.asn_id.id,
                            }
                            inventory_report = self.env['inventory.report'].create(inventory_report_vals)
                    else:
                        transaction_vals = {
                            'quantity': line.putaway_quantity,
                            'remarks': line.grn_line_id.remarks,
                            'lot1': line.grn_line_id.lot1,
                            'lot2': line.grn_line_id.lot2,
                            'lot3': line.grn_line_id.lot3,
                            'lot4': line.grn_line_id.lot4,
                            'lot5': line.grn_line_id.lot5,
                            'lot6': line.grn_line_id.lot6.id,
                            'lot7': line.grn_line_id.lot7.id,
                            'lot8': line.grn_line_id.lot8,
                            'lot9': line.grn_line_id.lot9,
                            'lot10': line.grn_line_id.lot10,
                            'serial_no': line.serial_no or False,
                            'description': line.grn_line_id.description,
                            'uom_id': line.grn_line_id.uom_id.id,
                            'sku_id': line.grn_line_id.sku_id.id,
                            'pack_key_id': line.grn_line_id.pack_key_id.id,
                            'bussines_unit_id': line.grn_line_id.bussines_unit_id.id,
                            'batch_no': line.batch_no or False,
                            'manf_date': line.grn_line_id.manf_date,
                            'expiry_date': line.grn_line_id.expiry_date,
                            'lot': line.grn_line_id.lot,
                            'location': line.putaway_location_id.id,
                            'transaction_type': 'putaway',
                            'document_no': rec.name,
                            'customer_id': rec.customer_id.id,
                            'warehouse_id': rec.warehouse_id.id,
                            'date': rec.grn_datetime.date(),
                        }
                        transaction = self.env['wms.transactions'].create(transaction_vals)
                        inventory_report_vals = {
                            'customer_id': rec.customer_id.id,
                            'warehouse_id': rec.warehouse_id.id,
                            'sku_id': line.grn_line_id.sku_id.id,
                            'location': line.putaway_location_id.id,
                            'available_qty': line.putaway_quantity,
                            'description': line.grn_line_id.description,
                            'uom_id': line.grn_line_id.uom_id.id,
                            'pack_key_id': line.grn_line_id.pack_key_id.id,
                            'serial_no': line.serial_no or False,
                            'lot': line.grn_line_id.lot,
                            'bussines_unit_id': line.grn_line_id.bussines_unit_id.id,
                            'batch_no': line.batch_no or False,
                            'remarks': line.grn_line_id.remarks,
                            'lot1': line.grn_line_id.lot1,
                            'lot2': line.grn_line_id.lot2,
                            'lot3': line.grn_line_id.lot3,
                            'lot4': line.grn_line_id.lot4,
                            'lot5': line.grn_line_id.lot5,
                            'lot6': line.grn_line_id.lot6.id,
                            'lot7': line.grn_line_id.lot7.id,
                            'lot8': line.grn_line_id.lot8,
                            'lot9': line.grn_line_id.lot9,
                            'lot10': line.grn_line_id.lot10,
                            # 'batch_id': line.batch_id.id or False,
                            # 'serial_no_id': line.serial_no.id or False,
                            'manf_date': line.grn_line_id.manf_date,
                            'expiry_date': line.grn_line_id.expiry_date,
                            'grn_id': rec.grn_id.id,
                            'asn_id': rec.asn_id.id,
                        }
                        inventory_report = self.env['inventory.report'].create(inventory_report_vals)
                else:
                    serial_line = self.env['inventory.report'].search([('grn_line_id', '=', line.grn_line_id.id)])
                    serial_line.unlink()
                    # if not line.putaway_location_single:
                    #     raise ValidationError(
                    #                         _('Location on Line is not set!.'))
                    transaction_vals = {
                        'quantity': 1,
                        'remarks': line.grn_line_id.remarks,
                        'lot1': line.grn_line_id.lot1,
                        'lot2': line.grn_line_id.lot2,
                        'lot3': line.grn_line_id.lot3,
                        'lot4': line.grn_line_id.lot4,
                        'lot5': line.grn_line_id.lot5,
                        'lot6': line.grn_line_id.lot6.id,
                        'lot7': line.grn_line_id.lot7.id,
                        'lot8': line.grn_line_id.lot8,
                        'lot9': line.grn_line_id.lot9,
                        'lot10': line.grn_line_id.lot10,
                        'serial_no': line.serial_no or False,
                        'description': line.grn_line_id.description,
                        'uom_id': line.grn_line_id.uom_id.id,
                        'sku_id': line.grn_line_id.sku_id.id,
                        'pack_key_id': line.grn_line_id.pack_key_id.id,
                        'bussines_unit_id': line.grn_line_id.bussines_unit_id.id,
                        # 'batch_no': line.batch_id.batch_no or False,
                        'manf_date': line.grn_line_id.manf_date,
                        'expiry_date': line.grn_line_id.expiry_date,
                        'lot': line.grn_line_id.lot,
                        'location': line.putaway_location_id.id,
                        'transaction_type': 'putaway',
                        'document_no': rec.name,
                        'customer_id': rec.customer_id.id,
                        'warehouse_id': rec.warehouse_id.id,
                        'date': rec.grn_datetime.date(),
                    }
                    transaction = self.env['wms.transactions'].create(transaction_vals)
                    inventory_report_vals = {
                        'customer_id': rec.customer_id.id,
                        'warehouse_id': rec.warehouse_id.id,
                        'sku_id': line.grn_line_id.sku_id.id,
                        'location': line.putaway_location_id.id,
                        'available_qty': 1,
                        'description': line.grn_line_id.description,
                        'uom_id': line.grn_line_id.uom_id.id,
                        'pack_key_id': line.grn_line_id.pack_key_id.id,
                        'serial_no': line.serial_no or False,
                        'lot': line.grn_line_id.lot,
                        'bussines_unit_id': line.grn_line_id.bussines_unit_id.id,
                        'batch_no': line.batch_no or False,
                        'remarks': line.grn_line_id.remarks,
                        'lot1': line.grn_line_id.lot1,
                        'lot2': line.grn_line_id.lot2,
                        'lot3': line.grn_line_id.lot3,
                        'lot4': line.grn_line_id.lot4,
                        'lot5': line.grn_line_id.lot5,
                        'lot6': line.grn_line_id.lot6.id,
                        'lot7': line.grn_line_id.lot7.id,
                        'lot8': line.grn_line_id.lot8,
                        'lot9': line.grn_line_id.lot9,
                        'lot10': line.grn_line_id.lot10,
                        # 'batch_id': line.batch_id.id or False,
                        # 'serial_no_id': line.serial_no.id or False,
                        'manf_date': line.grn_line_id.manf_date,
                        'expiry_date': line.grn_line_id.expiry_date,
                        'grn_id': rec.grn_id.id,
                        'asn_id': rec.asn_id.id,
                    }
                    inventory_report = self.env['inventory.report'].create(inventory_report_vals)
            rec.state = 'validate'


class wms_putaway_lines(models.Model):
    _name = 'wms.putaway.lines'
    # _inherit = ['mail.thread', 'mail.activity.mixin', 'mail.render.mixin']
    _description = 'wms_putaway_lines'

    def _get_default_locaiton_no(self):
        # print(self.serial_no != False, 'print 1')
        # print(self.serial_no == '', 'print 2')
        if self.serial_no != False:
            return 1
        else:
            return 0

    line_no = fields.Integer("S.NO",compute='_compute_get_number',store=True)
    sku_id = fields.Many2one('lsc.sku.master', "SKU")
    batch_no = fields.Char("Batch No")
    # batch_id = fields.Many2one('batch.grn.lines', string='Batch No.')
    serial_no = fields.Char(store=True, index=True,
                                string="Serial Number", readonly=False)
    bussines_unit_id = fields.Many2one('lsc.bussines.unit', string="Business Unit")
    recieved_qty = fields.Float('Received QTY')
    putaway_locations_no = fields.Integer('No. of Putaway Locations', default=_get_default_locaiton_no)
    putaway_location_single = fields.Many2one('lsc.location.master',string='Location')
    putaway_location_id = fields.Many2one('lsc.location.master',string='Putaway Locations',domain="[('warhouse_id','=', warehouse_id)]")
    wms_putaway_id = fields.Many2one('wms.putaway',string='Putaway')
    putaway_locaton_lenth = fields.Integer(string='Putaway location lenth',compute='_for_compute_location',store=True)
    putaway_locaton_qty_sum = fields.Integer(string='Putaway location Qty Sum',store=True)
    grn_line_id = fields.Many2one('lsc.grn.lines',string='Grn Line')
    for_compute = fields.Char(string='For Compute',store=True)
    is_serialized = fields.Boolean(string='Is Serialized?',compute='get_is_serialized',store=True)
    serial_color_red = fields.Boolean(compute='get_serial_color_red',store=True)
    batch_color_red = fields.Boolean(compute='get_batch_color_red',store=True)
    warehouse_id = fields.Many2one(related='wms_putaway_id.warehouse_id', store=True, string='Warehouse')
    putaway_quantity = fields.Integer(string='Putaway Quantity')
    batch_location_ids = fields.One2many('wms.putaway.location','putaway_line_id',string='Batch Locations',ondelete='cascade',domain="[('putaway_line_id','=', id)]")

    def open_putaway_line_form(self):
        for rec in self:
            if not len(rec.batch_location_ids) > 0:
                for x in range(rec.putaway_locations_no):
                    vals = {
                        'putaway_line_id':rec.id,
                    }
                    self.env['wms.putaway.location'].create(vals)
            view_id = self.env.ref('lsc_wms.wms_putaway_lines_view_form_batch').id
            return {
                'type': 'ir.actions.act_window',
                'name': 'Putaway Line',
                'view_mode': 'form',
                'view_type': 'form',
                'view_id': view_id,
                'res_model': "wms.putaway.lines",
                'res_id': self.id,
                'target': 'new',
                # 'flags': {'initial_mode': 'view'}
            }
    @api.depends('serial_no')
    def get_is_serialized(self):
        for rec in self:
            if rec.serial_no:
                rec.is_serialized = True
            else:
                rec.is_serialized = False

    @api.depends('serial_no','putaway_location_id','putaway_locations_no')
    def get_serial_color_red(self):
        for rec in self:
            if not rec.putaway_location_id and rec.putaway_locations_no == 1:
                rec.serial_color_red = True
            else:
                rec.serial_color_red = False

    @api.depends('putaway_locations_no', 'batch_location_ids')
    def get_batch_color_red(self):
        for rec in self:
            if not rec.batch_location_ids and rec.putaway_locations_no > 1:
                rec.batch_color_red = True
            elif rec.batch_location_ids and rec.putaway_locations_no > 1 and any(rec.batch_location_ids.filtered(lambda b: not b.putaway_location_id or b.putaway_qty < 1)):
                rec.batch_color_red = True
            else:
                rec.batch_color_red = False


    @api.depends('putaway_locations_no','recieved_qty')
    def _for_compute_location(self):
        for rec in self:
            if rec.putaway_locations_no > rec.recieved_qty:
                raise ValidationError("No. of putaway locations can not be greater than putaway quantity!")
            # rec.putaway_locaton_lenth = len(rec.putaway_location_id)
            # rec.putaway_locaton_qty_sum = sum(rec.putaway_quantity.mapped('name')) or 0
            # length_of_putaway_qty = len(rec.putaway_quantity)
            # if rec.putaway_locaton_lenth > rec.putaway_locations_no:
            #     raise ValidationError("You can not select locations more than No. of putaway locations!")
            # if rec.putaway_locaton_qty_sum > rec.recieved_qty:
            #     raise ValidationError("You can not select quantity more than Total received quantity!")


    @api.depends('wms_putaway_id')
    def _compute_get_number(self):
        for order in self.mapped('wms_putaway_id'):
            number = 1
            for line in order.putaway_line_ids:
                line.line_no = number
                number += 1

    # @api.constrains('putaway_locaton_lenth','putaway_quantity_ids','putaway_locaton_qty_sum','recieved_qty')
    # def _check_reconcile(self):
    #     for rec in self:
    #         length_of_putaway_qty = len(rec.putaway_quantity_ids)
    #         # if length_of_putaway_qty != rec.putaway_locaton_lenth:
    #         #     raise ValidationError("Number of Locations and Putaway must be the same!")
    #         if rec.putaway_locaton_qty_sum != rec.recieved_qty:
    #             raise ValidationError("Sum of Putaway quantity must be the same of received quantity!")




class wms_putaway_location(models.Model):
    _name = 'wms.putaway.location'
    # _inherit = ['mail.thread', 'mail.activity.mixin', 'mail.render.mixin']
    _description = 'wms_putaway_location'

    putaway_location_id = fields.Many2one('lsc.location.master',string='Putaway Location',domain="[('warhouse_id','=', warehouse_id)]")
    putaway_qty = fields.Integer(string='Quantity')
    putaway_line_id = fields.Many2one('wms.putaway.lines',string='Putaway Line')
    warehouse_id = fields.Many2one(related='putaway_line_id.warehouse_id', store=True, string='Warehouse')

    def name_get(self):
        result = []
        for rec in self:
            name = str(rec.putaway_location_id.location_name)+' '+'||'+''+''+''+' '+'Qty: ' + str(rec.putaway_qty)
            result.append((rec.id, str(name)))
        return result

class putaway_quantity(models.Model):
    _name = 'putaway.quantity'
    _description = 'putaway_quantity'

    name = fields.Integer(string='Putaway Quantity')

