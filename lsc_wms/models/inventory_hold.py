# -*- coding: utf-8 -*-
import pdb

from odoo import models, fields, api
from odoo.exceptions import UserError, ValidationError


class inventory_hold_notes(models.Model):
    _name = 'inventory.hold.notes'
    _description = 'inventory_hold_notes'

    name = fields.Text(string='Note')

class wms_transfers_hold(models.Model):
    _name = 'wms.transfers.hold'
    _inherit = ['mail.thread', 'mail.activity.mixin', 'mail.render.mixin']
    _description = 'wms Transfers hold'
    _rec_name = 'document_no'

    document_no = fields.Char(string='Document Number')
    transaction_date = fields.Date(string='Transaction Date')
    customer_id = fields.Many2one('res.partner',string='Customer')
    warehouse_id = fields.Many2one('lsc.sub.warehouse',store=True,string='Warehouse')
    warehouse_main_id = fields.Many2one(related='warehouse_id.warehouse_id', string='Main Warehouse')
    state = fields.Selection([('draft', 'Draft'), ('hold', 'Hold'), ('unhold', 'UN-hold')], string="Status",
                             default='draft',tracking=True)
    hold_lines_ids = fields.One2many('inventory.hold.line','hold_transfer_id',string='Hold Transfer Moves')
    type = fields.Selection([('hold', 'Hold'), ('unhold', 'Un-Hold')], string="Type", required=True)
    labour_id = fields.Many2one('lsc.labour.master', track_visibility='onchange', string="Labour")
    notes_id = fields.Many2one('inventory.hold.notes',string='Note')

    @api.model
    def create(self, vals):
        transfer_seq = self.env['ir.sequence'].next_by_code('seq.wms.transfer.hold')
        vals['document_no'] = transfer_seq
        res = super(wms_transfers_hold, self).create(vals)
        return res

    def inventory_hold(self):
        for rec in self:
            for line in rec.hold_lines_ids:
                if any(rec.hold_lines_ids.filtered(lambda b: b.to_qty < 1)):
                    raise ValidationError('To Quantity in lines is Zero!')
                if any(rec.hold_lines_ids.filtered(lambda b: b.to_location == False)):
                    raise ValidationError('Add To Location in Lines!')
                if line.serial_no:
                    # domain += [('serial_no', '=', line.serial_no)]
                    inventory_report_vals = {
                        'customer_id': rec.customer_id.id,
                        'sku_id': line.sku_id.id,
                        'location': line.to_location.id,
                        'available_qty': line.to_qty,
                        'description': line.description,
                        'uom_id': line.uom_id.id,
                        'pack_key_id': line.pack_key_id.id,
                        'serial_no': line.serial_no,
                        'lot': line.lot,
                        'bussines_unit_id': line.bussines_unit_id.id,
                        'batch_no': line.batch_no or False,
                        'remarks': line.remarks,
                        'lot1': line.lot1,
                        'lot2': line.lot2,
                        'lot3': line.lot3,
                        'lot4': line.lot4,
                        'lot5': line.lot5,
                        'lot6': line.lot6.id,
                        'lot7': line.lot7.id,
                        'lot8': line.lot8,
                        'lot9': line.lot9,
                        'lot10': line.lot10,
                        'manf_date': line.manf_date,
                        'expiry_date': line.expiry_date,
                        'warehouse_id': line.inventory_report_id.warehouse_id.id,
                    }
                    inventory_report = self.env['inventory.report'].create(inventory_report_vals)
                    transaction_vals = {
                        'quantity': -(line.to_qty),
                        'remarks': line.remarks,
                        'lot1': line.lot1,
                        'lot2': line.lot2,
                        'lot3': line.lot3,
                        'lot4': line.lot4,
                        'lot5': line.lot5,
                        'lot6': line.lot6.id,
                        'lot7': line.lot7.id,
                        'lot8': line.lot8,
                        'lot9': line.lot9,
                        'lot10': line.lot10,
                        'serial_no': line.serial_no or False,
                        'description': line.description,
                        'uom_id': line.uom_id.id,
                        'sku_id': line.sku_id.id,
                        'pack_key_id': line.pack_key_id.id,
                        'bussines_unit_id': line.bussines_unit_id.id,
                        'batch_no': line.batch_no or False,
                        'manf_date': line.manf_date,
                        'expiry_date': line.expiry_date,
                        'lot': line.lot,
                        'location': line.from_location.id,
                        'transaction_type': 'transfers',
                        'document_no': rec.document_no,
                        'customer_id': rec.customer_id.id,
                        'date': fields.date.today(),
                        'warehouse_id': line.inventory_report_id.warehouse_id.id,
                    }
                    transaction = self.env['wms.transactions'].create(transaction_vals)
                    transaction_vals = {
                        'quantity': (line.to_qty),
                        'remarks': line.remarks,
                        'lot1': line.lot1,
                        'lot2': line.lot2,
                        'lot3': line.lot3,
                        'lot4': line.lot4,
                        'lot5': line.lot5,
                        'lot6': line.lot6.id,
                        'lot7': line.lot7.id,
                        'lot8': line.lot8,
                        'lot9': line.lot9,
                        'lot10': line.lot10,
                        'serial_no': line.serial_no or False,
                        'description': line.description,
                        'uom_id': line.uom_id.id,
                        'sku_id': line.sku_id.id,
                        'pack_key_id': line.pack_key_id.id,
                        'bussines_unit_id': line.bussines_unit_id.id,
                        'batch_no': line.batch_no or False,
                        'manf_date': line.manf_date,
                        'expiry_date': line.expiry_date,
                        'lot': line.lot,
                        'location': line.to_location.id,
                        'transaction_type': 'transfers',
                        'document_no': rec.document_no,
                        'customer_id': rec.customer_id.id,
                        'date': fields.date.today(),
                        'warehouse_id': line.inventory_report_id.warehouse_id.id,
                    }
                    transaction = self.env['wms.transactions'].create(transaction_vals)
                    line.inventory_report_id.unlink()
                    # pdb.set_trace()
                else:
                    if line.inventory_report_id.available_qty - line.to_qty == 0:
                        # to_location_inventory_line[0].available_qty += line.to_qty

                        inventory_report_vals = {
                            'customer_id': rec.customer_id.id,
                            'sku_id': line.sku_id.id,
                            'location': line.to_location.id,
                            'available_qty': line.to_qty,
                            'description': line.description,
                            'uom_id': line.uom_id.id,
                            'pack_key_id': line.pack_key_id.id,
                            'serial_no': line.serial_no or False,
                            'lot': line.lot,
                            'bussines_unit_id': line.bussines_unit_id.id,
                            'batch_no': line.batch_no or False,
                            'remarks': line.remarks,
                            'lot1': line.lot1,
                            'lot2': line.lot2,
                            'lot3': line.lot3,
                            'lot4': line.lot4,
                            'lot5': line.lot5,
                            'lot6': line.lot6.id,
                            'lot7': line.lot7.id,
                            'lot8': line.lot8,
                            'lot9': line.lot9,
                            'lot10': line.lot10,
                            'manf_date': line.manf_date,
                            'expiry_date': line.expiry_date,
                            'warehouse_id': line.inventory_report_id.warehouse_id.id,
                        }
                        inventory_report = self.env['inventory.report'].create(inventory_report_vals)
                        transaction_vals = {
                            'quantity': -(line.to_qty),
                            'remarks': line.remarks,
                            'lot1': line.lot1,
                            'lot2': line.lot2,
                            'lot3': line.lot3,
                            'lot4': line.lot4,
                            'lot5': line.lot5,
                            'lot6': line.lot6.id,
                            'lot7': line.lot7.id,
                            'lot8': line.lot8,
                            'lot9': line.lot9,
                            'lot10': line.lot10,
                            'serial_no': line.serial_no or False,
                            'description': line.description,
                            'uom_id': line.uom_id.id,
                            'sku_id': line.sku_id.id,
                            'pack_key_id': line.pack_key_id.id,
                            'bussines_unit_id': line.bussines_unit_id.id,
                            'batch_no': line.batch_no or False,
                            'manf_date': line.manf_date,
                            'expiry_date': line.expiry_date,
                            'lot': line.lot,
                            'location': line.from_location.id,
                            'transaction_type': 'transfers',
                            'document_no': rec.document_no,
                            'customer_id': rec.customer_id.id,
                            'date': fields.date.today(),
                            'warehouse_id': line.inventory_report_id.warehouse_id.id,
                        }
                        transaction = self.env['wms.transactions'].create(transaction_vals)
                        transaction_vals = {
                            'quantity': (line.to_qty),
                            'remarks': line.remarks,
                            'lot1': line.lot1,
                            'lot2': line.lot2,
                            'lot3': line.lot3,
                            'lot4': line.lot4,
                            'lot5': line.lot5,
                            'lot6': line.lot6.id,
                            'lot7': line.lot7.id,
                            'lot8': line.lot8,
                            'lot9': line.lot9,
                            'lot10': line.lot10,
                            'serial_no': line.serial_no or False,
                            'description': line.description,
                            'uom_id': line.uom_id.id,
                            'sku_id': line.sku_id.id,
                            'pack_key_id': line.pack_key_id.id,
                            'bussines_unit_id': line.bussines_unit_id.id,
                            'batch_no': line.batch_no or False,
                            'manf_date': line.manf_date,
                            'expiry_date': line.expiry_date,
                            'lot': line.lot,
                            'location': line.to_location.id,
                            'transaction_type': 'transfers',
                            'document_no': rec.document_no,
                            'customer_id': rec.customer_id.id,
                            'date': fields.date.today(),
                            'warehouse_id': line.inventory_report_id.warehouse_id.id,
                        }
                        transaction = self.env['wms.transactions'].create(transaction_vals)
                        line.inventory_report_id.unlink()
                    elif line.inventory_report_id.available_qty - line.to_qty != 0:
                        line.inventory_report_id.write({'available_qty': line.to_qty})
                        # line.inventory_report_id.available_qty - int(line.to_qty)
                        inventory_report_vals = {
                            'customer_id': rec.customer_id.id,
                            'sku_id': line.sku_id.id,
                            'location': line.to_location.id,
                            'available_qty': line.to_qty,
                            'description': line.description,
                            'uom_id': line.uom_id.id,
                            'pack_key_id': line.pack_key_id.id,
                            'serial_no': line.serial_no or False,
                            'lot': line.lot,
                            'bussines_unit_id': line.bussines_unit_id.id,
                            'batch_no': line.batch_no or False,
                            'remarks': line.remarks,
                            'lot1': line.lot1,
                            'lot2': line.lot2,
                            'lot3': line.lot3,
                            'lot4': line.lot4,
                            'lot5': line.lot5,
                            'lot6': line.lot6.id,
                            'lot7': line.lot7.id,
                            'lot8': line.lot8,
                            'lot9': line.lot9,
                            'lot10': line.lot10,
                            'manf_date': line.manf_date,
                            'expiry_date': line.expiry_date,
                            'warehouse_id': line.inventory_report_id.warehouse_id.id,
                            'source_stock_onhand': line.inventory_report_id.id,

                        }
                        inventory_report = self.env['inventory.report'].create(inventory_report_vals)
                        transaction_vals = {
                            'quantity': -(line.to_qty),
                            'remarks': line.remarks,
                            'lot1': line.lot1,
                            'lot2': line.lot2,
                            'lot3': line.lot3,
                            'lot4': line.lot4,
                            'lot5': line.lot5,
                            'lot6': line.lot6.id,
                            'lot7': line.lot7.id,
                            'lot8': line.lot8,
                            'lot9': line.lot9,
                            'lot10': line.lot10,
                            'serial_no': line.serial_no or False,
                            'description': line.description,
                            'uom_id': line.uom_id.id,
                            'sku_id': line.sku_id.id,
                            'pack_key_id': line.pack_key_id.id,
                            'bussines_unit_id': line.bussines_unit_id.id,
                            'batch_no': line.batch_no or False,
                            'manf_date': line.manf_date,
                            'expiry_date': line.expiry_date,
                            'lot': line.lot,
                            'location': line.from_location.id,
                            'transaction_type': 'transfers',
                            'document_no': rec.document_no,
                            'customer_id': rec.customer_id.id,
                            'date': fields.date.today(),
                            'warehouse_id': line.inventory_report_id.warehouse_id.id,
                        }
                        transaction = self.env['wms.transactions'].create(transaction_vals)
                        transaction_vals = {
                            'quantity': (line.to_qty),
                            'remarks': line.remarks,
                            'lot1': line.lot1,
                            'lot2': line.lot2,
                            'lot3': line.lot3,
                            'lot4': line.lot4,
                            'lot5': line.lot5,
                            'lot6': line.lot6.id,
                            'lot7': line.lot7.id,
                            'lot8': line.lot8,
                            'lot9': line.lot9,
                            'lot10': line.lot10,
                            'serial_no': line.serial_no or False,
                            'description': line.description,
                            'uom_id': line.uom_id.id,
                            'sku_id': line.sku_id.id,
                            'pack_key_id': line.pack_key_id.id,
                            'bussines_unit_id': line.bussines_unit_id.id,
                            'batch_no': line.batch_no or False,
                            'manf_date': line.manf_date,
                            'expiry_date': line.expiry_date,
                            'lot': line.lot,
                            'location': line.to_location.id,
                            'transaction_type': 'transfers',
                            'document_no': rec.document_no,
                            'customer_id': rec.customer_id.id,
                            'date': fields.date.today(),
                            'warehouse_id': line.inventory_report_id.warehouse_id.id,
                        }
                        transaction = self.env['wms.transactions'].create(transaction_vals)
                rec.transaction_date = fields.date.today()
            rec.transaction_date = fields.date.today()
            rec.state = 'hold'


    def inventory_unhold(self):
        for rec in self:
            for line in rec.hold_lines_ids:
                if any(rec.hold_lines_ids.filtered(lambda b: b.to_qty < 1)):
                    raise ValidationError('To Quantity in lines is Zero!')
                if any(rec.hold_lines_ids.filtered(lambda b: b.to_location == False)):
                    raise ValidationError('Add To Location in Lines!')
                if line.serial_no:
                    # domain += [('serial_no', '=', line.serial_no)]
                    inventory_report_vals = {
                        'customer_id': rec.customer_id.id,
                        'sku_id': line.sku_id.id,
                        'location': line.to_location.id,
                        'available_qty': line.to_qty,
                        'description': line.description,
                        'uom_id': line.uom_id.id,
                        'pack_key_id': line.pack_key_id.id,
                        'serial_no': line.serial_no,
                        'lot': line.lot,
                        'bussines_unit_id': line.bussines_unit_id.id,
                        'batch_no': line.batch_no or False,
                        'remarks': line.remarks,
                        'lot1': line.lot1,
                        'lot2': line.lot2,
                        'lot3': line.lot3,
                        'lot4': line.lot4,
                        'lot5': line.lot5,
                        'lot6': line.lot6.id,
                        'lot7': line.lot7.id,
                        'lot8': line.lot8,
                        'lot9': line.lot9,
                        'lot10': line.lot10,
                        'manf_date': line.manf_date,
                        'expiry_date': line.expiry_date,
                        'warehouse_id': line.inventory_report_id.warehouse_id.id,
                    }
                    inventory_report = self.env['inventory.report'].create(inventory_report_vals)
                    transaction_vals = {
                        'quantity': -(line.to_qty),
                        'remarks': line.remarks,
                        'lot1': line.lot1,
                        'lot2': line.lot2,
                        'lot3': line.lot3,
                        'lot4': line.lot4,
                        'lot5': line.lot5,
                        'lot6': line.lot6.id,
                        'lot7': line.lot7.id,
                        'lot8': line.lot8,
                        'lot9': line.lot9,
                        'lot10': line.lot10,
                        'serial_no': line.serial_no or False,
                        'description': line.description,
                        'uom_id': line.uom_id.id,
                        'sku_id': line.sku_id.id,
                        'pack_key_id': line.pack_key_id.id,
                        'bussines_unit_id': line.bussines_unit_id.id,
                        'batch_no': line.batch_no or False,
                        'manf_date': line.manf_date,
                        'expiry_date': line.expiry_date,
                        'lot': line.lot,
                        'location': line.from_location.id,
                        'transaction_type': 'transfers',
                        'document_no': rec.document_no,
                        'customer_id': rec.customer_id.id,
                        'date': fields.date.today(),
                        'warehouse_id': line.inventory_report_id.warehouse_id.id,
                    }
                    transaction = self.env['wms.transactions'].create(transaction_vals)
                    transaction_vals = {
                        'quantity': (line.to_qty),
                        'remarks': line.remarks,
                        'lot1': line.lot1,
                        'lot2': line.lot2,
                        'lot3': line.lot3,
                        'lot4': line.lot4,
                        'lot5': line.lot5,
                        'lot6': line.lot6.id,
                        'lot7': line.lot7.id,
                        'lot8': line.lot8,
                        'lot9': line.lot9,
                        'lot10': line.lot10,
                        'serial_no': line.serial_no or False,
                        'description': line.description,
                        'uom_id': line.uom_id.id,
                        'sku_id': line.sku_id.id,
                        'pack_key_id': line.pack_key_id.id,
                        'bussines_unit_id': line.bussines_unit_id.id,
                        'batch_no': line.batch_no or False,
                        'manf_date': line.manf_date,
                        'expiry_date': line.expiry_date,
                        'lot': line.lot,
                        'location': line.to_location.id,
                        'transaction_type': 'transfers',
                        'document_no': rec.document_no,
                        'customer_id': rec.customer_id.id,
                        'date': fields.date.today(),
                        'warehouse_id': line.inventory_report_id.warehouse_id.id,
                    }
                    transaction = self.env['wms.transactions'].create(transaction_vals)
                    line.inventory_report_id.unlink()
                else:
                    if line.inventory_report_id.available_qty - line.to_qty == 0:
                        # to_location_inventory_line[0].available_qty += line.to_qty
                        if line.inventory_report_id.source_stock_onhand and line.inventory_report_id.source_stock_onhand.location.id == line.to_location.id:
                            line.inventory_report_id.source_stock_onhand.write({'available_qty': line.inventory_report_id.source_stock_onhand.available_qty + line.to_qty})
                            transaction_vals = {
                                'quantity': -(line.to_qty),
                                'remarks': line.remarks,
                                'lot1': line.lot1,
                                'lot2': line.lot2,
                                'lot3': line.lot3,
                                'lot4': line.lot4,
                                'lot5': line.lot5,
                                'lot6': line.lot6.id,
                                'lot7': line.lot7.id,
                                'lot8': line.lot8,
                                'lot9': line.lot9,
                                'lot10': line.lot10,
                                'serial_no': line.serial_no or False,
                                'description': line.description,
                                'uom_id': line.uom_id.id,
                                'sku_id': line.sku_id.id,
                                'pack_key_id': line.pack_key_id.id,
                                'bussines_unit_id': line.bussines_unit_id.id,
                                'batch_no': line.batch_no or False,
                                'manf_date': line.manf_date,
                                'expiry_date': line.expiry_date,
                                'lot': line.lot,
                                'location': line.from_location.id,
                                'transaction_type': 'transfers',
                                'document_no': rec.document_no,
                                'customer_id': rec.customer_id.id,
                                'date': fields.date.today(),
                                'warehouse_id': line.inventory_report_id.warehouse_id.id,
                            }
                            transaction = self.env['wms.transactions'].create(transaction_vals)
                            transaction_vals = {
                                'quantity': (line.to_qty),
                                'remarks': line.remarks,
                                'lot1': line.lot1,
                                'lot2': line.lot2,
                                'lot3': line.lot3,
                                'lot4': line.lot4,
                                'lot5': line.lot5,
                                'lot6': line.lot6.id,
                                'lot7': line.lot7.id,
                                'lot8': line.lot8,
                                'lot9': line.lot9,
                                'lot10': line.lot10,
                                'serial_no': line.serial_no or False,
                                'description': line.description,
                                'uom_id': line.uom_id.id,
                                'sku_id': line.sku_id.id,
                                'pack_key_id': line.pack_key_id.id,
                                'bussines_unit_id': line.bussines_unit_id.id,
                                'batch_no': line.batch_no or False,
                                'manf_date': line.manf_date,
                                'expiry_date': line.expiry_date,
                                'lot': line.lot,
                                'location': line.to_location.id,
                                'transaction_type': 'transfers',
                                'document_no': rec.document_no,
                                'customer_id': rec.customer_id.id,
                                'date': fields.date.today(),
                                'warehouse_id': line.inventory_report_id.warehouse_id.id,
                            }
                            transaction = self.env['wms.transactions'].create(transaction_vals)
                            line.inventory_report_id.unlink()
                        else:
                            inventory_report_vals = {
                                'customer_id': rec.customer_id.id,
                                'sku_id': line.sku_id.id,
                                'location': line.to_location.id,
                                'available_qty': line.to_qty,
                                'description': line.description,
                                'uom_id': line.uom_id.id,
                                'pack_key_id': line.pack_key_id.id,
                                'serial_no': line.serial_no or False,
                                'lot': line.lot,
                                'bussines_unit_id': line.bussines_unit_id.id,
                                'batch_no': line.batch_no or False,
                                'remarks': line.remarks,
                                'lot1': line.lot1,
                                'lot2': line.lot2,
                                'lot3': line.lot3,
                                'lot4': line.lot4,
                                'lot5': line.lot5,
                                'lot6': line.lot6.id,
                                'lot7': line.lot7.id,
                                'lot8': line.lot8,
                                'lot9': line.lot9,
                                'lot10': line.lot10,
                                'manf_date': line.manf_date,
                                'expiry_date': line.expiry_date,
                                'warehouse_id': line.inventory_report_id.warehouse_id.id,
                            }
                            inventory_report = self.env['inventory.report'].create(inventory_report_vals)
                            transaction_vals = {
                                'quantity': -(line.to_qty),
                                'remarks': line.remarks,
                                'lot1': line.lot1,
                                'lot2': line.lot2,
                                'lot3': line.lot3,
                                'lot4': line.lot4,
                                'lot5': line.lot5,
                                'lot6': line.lot6.id,
                                'lot7': line.lot7.id,
                                'lot8': line.lot8,
                                'lot9': line.lot9,
                                'lot10': line.lot10,
                                'serial_no': line.serial_no or False,
                                'description': line.description,
                                'uom_id': line.uom_id.id,
                                'sku_id': line.sku_id.id,
                                'pack_key_id': line.pack_key_id.id,
                                'bussines_unit_id': line.bussines_unit_id.id,
                                'batch_no': line.batch_no or False,
                                'manf_date': line.manf_date,
                                'expiry_date': line.expiry_date,
                                'lot': line.lot,
                                'location': line.from_location.id,
                                'transaction_type': 'transfers',
                                'document_no': rec.document_no,
                                'customer_id': rec.customer_id.id,
                                'date': fields.date.today(),
                                'warehouse_id': line.inventory_report_id.warehouse_id.id,
                            }
                            transaction = self.env['wms.transactions'].create(transaction_vals)
                            transaction_vals = {
                                'quantity': (line.to_qty),
                                'remarks': line.remarks,
                                'lot1': line.lot1,
                                'lot2': line.lot2,
                                'lot3': line.lot3,
                                'lot4': line.lot4,
                                'lot5': line.lot5,
                                'lot6': line.lot6.id,
                                'lot7': line.lot7.id,
                                'lot8': line.lot8,
                                'lot9': line.lot9,
                                'lot10': line.lot10,
                                'serial_no': line.serial_no or False,
                                'description': line.description,
                                'uom_id': line.uom_id.id,
                                'sku_id': line.sku_id.id,
                                'pack_key_id': line.pack_key_id.id,
                                'bussines_unit_id': line.bussines_unit_id.id,
                                'batch_no': line.batch_no or False,
                                'manf_date': line.manf_date,
                                'expiry_date': line.expiry_date,
                                'lot': line.lot,
                                'location': line.to_location.id,
                                'transaction_type': 'transfers',
                                'document_no': rec.document_no,
                                'customer_id': rec.customer_id.id,
                                'date': fields.date.today(),
                                'warehouse_id': line.inventory_report_id.warehouse_id.id,
                            }
                            transaction = self.env['wms.transactions'].create(transaction_vals)
                            line.inventory_report_id.unlink()
                    elif line.inventory_report_id.available_qty - line.to_qty != 0:
                        # line.inventory_report_id.available_qty - line.to_qty
                        if line.inventory_report_id.source_stock_onhand and line.inventory_report_id.source_stock_onhand.location.id == line.to_location.id:
                            line.inventory_report_id.source_stock_onhand.write({'available_qty': line.inventory_report_id.source_stock_onhand.available_qty + line.to_qty})
                            line.inventory_report_id.write({'available_qty': line.inventory_report_id.available_qty - line.to_qty})
                            transaction_vals = {
                                'quantity': -(line.to_qty),
                                'remarks': line.remarks,
                                'lot1': line.lot1,
                                'lot2': line.lot2,
                                'lot3': line.lot3,
                                'lot4': line.lot4,
                                'lot5': line.lot5,
                                'lot6': line.lot6.id,
                                'lot7': line.lot7.id,
                                'lot8': line.lot8,
                                'lot9': line.lot9,
                                'lot10': line.lot10,
                                'serial_no': line.serial_no or False,
                                'description': line.description,
                                'uom_id': line.uom_id.id,
                                'sku_id': line.sku_id.id,
                                'pack_key_id': line.pack_key_id.id,
                                'bussines_unit_id': line.bussines_unit_id.id,
                                'batch_no': line.batch_no or False,
                                'manf_date': line.manf_date,
                                'expiry_date': line.expiry_date,
                                'lot': line.lot,
                                'location': line.from_location.id,
                                'transaction_type': 'transfers',
                                'document_no': rec.document_no,
                                'customer_id': rec.customer_id.id,
                                'date': fields.date.today(),
                                'warehouse_id': line.inventory_report_id.warehouse_id.id,
                            }
                            transaction = self.env['wms.transactions'].create(transaction_vals)
                            transaction_vals = {
                                'quantity': (line.to_qty),
                                'remarks': line.remarks,
                                'lot1': line.lot1,
                                'lot2': line.lot2,
                                'lot3': line.lot3,
                                'lot4': line.lot4,
                                'lot5': line.lot5,
                                'lot6': line.lot6.id,
                                'lot7': line.lot7.id,
                                'lot8': line.lot8,
                                'lot9': line.lot9,
                                'lot10': line.lot10,
                                'serial_no': line.serial_no or False,
                                'description': line.description,
                                'uom_id': line.uom_id.id,
                                'sku_id': line.sku_id.id,
                                'pack_key_id': line.pack_key_id.id,
                                'bussines_unit_id': line.bussines_unit_id.id,
                                'batch_no': line.batch_no or False,
                                'manf_date': line.manf_date,
                                'expiry_date': line.expiry_date,
                                'lot': line.lot,
                                'location': line.to_location.id,
                                'transaction_type': 'transfers',
                                'document_no': rec.document_no,
                                'customer_id': rec.customer_id.id,
                                'date': fields.date.today(),
                                'warehouse_id': line.inventory_report_id.warehouse_id.id,
                            }
                            transaction = self.env['wms.transactions'].create(transaction_vals)
                        else:
                            line.inventory_report_id.write(
                                {'available_qty': line.inventory_report_id.available_qty - line.to_qty})
                            inventory_report_vals = {
                                'customer_id': rec.customer_id.id,
                                'sku_id': line.sku_id.id,
                                'location': line.to_location.id,
                                'available_qty': line.to_qty,
                                'description': line.description,
                                'uom_id': line.uom_id.id,
                                'pack_key_id': line.pack_key_id.id,
                                'serial_no': line.serial_no or False,
                                'lot': line.lot,
                                'bussines_unit_id': line.bussines_unit_id.id,
                                'batch_no': line.batch_no or False,
                                'remarks': line.remarks,
                                'lot1': line.lot1,
                                'lot2': line.lot2,
                                'lot3': line.lot3,
                                'lot4': line.lot4,
                                'lot5': line.lot5,
                                'lot6': line.lot6.id,
                                'lot7': line.lot7.id,
                                'lot8': line.lot8,
                                'lot9': line.lot9,
                                'lot10': line.lot10,
                                'manf_date': line.manf_date,
                                'expiry_date': line.expiry_date,
                                'warehouse_id': line.inventory_report_id.warehouse_id.id,
                            }
                            inventory_report = self.env['inventory.report'].create(inventory_report_vals)
                            transaction_vals = {
                                'quantity': -(line.to_qty),
                                'remarks': line.remarks,
                                'lot1': line.lot1,
                                'lot2': line.lot2,
                                'lot3': line.lot3,
                                'lot4': line.lot4,
                                'lot5': line.lot5,
                                'lot6': line.lot6.id,
                                'lot7': line.lot7.id,
                                'lot8': line.lot8,
                                'lot9': line.lot9,
                                'lot10': line.lot10,
                                'serial_no': line.serial_no or False,
                                'description': line.description,
                                'uom_id': line.uom_id.id,
                                'sku_id': line.sku_id.id,
                                'pack_key_id': line.pack_key_id.id,
                                'bussines_unit_id': line.bussines_unit_id.id,
                                'batch_no': line.batch_no or False,
                                'manf_date': line.manf_date,
                                'expiry_date': line.expiry_date,
                                'lot': line.lot,
                                'location': line.from_location.id,
                                'transaction_type': 'transfers',
                                'document_no': rec.document_no,
                                'customer_id': rec.customer_id.id,
                                'date': fields.date.today(),
                                'warehouse_id': line.inventory_report_id.warehouse_id.id,
                            }
                            transaction = self.env['wms.transactions'].create(transaction_vals)
                            transaction_vals = {
                                'quantity': (line.to_qty),
                                'remarks': line.remarks,
                                'lot1': line.lot1,
                                'lot2': line.lot2,
                                'lot3': line.lot3,
                                'lot4': line.lot4,
                                'lot5': line.lot5,
                                'lot6': line.lot6.id,
                                'lot7': line.lot7.id,
                                'lot8': line.lot8,
                                'lot9': line.lot9,
                                'lot10': line.lot10,
                                'serial_no': line.serial_no or False,
                                'description': line.description,
                                'uom_id': line.uom_id.id,
                                'sku_id': line.sku_id.id,
                                'pack_key_id': line.pack_key_id.id,
                                'bussines_unit_id': line.bussines_unit_id.id,
                                'batch_no': line.batch_no or False,
                                'manf_date': line.manf_date,
                                'expiry_date': line.expiry_date,
                                'lot': line.lot,
                                'location': line.to_location.id,
                                'transaction_type': 'transfers',
                                'document_no': rec.document_no,
                                'customer_id': rec.customer_id.id,
                                'date': fields.date.today(),
                                'warehouse_id': line.inventory_report_id.warehouse_id.id,
                            }
                            transaction = self.env['wms.transactions'].create(transaction_vals)
            rec.transaction_date = fields.date.today()
            rec.state = 'unhold'


    # def hold_unhold(self):
    #     for rec in self:
    #         if rec.state != 'hold':
    #             rec.state = 'hold'
    #         else:
    #             rec.state = 'draft'

    @api.model
    def open_transfer_wizard_hold(self):
        return {
            'name': "Hold Transfer",
            'type': 'ir.actions.act_window',
            'view_mode': 'form',
            'view_type': 'form',
            'res_model': 'hold.transfers.wizard',
            'views': [(False, 'form')],
            'target': 'new',
        }

class inventory_hold_line(models.Model):
    _name = 'inventory.hold.line'
    _inherit = ['mail.thread', 'mail.activity.mixin', 'mail.render.mixin']
    _description = 'inventory.hold.line'

    hold_transfer_id = fields.Many2one('wms.transfers.hold',string='Document Number')
    transaction_date = fields.Date(related='hold_transfer_id.transaction_date',string='Transaction Date',store=True)
    customer_id = fields.Many2one(related='hold_transfer_id.customer_id', string='Customer',store=True)
    warehouse_id = fields.Many2one(related='hold_transfer_id.warehouse_id', store=True, string='Warehouse')
    type = fields.Selection(related='hold_transfer_id.type', string="Type")
    sku_id = fields.Many2one('lsc.sku.master', string="SKU")
    description = fields.Char(string='Description')
    uom_id = fields.Many2one('uom.uom', string='UOM')
    batch_no = fields.Char(string="Batch No")
    serial_no = fields.Char(string="Serial No")
    bussines_unit_id = fields.Many2one('lsc.bussines.unit', string="Business Unit")
    location_type = fields.Many2one(related='from_location.location_type', string='Location Type', store=True)
    manf_date = fields.Date(string="Manufacturing Date")
    expiry_date = fields.Date("Expiry Date")
    lot = fields.Char("Lot No")
    lot1 = fields.Char("Lottable01")
    lot2 = fields.Char('Lottable02')
    lot3 = fields.Char('Lottable03')
    lot4 = fields.Char('Lottable04')
    lot5 = fields.Char('Lottable05')
    lot6 = fields.Many2one('sku.lotable.six',string='Lottable06')
    lot7 = fields.Many2one('sku.lotable',string='Lot07')
    lot8 = fields.Datetime('Lottable08')
    lot9 = fields.Datetime('Lottable09')
    lot10 = fields.Datetime('Lottable10')
    remarks = fields.Char("Remarks")

    from_location = fields.Many2one('lsc.location.master', string='From Location')
    from_location_id = fields.Integer(related='from_location.id', string='From Location id',store=True)
    to_location_ids = fields.Many2many('lsc.location.master', string='To Location', compute='get_to_location_ids')
    to_location = fields.Many2one('lsc.location.master', string='To Location',domain="[('id','!=',from_location_id),('id','in',to_location_ids)]")
    from_qty = fields.Float(string='From Quantity', store=True)
    to_qty = fields.Float(string='To Quantity')
    state = fields.Selection(related='hold_transfer_id.state',string='Status')
    pack_key_id = fields.Many2one('lsc.pack.key', 'Pack Key')
    inventory_report_id = fields.Many2one('inventory.report', string='Inventory Report ID')


    reason = fields.Text(string='Hold Reason')


    @api.constrains('to_qty', 'serial_no','from_qty')
    def _check_to_quantity_hold(self):
        if self.serial_no and self.to_qty > 1:
            raise ValidationError('You can not add Quantity more than one with serial no!')
        if self.to_qty > self.from_qty:
            raise ValidationError('You can not add To Quantity more than From Quantity!')

    @api.depends('type')
    def get_to_location_ids(self):
        for rec in self:
            if rec.type == 'hold':
                hold_locations = self.env['lsc.location.master'].search([('location_status','!=','good')]).mapped('id')
                rec.to_location_ids = hold_locations
            elif rec.type == 'unhold':
                unhold_locations = self.env['lsc.location.master'].search([('location_status', '=', 'good')]).mapped(
                    'id')
                rec.to_location_ids = unhold_locations
    # def show_detail(self):
    #     self.ensure_one()
    #     return {
    #         'name': "WMS Transfer Form",
    #         'type': 'ir.actions.act_window',
    #         'view_type': 'form',
    #         'view_mode': 'form',
    #         'res_model': 'wms.transfers',
    #         'res_id': self.wms_transfer_id.id,
    #     }

class hold_transfers_wizard(models.TransientModel):
    _name = 'hold.transfers.wizard'
    _inherit = ['mail.thread', 'mail.activity.mixin', 'mail.render.mixin']
    _description = 'hold_transfers_wizard'

    customer_id = fields.Many2one('res.partner', string='Customer',domain="[('wms','=',True),('customer_rank','=',1)]",required=True)
    sku_id = fields.Many2one('lsc.sku.master', string="SKU")
    batch_no = fields.Char(string="Batch No")
    serial_no = fields.Char(string="Serial No")
    bussines_unit_id = fields.Many2one('lsc.bussines.unit', string="Business Unit")
    warehouse_id = fields.Many2one('lsc.sub.warehouse', store=True, string='Sub Warehouse', required=True)
    from_location = fields.Many2one('lsc.location.master',string='From Location')
    wizard_line_ids = fields.One2many('hold.wizard.lines','hold_wizard_id',string='Inventory Report Lines',readonly=False)
    type = fields.Selection([('hold', 'Hold'), ('unhold', 'Un-Hold')], string="Type",required=True)


    @api.onchange('sku_id', 'batch_no', 'serial_no', 'bussines_unit_id', 'from_location','customer_id','type')
    def get_inventory_report_lines(self):
        for rec in self:
            domain = []
            if rec.customer_id:
                domain += [('customer_id', '=', rec.customer_id.id)]
            if rec.sku_id:
                domain += [('sku_id', '=', rec.sku_id.id)]
            if rec.batch_no:
                domain += [('batch_no', '=', rec.batch_no)]
            if rec.serial_no:
                domain += [('serial_no', '=', rec.serial_no)]
            if rec.bussines_unit_id:
                domain += [('bussines_unit_id', '=', rec.bussines_unit_id.id)]
            if rec.from_location:
                domain += [('location', '=', rec.from_location.id)]
            if rec.warehouse_id:
                domain += [('warehouse_id', '=', rec.warehouse_id.id)]
            if rec.type == 'hold':
                location_types = self.env['lsc.location.type.master'].search([('location_status', '=', 'good')]).mapped('id')
                domain += [('location_type', 'in', location_types)]
            if rec.type == 'unhold':
                location_types = self.env['lsc.location.type.master'].search([('location_status', '!=', 'good')]).mapped(
                    'id')
                domain += [('location_type', 'in', location_types)]
            if domain != []:
                # wizard_lines_previous = self.env['hold.wizard.lines'].search([])
                # wizard_lines_previous.unlink()
                from_locaiton_inventory_line = self.env['inventory.report'].search(domain)
                # rec.wizard_line_ids = from_locaiton_inventory_line.ids
                wizard_lines = self.env['hold.wizard.lines']
                for line in from_locaiton_inventory_line.filtered(lambda b: b.available_qty > 0):
                    vals = {
                        'customer_id': line.customer_id.id,
                        'sku_id': line.sku_id.id,
                        'serial_no': line.serial_no or False,
                        'bussines_unit_id': line.bussines_unit_id.id,
                        'batch_no': line.batch_no or False,
                        'from_location': line.location.id or False,
                        'hold_wizard_id': rec.id,
                        'inventory_report_id': line.id,
                        'qty': line.available_qty,
                    }
                    wizard_line = self.env['hold.wizard.lines'].create(vals)
                    wizard_lines += wizard_line

                rec.wizard_line_ids = wizard_lines.ids
            else:
                rec.wizard_line_ids = False

    def map_inventory_report_lines(self):
        for rec in self:
            wizard_line = self.env['hold.wizard.lines'].search([('id', 'in', rec.wizard_line_ids.ids)])
            for line in wizard_line:
                print(line.select)
            # pdb.set_trace()

            transfer_vals = {
                'customer_id': rec.customer_id.id or False,
                'type': rec.type,
                'warehouse_id': rec.warehouse_id.id or False,
            }
            wms_inventory_hold = self.env['wms.transfers.hold'].create(transfer_vals)
            for line in rec.wizard_line_ids.filtered(lambda b: b.select == True):
                transfer_line_vals = {
                    'sku_id': line.inventory_report_id.sku_id.id,
                    'from_location': line.inventory_report_id.location.id,
                    'from_qty': line.inventory_report_id.available_qty,
                    'description': line.inventory_report_id.description,
                    'uom_id': line.inventory_report_id.uom_id.id,
                    'serial_no': line.inventory_report_id.serial_no or False,
                    'lot': line.inventory_report_id.lot,
                    'bussines_unit_id': line.inventory_report_id.bussines_unit_id.id,
                    'batch_no': line.inventory_report_id.batch_no or False,
                    'remarks': line.inventory_report_id.remarks,
                    'pack_key_id': line.inventory_report_id.pack_key_id.id,
                    'lot1': line.inventory_report_id.lot1,
                    'lot2': line.inventory_report_id.lot2,
                    'lot3': line.inventory_report_id.lot3,
                    'lot4': line.inventory_report_id.lot4,
                    'lot5': line.inventory_report_id.lot5,
                    'lot6': line.inventory_report_id.lot6.id,
                    'lot7': line.inventory_report_id.lot7.id,
                    'lot8': line.inventory_report_id.lot8,
                    'lot9': line.inventory_report_id.lot9,
                    'lot10': line.inventory_report_id.lot10,
                    'manf_date': line.inventory_report_id.manf_date,
                    'expiry_date': line.inventory_report_id.expiry_date,
                    'inventory_report_id': line.inventory_report_id.id,
                    'hold_transfer_id': wms_inventory_hold.id,
                }
                hold_inventory_line = self.env['inventory.hold.line'].create(transfer_line_vals)


class hold_wizard_lines(models.TransientModel):
    _name = 'hold.wizard.lines'
    _description = 'hold_wizard_lines'

    customer_id = fields.Many2one('res.partner', string='Customer')
    sku_id = fields.Many2one('lsc.sku.master', string="SKU")
    batch_no = fields.Char(string="Batch No")
    serial_no = fields.Char(string="Serial No")
    bussines_unit_id = fields.Many2one('lsc.bussines.unit', string="Business Unit")
    from_location = fields.Many2one('lsc.location.master', string='From Location')
    hold_wizard_id = fields.Many2one('hold.transfers.wizard',string='Hold Wizard Id')
    inventory_report_id = fields.Many2one('inventory.report',string='Inventory Report ID')
    select = fields.Boolean(string='Select')
    qty = fields.Float(string='Qty Available')