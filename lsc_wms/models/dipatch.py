from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError


class Dispatch(models.Model):
    _name = 'lsc.dispatch'
    _inherit = ['mail.thread', 'mail.activity.mixin', 'mail.render.mixin']
    _description = "Dispatch"

    name = fields.Char("DN", readonly=1)
    order_type = fields.Selection([
        ('delivery', 'Delivery'),
        ('vendor_return', 'Vendor Return'),
        ('stock_trnasfer', 'Stock Transfer')], string="Order Type", readonly=1)
    dipatch_date = fields.Date("Dispatch Date")
    order_id_alloc = fields.Many2one('lsc.allocation', 'Dispatch', readonly=1)
    order_outbound_id = fields.Many2one('lsc.outbound.orders', readonly=1)
    external_ref = fields.Char("External Ref", readonly=1)
    invoice_number = fields.Char("Invoice Number", readonly=1)
    order_date_time = fields.Datetime("Order Date time", readonly=1)
    dispatch_date = fields.Date("Dispatch Date")
    customer_id = fields.Many2one('res.partner', "Customer", domain="[('customer_rank','>',0)]", readonly=1)
    supplier_id = fields.Many2one('res.partner', "Supplier/Vendor", domain="[('supplier_rank','>',0)]", readonly=1)
    remarks = fields.Text("Remarks", readonly=1)
    udf1 = fields.Char("UDF1", readonly=1)
    udf2 = fields.Char("UDF2", readonly=1)
    udf3 = fields.Char("UDF3", readonly=1)
    transporter_name = fields.Char("Transporter Name",track_visibility='onchange')
    notes = fields.Text("Notes")
    vehicle = fields.Char("Vehicle No",track_visibility='onchange')
    labour_id = fields.Many2one('lsc.labour.master',track_visibility='onchange' ,string="Labour")
    tracking_no = fields.Char("Tracking No")
    order_ids = fields.One2many('lsc.dispatch.line', 'order_id', string="Order Lines")
    state = fields.Selection(
        [('draft', 'Draft'), ('part_dispatch', 'Part Dispatch'), ('dispatched', 'Dispatched'), ('close', 'Close'),('cancel','cancel')],
        string='state',default='draft')

    cancel_comments= fields.Char('Cancel comments',readonly=1)

    dispatch_id = fields.Many2one('lsc.dispatch',string="Dispatch ")
    # cancel_bo = fields.Boolean('Cancel Before',compute='_compute_cancel')

    def create_dispatch_wizard(self):
        view_id = self.env.ref('lsc_wms.lsc_return_wizard_view_for_dispatch').id
        return {
            'name': 'Return Wizard',
            'type': 'ir.actions.act_window',
            'res_model': 'lsc.return.wizard',
            'view_mode': 'form',
            'view_id': view_id,
            'target': 'new'}
    def create_return_wizard(self):
        return {
            'name': 'Return Wizard',
            'type': 'ir.actions.act_window',
            'res_model': 'lsc.return.wizard',
            'view_mode': 'form',
            'view_id': False,
            'target': 'new'}
    def cancel_wizard(self):
            return {
                'name': 'Cancel Comment',
                'type': 'ir.actions.act_window',
                'res_model': 'lsc.cancel.comments',
                'view_mode': 'form',
                'view_id': False,
                'target': 'new'}

    def dispatch_open(self):
        self.ensure_one()
        action = self.env["ir.actions.actions"]._for_xml_id("lsc_wms.action_lsc_dispatch")
        scraps = self.env['lsc.dispatch'].search([('dispatch_id', '=', self.id)])
        action['domain'] = [('id', 'in', scraps.ids)]
        return action
    def cancel(self):
        self.state='cancel'
        for line in self.order_ids:
            line.write({
                'dispatch_qty':0.0
            })

        msg = 'The   ' + str(
            ' Dispatch Created against this allocation has been cancel: <a href=# data-oe-model=lsc.dispatch data-oe-id=%d>%s</a> '
         ) % (
                  self.id, self.name)
        self.order_id_alloc.message_post(body=msg)
        # self.cancel_before=True
    @api.model
    def create(self, vals):
        dispatch_seq = self.env['ir.sequence'].next_by_code('dispatch.seq')
        vals['name'] = dispatch_seq
        res = super(Dispatch, self).create(vals)
        return res

    def create_dispatch(self):
        # if any(order_line.balance > 0.0
        #        for order_line in self.order_ids):
        #     self.state = 'part_dispatch'
        #
        # else:
        #     self.state='dispatched'
        for order in self.order_ids:
            order.inv_report_id.picked_qty-=order.dispatch_qty
            msg = 'The   ' +str(order.dispatch_qty)+'has been dispatched from'+ str(
                ' : <a href=# data-oe-model=lsc.dispatch data-oe-id=%d>%s</a> '
            ) % (
                      self.id, self.name)
            if order.inv_report_id:
                order.inv_report_id.message_post(body=msg)
            msg = 'the qty' + str(order.dispatch_qty) + ' for sku ' + str(order.sku_id.code) + 'has been picked'
            order.order_id.message_post(body=msg)
            order._asign_state()
        if any(disptch_line.state == 'part_shipped' for disptch_line in self.order_ids):
            self.state='part_dispatch'
        elif all(disptch_line.state == 'shipped' for disptch_line in self.order_ids):
            self.state='dispatched'






class DispatchLine(models.Model):
    _name = 'lsc.dispatch.line'

    order_id = fields.Many2one('lsc.dispatch', "Rel", ondelete='cascade', )
    line_no = fields.Integer("Line no")
    sku_id = fields.Many2one('lsc.sku.master', "SKU")
    description = fields.Char("Description")
    location = fields.Many2one('lsc.location.master', string='Location')
    warehouse_id = fields.Many2one('lsc.sub.warehouse', store=True, string='Warehouse')
    inv_report_id = fields.Many2many('inventory.report', string="Inventory Report")
    order_line_id = fields.Many2one('lsc.order.line', "Order Line ID")
    uom_id = fields.Many2one('uom.uom', "UOM")
    dispatch_qty = fields.Float("Dispatch QTY")
    qty = fields.Float("Quantity")
    balance = fields.Float("Balance")
    pack_key_id = fields.Many2one('lsc.pack.key', "Pack key")
    batch_number = fields.Char("Batch No")
    serilizad_bo = fields.Boolean("Serilized Bo")
    serial_no = fields.Char("Serial Number")
    bussines_id = fields.Many2one('lsc.bussines.unit', string="Business Unit")
    allocation_line_id = fields.Many2one('lsc.allocation.line')
    vehicle_no = fields.Char("Vehicle No")

    notes = fields.Text("Notes")
    remarks = fields.Char("Remarks")
    lot1 = fields.Char("Lottable01")
    lot2 = fields.Char('Lottable02')
    lot3 = fields.Char('Lottable03')
    lot4 = fields.Char('Lottable04')
    lot5 = fields.Char('Lottable05')
    lot6 = fields.Many2one('sku.lotable.six',string='Lottable06')
    lot7 = fields.Many2one('sku.lotable',string='Lot07')
    lot8 = fields.Datetime('Lottable08')
    lot9 = fields.Datetime('Lottable09')
    lot10 = fields.Datetime('Lottable10')
    lot = fields.Char("Lot")
    return_qty = fields.Float('Return QTY',readonly=1)
    dispatch_qty_wizard = fields.Float("Dispatch QTY WIzard",readonly=1)
    other_dispatch_ref = fields.Many2one('lsc.dispatch.line')
    state = fields.Selection([('draft','draft'),('shipped','Shipped'),('part_shipped','Part Shipped'),('return','Return')],string='state',default='draft')



    @api.onchange('dispatch_qty','return_qty','dispatch_qty_wizard')
    def _compute_balance(self):
        total_balance=self.dispatch_qty+self.return_qty+self.dispatch_qty_wizard
        self.balance=self.qty-total_balance


    def _asign_state(self):
        order_line = self.search([('order_line_id', '=', self.order_line_id.id)])
        total_dispatch = sum(order_line.mapped('dispatch_qty'))
        if self.balance==0 and total_dispatch==self.order_line_id.qty:
            self.state='shipped'
        elif self.balance==0 and total_dispatch!=self.order_line_id.qty:
            self.state='part_shipped'
        elif self.balance>0 and self.balance==self.qty:
            self.state='draft'
        elif self.balance>0 and self.balance!=self.qty:
            self.state='part_shipped'

        if total_dispatch==self.order_line_id.qty:
            self.order_line_id.state='shipped'
        elif total_dispatch<self.order_line_id.qty:
            self.order_line_id.state='part_shipped'
    def return_dispatch(self):
        if self.balance>0:
            if self.inv_report_id:
                self.inv_report_id.picked_qty-=self.balance

                self.inv_report_id.available_qty+=self.balance
                self.return_qty = self.balance
                self.state='return'
                msg = 'The   '+str(self.balance)+ str(
                    ' has been return from dispatch: <a href=# data-oe-model=lsc.dispatch data-oe-id=%d>%s</a> '
                ) % (
                          self.order_id.id, self.order_id.name)
                self.inv_report_id.message_post(body=msg)
                transcation_report = self.env['wms.transactions'].create({
                    'transaction_type': 'transfers',
                    'document_no': self.order_id.name,
                    'date': fields.date.today(),
                    'customer_id': self.order_id.customer_id.id,
                    'warehouse_id': self.warehouse_id.id,
                    'sku_id': self.sku_id.id,
                    'description': self.description,
                    'uom_id': self.uom_id.id,
                    'pack_key_id': self.pack_key_id.id,
                    'batch_no': self.batch_number,
                    'manf_date': False,
                    'expiry_date': False,
                    'bussines_unit_id': self.bussines_id.id,
                    'quantity': self.balance,
                    'lot1': self.lot1,
                    'lot2': self.lot2,
                    'lot3': self.lot3,
                    'lot4': self.lot4,
                    'lot5': self.lot5,
                    'lot6': self.lot6.id,
                    'lot7': self.lot7.id,
                    'lot8': self.lot8,
                    'lot9': self.lot9,
                    'lot10': self.lot10,
                    'remarks': self.remarks,
                    'lot': self.lot,
                    'serial_no': self.serial_no,
                    'location': self.location.id

                })

    def action_return(self):
        if self.balance>0:

            self.inv_report_id.picked_qty-=self.return_qty
            self.inv_report_id.available_qty+=self.return_qty
            self.balance-=self.return_qty
            self.state='return'

class ReturnLines(models.Model):
    _name = 'return.lines'

    sku_id = fields.Many2one('lsc.sku.master', "SKU")
    description = fields.Char("Description")
    location = fields.Many2one('lsc.location.master', string='Location')
    warehouse_id = fields.Many2one('lsc.sub.warehouse', store=True, string='Warehouse')
    return_qty = fields.Float('Return QTY')
    other_dispatch_ref = fields.Many2one('lsc.dispatch.line')
    balance = fields.Float("Balance")
    qty=fields.Float("qty")
    dispatch_qty= fields.Float("Dispatch QTY")


