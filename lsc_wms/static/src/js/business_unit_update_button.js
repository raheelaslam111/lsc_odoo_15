odoo.define('lsc_wms.WmsButtonBuUpdate', function (require) {
"use strict";

var WmsButtonControllerBuUpdate = require('lsc_wms.WmsBuUpdate');
var ListView = require('web.ListView');
var viewRegistry = require('web.view_registry');

var WmsButtonBuUpdate = ListView.extend({
    config: _.extend({}, ListView.prototype.config, {
        Controller: WmsButtonControllerBuUpdate
    })
});

viewRegistry.add('wms_wizard_button_bu_update', WmsButtonBuUpdate);

});