odoo.define('lsc_wms.WmsTransferButtonAdjustment', function (require) {
"use strict";

var WmsTransferButtonControllerAdjustment = require('lsc_wms.WmsTransferAdjustment');
var ListView = require('web.ListView');
var viewRegistry = require('web.view_registry');

var WmsTransferButtonAdjustment = ListView.extend({
    config: _.extend({}, ListView.prototype.config, {
        Controller: WmsTransferButtonControllerAdjustment
    })
});

viewRegistry.add('wms_transfer_wizard_button_adjustment', WmsTransferButtonAdjustment);

});