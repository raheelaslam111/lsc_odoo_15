odoo.define('lsc_wms.WmsTransferAdjustment', function (require) {
"use strict";

var core = require('web.core');
var ListController = require('web.ListController');

var _t = core._t;
var qweb = core.qweb;

var WmsTransferControllerAdjustment = ListController.extend({
    events: _.extend({
        'click .o_button_wms_transfer_adjustment': '_onTransferButtonAdjustment'
    }, ListController.prototype.events),
    /**
     * @override
     */
    init: function (parent, model, renderer, params) {
        var context = renderer.state.getContext();
//        this.inventory_id = context.active_id;
        return this._super.apply(this, arguments);
    },

    // -------------------------------------------------------------------------
    // Public
    // -------------------------------------------------------------------------

    /**
     * @override
     */
    renderButtons: function () {
        this._super.apply(this, arguments);
        var $validationButton = $(qweb.render('WmsTransfer.ButtonsAdjustment'));
        this.$buttons.prepend($validationButton);
    },

    // -------------------------------------------------------------------------
    // Handlers
    // -------------------------------------------------------------------------

    /**
     * Handler called when user click on validation button in inventory lines
     * view. Makes an rpc to try to validate the inventory, then will go back on
     * the inventory view form if it was validated.
     * This method could also open a wizard in case something was missing.
     *
     * @private
     */
    _onTransferButtonAdjustment: function () {
        var self = this;
        var prom = Promise.resolve();
        var recordID = this.renderer.getEditableRecordID();
        if (recordID) {
            // If user's editing a record, we wait to save it before to try to
            // validate the inventory.
            prom = this.saveRecord(recordID);
        }

        prom.then(function () {
            self._rpc({
                model: 'wms.inventory.adjustment',
                method: 'open_transfer_wizard_adjustment',
                context: self.context,
                }).then(function (res) {
                var exitCallback = function (infos) {
                    // In case we discarded a wizard, we do nothing to stay on
                    // the same view...
                    if (infos && infos.special) {
                        return;
                    }
                    // ... but in any other cases, we go back on the inventory form.
                    self.reload();
                    self.displayNotification({ message: _t("The Line is Created!") });
                    self.trigger_up('history_back');
                };

                if (_.isObject(res)) {
                    self.do_action(res, { on_close: exitCallback });
                } else {
                        this.displayNotification({ message: _t("All the quality checks have been done!") });
                    }
            });
        });
    },
});

return WmsTransferControllerAdjustment;

});
