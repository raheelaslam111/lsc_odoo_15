odoo.define('lsc_wms.WmsTransferButtonView', function (require) {
"use strict";

var WmsTransferButtonController = require('lsc_wms.WmsTransferButton');
var ListView = require('web.ListView');
var viewRegistry = require('web.view_registry');

var WmsTransferButtonView = ListView.extend({
    config: _.extend({}, ListView.prototype.config, {
        Controller: WmsTransferButtonController
    })
});

viewRegistry.add('wms_transfer_wizard_button', WmsTransferButtonView);

});