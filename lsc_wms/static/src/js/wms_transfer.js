odoo.define('lsc_wms.WmsTransferButton', function (require) {
"use strict";

var core = require('web.core');
var ListController = require('web.ListController');

var _t = core._t;
var qweb = core.qweb;
var FormController = require('web.FormController');
//var mydata = $serial_barcode_scanner.val();
//        console.log(mydata);
//        document.onclick = $serial_barcode_scanner.focus();

//var x = document.getElementById("mySelect").value;

//function myFunction() {
//  var x = document.getElementById("fname");
//  x.value = x.value.toUpperCase();
//  console.log('bbbbb');
//}



var WmsTransferController = ListController.extend({
    events: _.extend({
        'click .o_button_wms_transfer': '_onTransferButton'
    }, ListController.prototype.events),
    /**
     * @override
     */
    init: function (parent, model, renderer, params) {
        var context = renderer.state.getContext();
//        this.inventory_id = context.active_id;
        return this._super.apply(this, arguments);
    },

    // -------------------------------------------------------------------------
    // Public
    // -------------------------------------------------------------------------

    /**
     * @override
     */
    renderButtons: function () {
        this._super.apply(this, arguments);
        var $validationButton = $(qweb.render('WmsTransfer.Buttons'));
        this.$buttons.prepend($validationButton);
    },

    // -------------------------------------------------------------------------
    // Handlers
    // -------------------------------------------------------------------------

    /**
     * Handler called when user click on validation button in inventory lines
     * view. Makes an rpc to try to validate the inventory, then will go back on
     * the inventory view form if it was validated.
     * This method could also open a wizard in case something was missing.
     *
     * @private
     */
    _onTransferButton: function () {
        var self = this;
        var prom = Promise.resolve();
        var recordID = this.renderer.getEditableRecordID();
        if (recordID) {
            // If user's editing a record, we wait to save it before to try to
            // validate the inventory.
            prom = this.saveRecord(recordID);
        }

        prom.then(function () {
            self._rpc({
                model: 'wms.transfers',
                method: 'open_transfer_wizard',
                context: self.context,
                }).then(function (res) {
                var exitCallback = function (infos) {
                    // In case we discarded a wizard, we do nothing to stay on
                    // the same view...
                    if (infos && infos.special) {
                        return;
                    }
                    // ... but in any other cases, we go back on the inventory form.
                    self.reload();
                    self.displayNotification({ message: _t("The Line is Created!") });
                    self.trigger_up('history_back');
                };

                if (_.isObject(res)) {
                    self.do_action(res, { on_close: exitCallback });
                } else {
                        this.displayNotification({ message: _t("All the quality checks have been done!") });
                    }
            });
        });
    },
});

return WmsTransferController;

});




//odoo.define('lsc_wms.web_widget_sparklines_barchart', function (require) {
//"use strict";
//    console.log('1 abcd ppp ppp');
//    var fieldRegistry = require('web.field_registry');
//    var AbstractField = require('web.AbstractField');
//    var fields = require('web.basic_fields');
//    var basicFields = require('web.basic_fields');
////    var myTextBox = document.getElementById('mySelect');
////    console.log(myTextBox);
////    myTextBox.addEventListener('keyup', function(){
////        //do some stuff
////        console.log('abcd ppp ppp');
////    });
//
//    var SparklinesChartWidget = basic_fields.InputField.extend({
//
//
//
//
////    init: function () {
////        this._super.apply(this, arguments);
////
////        if (this.mode === 'edit') {
////            this.className += ' o_edit_mode';
////        }
////        console.log('1 abcd ppp ppp');
////    },
//
////        supportedFieldTypes: ['char'],
////        jsLibs: ['/web_widget_sparklines_barchart/static/src/lib/jquery.sparkline.js'],
////        start: function() {
////            var barchart = "abcd";
////            this.$el.append(barchart);
////            var $textarea = $('<textarea>');
////            $textarea.addClass('custom_text_area');
////            $textarea.css('box-sizing', 'border-box');
////            $textarea.appendTo(this.$el);
////
////            return this._super.apply(this, arguments);
////        },
////        _render: function () {
//////            $.sparkline_display_force_visible();
////        },
//        events: _.extend({}, basic_fields.InputField.prototype.events, {
//		'click .serial_barcode_scanner': '_SerialInput',
//            'keyup serial_barcode_scanner': '_onInputKeyup',
//    }),
////        events: _.extend({}, AbstractField.prototype.custom_events, {
////            'input .serial_barcode_scanner': '_SerialInput',
////            'keyup input': '_onInputKeyup',
////        }),
//
////        _onClick: function (ev) {
////            ev.preventDefault();
//////            var $taxGroupElement = $(ev.target).parents('.oe_tax_group_editable');
////            // Show input and hide previous element
//////            $taxGroupElement.find('.tax_group_edit').addClass('d-none');
//////            $taxGroupElement.find('.tax_group_edit_input').removeClass('d-none');
////            var $input = $taxGroupElement.find('.tax_group_edit_input input');
////            // Get original value and display it in user locale in the input
////            var formatedOriginalValue = fieldUtils.format.float($input.data('originalValue'), {}, {});
////            $input.focus(); // Focus the input
////            $input.val(formatedOriginalValue); //add value in user locale to the input
////        },
//
////        mounted() {
////        document.addEventListener('keyup', this._onClickGlobalSerial, true);
////        }
////
////        _onClickGlobalSerial: function(ev) {
////            console.log('Alhamdullilah');
////            }
//
//         _SerialInput: function(event) {
//            console.log('serial Input');
////            return this._super.apply(this, arguments);
//        },
//
//        _onInputKeyup: function (ev) {
////        var x = this.$input.val();
//        console.log('input val');
////        return this._super.apply(this, arguments);
//    },
//
//
//    });
//
//    fieldRegistry.add('sparklines_chart', SparklinesChartWidget);
//
//    return {
//        SparklinesChartWidget: SparklinesChartWidget
//    };
//
//});
