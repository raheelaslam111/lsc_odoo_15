odoo.define('lsc_wms.WmsButtonLottablesUpdate', function (require) {
"use strict";

var WmsButtonControllerLottablesUpdate = require('lsc_wms.WmsLottablesUpdate');
var ListView = require('web.ListView');
var viewRegistry = require('web.view_registry');

var WmsButtonLottablesUpdate = ListView.extend({
    config: _.extend({}, ListView.prototype.config, {
        Controller: WmsButtonControllerLottablesUpdate
    })
});

viewRegistry.add('wms_wizard_button_lottables_update', WmsButtonLottablesUpdate);

});