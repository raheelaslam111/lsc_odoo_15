odoo.define('lsc_wms.WmsTransferButtonViewHold', function (require) {
"use strict";

var WmsTransferButtonControllerHold = require('lsc_wms.WmsTransferHold');
var ListView = require('web.ListView');
var viewRegistry = require('web.view_registry');

var WmsTransferButtonViewHold = ListView.extend({
    config: _.extend({}, ListView.prototype.config, {
        Controller: WmsTransferButtonControllerHold
    })
});

viewRegistry.add('wms_transfer_wizard_button_hold', WmsTransferButtonViewHold);

});