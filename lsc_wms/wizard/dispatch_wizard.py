from odoo import models, fields, api, _
from datetime import datetime
from dateutil.relativedelta import relativedelta
from odoo.exceptions import except_orm
from odoo.exceptions import UserError
from odoo.exceptions import RedirectWarning, UserError, ValidationError, AccessError



class DispatchedWizard(models.TransientModel):
    _name = "lsc.dispatched.wizard"
    _description = "Dispatched wizard"


    def dispatched_default(self):
        active_id = self._context.get('active_id', False)
        dispatch_id = self.env['lsc.dispatch'].browse(active_id)
        return dispatch_id
    dispatched_id = fields.Many2one('lsc.dispatch',string="Dispatched",readonly=1,default=dispatched_default)
    dispached_line_id = fields.Many2many('lsc.dispatch.line',string="Dispatched line")

    @api.onchange('dispatched_id')
    def _onchange_dispatch_id(self):
        dispatch_line = self.env['lsc.dispatch.line'].search([('order_id','=',self.dispatched_id.id),('state','!=','return'),('balance','>',0)])
        self.dispached_line_id = dispatch_line.ids

    def create_dispatch_record(self):
        print("KKKKK")
    #
    # def create_dispatch_record(self):
    #     print("yasir")
        line_lst = []
        order_lines = self.dispached_line_id.filtered(lambda b: b.balance > 0)
        for line in order_lines:
            create_lines = self.create_pick_line(line)
            # print(line.picked_qty)
            # balance = line.inv_report_id.allocated_qty- line.picked_qty
            # line.inv_report_id.allocated_qty =balance

            line_lst.append(create_lines)
        create_pick_rec = self.env['lsc.dispatch'].create({
                'order_outbound_id': self.dispatched_id.order_outbound_id.id,
                'order_id_alloc': self.dispatched_id.order_id_alloc.id,
                'order_date_time': self.dispatched_id.order_date_time,
                'customer_id': self.dispatched_id.customer_id.id,
                'supplier_id': self.dispatched_id.supplier_id.id,
                'external_ref': self.dispatched_id.external_ref,
                'invoice_number': self.dispatched_id.invoice_number,
                'order_type': self.dispatched_id.order_type,
                'remarks': self.dispatched_id.remarks,
                'udf1': self.dispatched_id.udf1,
                'udf2': self.dispatched_id.udf2,
                'udf3': self.dispatched_id.udf3,
                 'dispatch_id':self.dispatched_id.id,
                'order_ids': line_lst,

            })
        # self.dispatched_id.dispatch_id = create_pick_rec.id
        self.dispatched_id.state='close'

        # if any(disptch_line.balance != 0 for disptch_line in self.order_ids):
        #     self.state = 'allocated_partially'
        # else:
        #     self.state = 'picked'

    def create_pick_line(self, line):
        lines = ((0, 0, {
            'sku_id': line.sku_id.id,
            'other_dispatch_ref':line.id,
            'description': line.description,
            'allocation_line_id': line.allocation_line_id.id,
            'inv_report_id':line.inv_report_id.ids,
            'uom_id': line.uom_id.id,
            'qty': line.balance,
            'batch_number': line.batch_number.id if line.batch_number else False,
            'serial_no': line.serial_no,
            'lot': line.lot,
            'bussines_id': line.bussines_id.id,
            'remarks': line.remarks,
            'pack_key_id': line.pack_key_id,
            'lot1': line.lot1,
            'lot2': line.lot2,
            'lot3': line.lot3,
            'lot4': line.lot4,
            'lot5': line.lot5,
            'lot6': line.lot6,
            'lot7': line.lot7,
            'lot8': line.lot8,
            'lot9': line.lot9,
            'lot10': line.lot10,
            'location': line.location.id,
            'warehouse_id': line.warehouse_id.id,
            # 'inv_report_id':line.inv_report_id.id,
            'order_line_id': line.order_line_id.id
        }))
        # line.balance

        return lines

class ReturnWizard(models.TransientModel):
    _name = "lsc.return.wizard"
    _description = "Return wizard"

    def dispatched_default(self):
        active_id = self._context.get('active_id', False)
        dispatch_id = self.env['lsc.dispatch'].browse(active_id)
        return dispatch_id

    dispatched_id = fields.Many2one('lsc.dispatch', string="Dispatched", default=dispatched_default)
    dispached_line_id = fields.Many2many('return.lines', string="Dispatched line")

    @api.onchange('dispatched_id')
    def _onchange_dispatch_id(self):

        dispatch_line = self.env['lsc.dispatch.line'].search(
            [('order_id', '=', self.dispatched_id.id),('state','!=','return') ,('balance', '>', 0)])
        disp_lst=[]
        for dispatch in dispatch_line:
            return_line = self.env['return.lines'].create({
                'sku_id':dispatch.sku_id.id,
                'description':dispatch.description,
                'location':dispatch.location.id,
                'balance':dispatch.balance,
                'dispatch_qty':dispatch.dispatch_qty,
                'warehouse_id':dispatch.warehouse_id.id,
                'other_dispatch_ref':dispatch.id,
                'balance':dispatch.balance

            })
            disp_lst.append(return_line.id)
        self.dispached_line_id=[(6, 0, disp_lst)]


    def action_confirm(self):

        if self.env.context.get('dispatch')==True:
            line_lst = []
            order_lines = self.dispached_line_id
            for line in order_lines:
                if line.balance<line.return_qty:
                    raise ValidationError("Dispatch QTY Should be less or equal to balance")
                line.other_dispatch_ref.dispatch_qty_wizard+=line.return_qty
                total_balance = line.other_dispatch_ref.dispatch_qty_wizard+line.other_dispatch_ref.return_qty+line.return_qty
                line.other_dispatch_ref.balance=line.other_dispatch_ref.qty-total_balance
                create_lines = self.create_pick_line(line)
                # print(line.picked_qty)
                # balance = line.inv_report_id.allocated_qty- line.picked_qty
                # line.inv_report_id.allocated_qty =balance

                line_lst.append(create_lines)
            create_pick_rec = self.env['lsc.dispatch'].create({
                'order_outbound_id': self.dispatched_id.order_outbound_id.id,
                'order_id_alloc': self.dispatched_id.order_id_alloc.id,
                'order_date_time': self.dispatched_id.order_date_time,
                'customer_id': self.dispatched_id.customer_id.id,
                'supplier_id': self.dispatched_id.supplier_id.id,
                'external_ref': self.dispatched_id.external_ref,
                'invoice_number': self.dispatched_id.invoice_number,
                'order_type': self.dispatched_id.order_type,
                'remarks': self.dispatched_id.remarks,
                'udf1': self.dispatched_id.udf1,
                'udf2': self.dispatched_id.udf2,
                'udf3': self.dispatched_id.udf3,
                'dispatch_id': self.dispatched_id.id,
                'order_ids': line_lst,

            })
            # self.dispatched_id.dispatch_id = create_pick_rec.id
            self.dispatched_id.state = 'close'
        else:
            for line in self.dispached_line_id:
                if line.balance<line.return_qty:
                    raise ValidationError("Dispatch QTY Should be less or equal to balance")
                line.other_dispatch_ref.return_qty+=line.return_qty
                total_balance = (line.other_dispatch_ref.return_qty)+line.other_dispatch_ref.dispatch_qty
                line.other_dispatch_ref.balance=line.other_dispatch_ref.qty-total_balance
                line.other_dispatch_ref.inv_report_id.picked_qty-=line.return_qty
                line.other_dispatch_ref.inv_report_id.available_qty += line.return_qty
                if line.other_dispatch_ref.balance==0:
                    line.other_dispatch_ref.state='return'

            # line.action_return()


    def create_pick_line(self, line):
        lines = ((0, 0, {
            'sku_id': line.other_dispatch_ref.sku_id.id,
            'other_dispatch_ref':line.other_dispatch_ref.id,
            'description': line.other_dispatch_ref.description,
            'allocation_line_id': line.other_dispatch_ref.allocation_line_id.id,
            'inv_report_id':line.other_dispatch_ref.inv_report_id.ids,
            'uom_id': line.other_dispatch_ref.uom_id.id,
            'qty': line.return_qty,
            'balance':line.return_qty,
            'batch_number': line.other_dispatch_ref.batch_number.id if line.other_dispatch_ref.batch_number else False,
            'serial_no': line.other_dispatch_ref.serial_no,
            'lot': line.other_dispatch_ref.lot,
            'bussines_id': line.other_dispatch_ref.bussines_id.id,
            'remarks': line.other_dispatch_ref.remarks,
            'pack_key_id': line.other_dispatch_ref.pack_key_id.id,
            'lot1': line.other_dispatch_ref.lot1,
            'lot2': line.other_dispatch_ref.lot2,
            'lot3': line.other_dispatch_ref.lot3,
            'lot4': line.other_dispatch_ref.lot4,
            'lot5': line.other_dispatch_ref.lot5,
            'lot6': line.other_dispatch_ref.lot6,
            'lot7': line.other_dispatch_ref.lot7,
            'lot8': line.other_dispatch_ref.lot8,
            'lot9': line.other_dispatch_ref.lot9,
            'lot10': line.other_dispatch_ref.lot10,
            'location': line.other_dispatch_ref.location.id,
            'warehouse_id': line.other_dispatch_ref.warehouse_id.id,
            # 'inv_report_id':line.inv_report_id.id,
            'order_line_id': line.other_dispatch_ref.order_line_id.id
        }))
        # line.balance

        return lines
    # def action_confirm(self):
    #     active_id = self._context.get('active_id', False)
    #     if active_id:
    #         req= self.env['lsc.change.request'].browse(active_id)
    #         req.write({
    #             'manager_comments':self.comments,
    #             'state':'manger_approved'
    #         })

