from odoo import models, fields, api, _
from datetime import datetime
from dateutil.relativedelta import relativedelta
from odoo.exceptions import except_orm
from odoo.exceptions import UserError


class PriorityWizard(models.TransientModel):
    _name = "change.request.priority.wizard"
    _description = "Manager Comments"

    change_req_ids = fields.Many2many('lsc.change.request',string="Change Request")


    def action_confirm(self):
        active_id = self._context.get('active_id', False)
        if active_id:
            req= self.env['lsc.change.request'].browse(active_id)
            req.write({
                'manager_comments':self.comments,
                'state':'manger_approved'
            })

