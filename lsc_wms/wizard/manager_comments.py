from odoo import models, fields, api, _
from datetime import datetime
from dateutil.relativedelta import relativedelta
from odoo.exceptions import except_orm
from odoo.exceptions import UserError,ValidationError


class ManagerComements(models.TransientModel):
    _name = "lsc.cancel.comments"
    _description = "Manager Comments"

    comments = fields.Text(string="Comments", required=True)

    def action_confirm(self):
        active_id = self._context.get('active_id', False)
        active_model = self.env.context.get('active_model')
        record = self.env[active_model].browse(active_id)
        if active_model=='lsc.dispatch':
            dispatch= self.env['lsc.dispatch'].browse(active_id)
            dispatch.cancel()
            dispatch.order_id_alloc.write({
                'dispatch_bo':False
            })
            for order in dispatch.order_id_alloc.order_ids:
                order.write({
                    # 'picked_qty':0,
                    'balance':order.qty-order.picked_qty
                })

                # cancel()
            # dispatch.order_id_alloc.write({
            #     'cancel_comments':self.comments
            # })

            # dispatch.order_id.order_id.write({
            #     'comments':self.comments
            # })
            # dispatch.order_id.order_id.cancel()
            dispatch.write({
                'cancel_comments':self.comments
            })
        elif active_model=='lsc.allocation':

            allocation = self.env['lsc.allocation'].browse(active_id)
            if allocation.dispatch_id:
                if allocation.dispatch_id.state!='cancel':
                    raise ValidationError("To Cancel the allocation please cancel dispatch first")
                else:
                    allocation.cancel()
                    allocation.write({
                        'cancel_comments': self.comments,
                        # 'state': 'cancel'

                    })

            else:
                allocation.cancel()
                allocation.write({
                    'cancel_comments':self.comments,
                    'state':'cancel'

                })
        elif active_model=='lsc.outbound.orders':

            out_bound= self.env['lsc.outbound.orders'].browse(active_id)
            if out_bound.allocation_id:
                if out_bound.allocation_id.state!='cancel':
                    raise ValidationError("Please Cancel the allocation first")
            if out_bound.allocation_id.dispatch_id:
                    if out_bound.allocation_id.dispatch_id.state!='cancel':
                        raise ValidationError("Please cancel the dispatched ")

            out_bound.cancel()
            out_bound.write({
                'comments':self.comments
            })
            # out_bound.allocation_id.write({
            #     'cancel_comments': self.comments
            # })
            # out_bound.allocation_id.cancel()
            #
            # out_bound.allocation_id.dispatch_id.write({
            #     'cancel_comments':self.comments
            # })
            # out_bound.allocation_id.dispatch_id.cancel()


