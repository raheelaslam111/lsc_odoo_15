from odoo import models, fields, api, _
from datetime import datetime
from dateutil.relativedelta import relativedelta
from odoo.exceptions import except_orm
from odoo.exceptions import UserError


class ProcessComments(models.TransientModel):
    _name = "change.request.process.comments"
    _description = "Process Owner Comments"

    comments = fields.Text(string="Comments", required=True)

    def action_confirm(self):
        active_id = self._context.get('active_id', False)
        if active_id:
            req= self.env['lsc.change.request'].browse(active_id)
            req.write({
                'process_owner_comments':self.comments,
                'state':'not_start'
            })

