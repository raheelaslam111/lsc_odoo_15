# -*- coding: utf-8 -*-
# See LICENSE file for full copyright and licensing details.

{
    'name': 'LSC WMS',
    'version': '14.0.0.18',
    'summary': 'LSC WMS',
    'description': """
        Helps you to manage Change Request in odoo ERP.
        """,
    'category': 'IT/Support',
    'author': 'LSC IT Dept',
    'website': 'http://lsclogisitics.com/',
    'depends': ['base','mail','account','stock','sale'],
    'data': [
        'security/security_user_role.xml',
        'security/ir.model.access.csv',
        'data/lot_sequence.xml',
        'data/server_action.xml',
        'report/grn_batch_report.xml',
        'views/labour_master.xml',
        'views/labour_master_category.xml',
        'views/location_master.xml',
        'views/site_master.xml',
        'views/sub_warehouse.xml',
        'views/location_heirarichy.xml',
        'views/location_level.xml',
        'views/location_2_customer.xml',
        'views/sku_category.xml',
        'views/sku_sub_category.xml',
        'data/email_template.xml',
        'views/pack_key.xml',
        'views/sku_master.xml',
        'views/customer_lottables.xml',
        'views/warehouse_master.xml',
        'views/bussiness_unit.xml',
        'views/transactions.xml',
        'data/asn_seq.xml',
        'views/asn_view.xml',
        'wizard/manager_comments_view.xml',
        'wizard/dispatched_wizard.xml',
        'wizard/return_wizard.xml',
        'views/grn_view.xml',
        'views/putaway.xml',
        'views/transfers.xml',
        'views/inventory_report.xml',
        'views/hold_transfers.xml',
        'views/inventory_adjustment.xml',
        'views/lottables_update.xml',
        'views/business_unit_update.xml',
         'views/out_bound_view.xml',
        'views/allocation_view.xml',
        'views/pick_view.xml',
        'views/dispatch.xml',
        # 'views/assets.xml',
        'views/user_role.xml',
        'views/menu_items.xml'





    ],
    'demo': [],
    'installable': True,
    'auto_install': False,
    'assets': {
        'web.assets_backend': [
            '/lsc_wms/static/src/js/wms_transfer.js',
            '/lsc_wms/static/src/js/wms_transfer_button_view.js',
            '/lsc_wms/static/src/js/wms_transfer_hold.js',
            '/lsc_wms/static/src/js/wms_transfer_button_view_hold.js',
            '/lsc_wms/static/src/js/wms_transfer_adjustment.js',
            '/lsc_wms/static/src/js/wms_transfer_button_view_adjustment.js',
            '/lsc_wms/static/src/js/lottables_update.js',
            '/lsc_wms/static/src/js/lottables_update_button.js',
            '/lsc_wms/static/src/js/business_unit_update.js',
            '/lsc_wms/static/src/js/business_unit_update_button.js',

            '/lsc_wms/static/src/js/web_tree_dynamic_colored_field.js',
            '/lsc_wms/static/src/css/color.css',
        ],
        'web.assets_qweb': [
            '/lsc_wms/static/src/xml/transfer_button.xml',
            '/lsc_wms/static/src/xml/transfer_button_hold.xml',
            '/lsc_wms/static/src/xml/adjustmant_button.xml',
            '/lsc_wms/static/src/xml/lottables_update_button.xml',
            '/lsc_wms/static/src/xml/business_unit_update_button.xml',
        ],
    },
    # 'qweb': [
    #         'static/src/xml/transfer_button.xml',
    #         'static/src/xml/transfer_button_hold.xml',
    #         'static/src/xml/adjustmant_button.xml',
    #         'static/src/xml/lottables_update_button.xml',
    #         'static/src/xml/business_unit_update_button.xml',
    #     ],
}
#result = -payslip.loan_amount