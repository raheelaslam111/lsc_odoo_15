from odoo import models, fields, api


class GRNLotSelection(models.Model):
    _name = 'lsc.grn.lot.selection.wizard'

    no_of_lot = fields.Selection([('0', '0'), ('1', '1'), ('2', '2'), ('3', '3'), ('4', '4'), ('5', '5'),
                                  ('6', '6'), ('7', '7'), ('8', '8'), ('9', '9'), ('10', '10')], string='No Of Lot To Prints', default='0', required=1)

    lottable1 = fields.Boolean()
    lottable2 = fields.Boolean()
    lottable3 = fields.Boolean()
    lottable4 = fields.Boolean()
    lottable5 = fields.Boolean()
    lottable6 = fields.Boolean()
    lottable7 = fields.Boolean()
    lottable8 = fields.Boolean()
    lottable9 = fields.Boolean()
    lottable10 = fields.Boolean()

    def print_grn(self):
        active_id = self._context.get('active_id', False)
        if active_id:
            req = self.env['lsc.grn'].browse(active_id)
            no_of_lot = 0
            if self.lottable1:
                no_of_lot = no_of_lot + 1
            if self.lottable2:
                no_of_lot = no_of_lot + 1
            if self.lottable3:
                no_of_lot = no_of_lot + 1
            if self.lottable4:
                no_of_lot = no_of_lot + 1
            if self.lottable5:
                no_of_lot = no_of_lot + 1
            if self.lottable6:
                no_of_lot = no_of_lot + 1
            if self.lottable7:
                no_of_lot = no_of_lot + 1
            if self.lottable8:
                no_of_lot = no_of_lot + 1
            if self.lottable9:
                no_of_lot = no_of_lot + 1
            if self.lottable10:
                no_of_lot = no_of_lot + 1

            print("Tot Lot", no_of_lot)
            req.write({
                'no_of_lot': no_of_lot,
                'print_lottable1': self.lottable1,
                'print_lottable2': self.lottable2,
                'print_lottable3': self.lottable3,
                'print_lottable4': self.lottable4,
                'print_lottable5': self.lottable5,
                'print_lottable6': self.lottable6,
                'print_lottable7': self.lottable7,
                'print_lottable8': self.lottable8,
                'print_lottable9': self.lottable9,
                'print_lottable10': self.lottable10,
            })
            return self.env.ref('lsc_wms_reports.wms_grn_report').report_action(req)