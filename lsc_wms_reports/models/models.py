# -*- coding: utf-8 -*-
import contextlib
from urllib.parse import urlencode
from urllib.request import urlopen

from odoo import models, fields, api, _


class GRN(models.Model):
    _inherit = 'lsc.grn'

    # no_of_lot = fields.Selection([('0', '0'), ('1', '1'), ('2', '2'), ('3', '3'), ('4', '4'), ('5', '5'),
    #                   ('6', '6'), ('7', '7'), ('8', '8'), ('9', '9'), ('10', '10')])
    #
    # no_of_lot = fields.Integer(string='No Of Lot To Prints')
    no_of_lot = fields.Integer(string='No Of Lot To Prints')

    print_lottable1 = fields.Boolean()
    print_lottable2 = fields.Boolean()
    print_lottable3 = fields.Boolean()
    print_lottable4 = fields.Boolean()
    print_lottable5 = fields.Boolean()
    print_lottable6 = fields.Boolean()
    print_lottable7 = fields.Boolean()
    print_lottable8 = fields.Boolean()
    print_lottable9 = fields.Boolean()
    print_lottable10 = fields.Boolean()

    def geturl(self):
        action_id = self.env.ref('lsc_wms.action_lsc_grn').id
        menu_id = self.env.ref('lsc_wms.menu_grn').id
        # dbname = self.env.cr.dbname or [''],
        link = "/web?#id=%s&action=%s&model=lsc.grn&view_type=form&menu_id=%s" % (self.id, action_id, menu_id)
        base_url = self.env['ir.config_parameter'].sudo().get_param('web.base.url')
        url = base_url + link #'/web?debug=1#id=1&action=1542&model=lsc.grn&view_type=form&cids=1&menu_id=754'
        # url = url.replace('#', '%23')
        # url = url.replace('&', '%26')
        print("URL", url)
        url = self.make_tiny(url)
        print("Tiny URL", url)
        return url

    def make_tiny(self,url):
        request_url = ('http://tinyurl.com/api-create.php?' + urlencode({'url': url}))
        with contextlib.closing(urlopen(request_url)) as response:
            return response.read().decode('utf-8 ')

    def print_grn(self):
        return {'name': _('Print GRN'),
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'lsc.grn.lot.selection.wizard',
                'target': 'new',
                # 'context': {'default_partner_id': self.customer_ref.id,
                #             'default_asn_id': self.id,
                #             'default_warehouse_id': self.warhouse_id.id
                #             },
                # 'view_id': False,
                'type': 'ir.actions.act_window'}


class Allocation(models.Model):
    _inherit = 'lsc.allocation'

    no_of_lot = fields.Integer(string='No Of Lot To Prints')
    print_lottable1 = fields.Boolean()
    print_lottable2 = fields.Boolean()
    print_lottable3 = fields.Boolean()
    print_lottable4 = fields.Boolean()
    print_lottable5 = fields.Boolean()
    print_lottable6 = fields.Boolean()
    print_lottable7 = fields.Boolean()
    print_lottable8 = fields.Boolean()
    print_lottable9 = fields.Boolean()
    print_lottable10 = fields.Boolean()

    def geturl(self):
        action_id = self.env.ref('lsc_wms.action_lsc_allocation').id
        menu_id = self.env.ref('lsc_wms.menu_allocation').id
        # dbname = self.env.cr.dbname or [''],
        link = "/web?#id=%s&action=%s&model=lsc.allocation&view_type=form&menu_id=%s" % (self.id, action_id, menu_id)
        base_url = self.env['ir.config_parameter'].sudo().get_param('web.base.url')
        url = base_url + link #'/web?debug=1#id=1&action=1542&model=lsc.grn&view_type=form&cids=1&menu_id=754'
        # url = url.replace('#', '%23')
        # url = url.replace('&', '%26')
        print("URL", url)
        url = self.make_tiny(url)

        print("Tiny URL", url)
        return url

    def print_allocation(self):
        return {'name': _('Print Allocation'),
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'lsc.allocation.lot.selection.wizard',
                'target': 'new',
                # 'context': {'default_partner_id': self.customer_ref.id,
                #             'default_asn_id': self.id,
                #             'default_warehouse_id': self.warhouse_id.id
                #             },
                # 'view_id': False,
                'type': 'ir.actions.act_window'}

    def get_pick_list_summary(self):
        qry = '''select p.code,loc.batch_number,sum(COALESCE(loc.qty,0)) as qty from lsc_allocation_line loc,lsc_sku_master p
            where p.id=loc.sku_id and loc.order_id='''+str(self.id)+'''
            group by p.code,loc.batch_number'''

        self.env.cr.execute(qry)
        res = self.env.cr.dictfetchall()
        print("PLR", res)
        return res

    def make_tiny(self,url):
        request_url = ('http://tinyurl.com/api-create.php?' + urlencode({'url': url}))
        with contextlib.closing(urlopen(request_url)) as response:
            return response.read().decode('utf-8 ')


class Dispatch(models.Model):
    _inherit = 'lsc.dispatch'

    # no_of_lot = fields.Selection([('0', '0'), ('1', '1'), ('2', '2'), ('3', '3'), ('4', '4'), ('5', '5'),
    #                   ('6', '6'), ('7', '7'), ('8', '8'), ('9', '9'), ('10', '10')])
    #
    # no_of_lot = fields.Integer(string='No Of Lot To Prints')
    no_of_lot = fields.Integer(string='No Of Lot To Prints')

    print_lottable1 = fields.Boolean()
    print_lottable2 = fields.Boolean()
    print_lottable3 = fields.Boolean()
    print_lottable4 = fields.Boolean()
    print_lottable5 = fields.Boolean()
    print_lottable6 = fields.Boolean()
    print_lottable7 = fields.Boolean()
    print_lottable8 = fields.Boolean()
    print_lottable9 = fields.Boolean()
    print_lottable10 = fields.Boolean()

    def geturl(self):
        action_id = self.env.ref('lsc_wms.action_lsc_dispatch').id
        menu_id = self.env.ref('lsc_wms.menu_dispatch').id
        # dbname = self.env.cr.dbname or [''],
        link = "/web?#id=%s&action=%s&model=lsc.dispatch&view_type=form&menu_id=%s" % (self.id, action_id, menu_id)
        base_url = self.env['ir.config_parameter'].sudo().get_param('web.base.url')
        url = base_url + link #'/web?debug=1#id=1&action=1542&model=lsc.grn&view_type=form&cids=1&menu_id=754'
        # url = url.replace('#', '%23')
        # url = url.replace('&', '%26')
        print("URL", url)
        url = self.make_tiny(url)
        print("Tiny URL", url)
        return url

    def make_tiny(self,url):
        request_url = ('http://tinyurl.com/api-create.php?' + urlencode({'url': url}))
        with contextlib.closing(urlopen(request_url)) as response:
            return response.read().decode('utf-8 ')

    def print_dn(self):
        return {'name': _('Print Delivery Note'),
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'lsc.dispatch.lot.selection.wizard',
                'target': 'new',
                # 'context': {'default_partner_id': self.customer_ref.id,
                #             'default_asn_id': self.id,
                #             'default_warehouse_id': self.warhouse_id.id
                #             },
                # 'view_id': False,
                'type': 'ir.actions.act_window'}

