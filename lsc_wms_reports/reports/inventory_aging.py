from datetime import datetime

from odoo import models, api, fields, _


class InventoryAgingReportWizard(models.TransientModel):
    _name = "inventory.aging.report.wizard"
    _description = "Inventory Aging Report Wizard"

    date_from = fields.Date("From", required=False)
    date_to = fields.Date("To", required=False)
    customer_id = fields.Many2one('res.partner', required=1, domain="[('customer_rank','>', 0),('wms','=',True)]")
    sku_id = fields.Many2one('lsc.sku.master', "SKU")
    warehouse_id = fields.Many2one('lsc.sub.warehouse', string='Warehouse')
    business_unit_id = fields.Many2one('lsc.bussines.unit', string="Business Unit")

    rec_lines = fields.Many2many('inventory.aging.report.detail')

    file = fields.Binary('Download Report')
    name = fields.Char()

    def run_report(self):
        print('Report Run')

        filter_val = [('customer_id', '=', self.customer_id.id), ('total_quantity', '>', 0)]
        if self.sku_id:
            filter_val.append(tuple(['sku_id', '=', self.sku_id.id]))
        if self.warehouse_id:
            filter_val.append(tuple(['warehouse_id', '=', self.warehouse_id.id]))
        if self.business_unit_id:
            filter_val.append(tuple(['bussines_unit_id', '=', self.business_unit_id.id]))

        lst = []
        inventory = self.env['inventory.report'].search(filter_val)
        for rec in inventory:
            if rec.total_quantity > 0:
                dic = {
                    'warehouse': rec.warehouse_id.name,
                    'customer': rec.customer_id.name,
                    'sku': rec.sku_id.code,
                    'description': rec.description,
                    'uom': rec.uom_id.name,
                    'batch_number': rec.batch_no or False,
                    'serial_number': rec.serial_no,
                    'business_unit': rec.bussines_unit_id.name,
                    'lot_no': rec.lot,
                    'manufacturing_date': rec.manf_date,
                    'expiry_date': rec.expiry_date,
                    'location': rec.location.location_name,
                    'quantity': rec.total_quantity,
                    'remarks': rec.remarks,
                    'lottable01': rec.lot1,
                    'lottable02': rec.lot2,
                    'lottable03': rec.lot3,
                    'lottable04': rec.lot4,
                    'lottable05': rec.lot5,
                    'lottable06': rec.lot6.id,
                    'lottable07': rec.lot7,
                    'lottable08': rec.lot8,
                    'lottable09': rec.lot9,
                    'lottable10': rec.lot10,
                    'age': abs((fields.datetime.today()-rec.create_date).days)+1
                }

                inv = self.env['inventory.aging.report.detail'].create(dic)
                lst.append(inv.id)

        self.rec_lines = [(6, 0, lst)]

    def generate_wps_xlsx(self):
        return self.env.ref('lsc_wms_reports.inventory_aging_report').report_action(self)


class InventoryAgingReportDetail(models.TransientModel):
    _name = "inventory.aging.report.detail"
    _description = "Inventory Aging Report Detail"

    warehouse = fields.Char()
    customer = fields.Char()
    sku = fields.Char()
    description = fields.Char()
    uom = fields.Char()
    batch_number = fields.Char()
    serial_number = fields.Char()
    business_unit = fields.Char()
    lot_no = fields.Char()
    manufacturing_date = fields.Date()
    expiry_date = fields.Date()
    location = fields.Char()
    quantity = fields.Float()
    remarks = fields.Char()
    lottable01 = fields.Char()
    lottable02 = fields.Char()
    lottable03 = fields.Char()
    lottable04 = fields.Char()
    lottable05 = fields.Char()
    lottable06 = fields.Many2one('sku.lotable.six')
    lottable07 = fields.Char()
    lottable08 = fields.Datetime()
    lottable09 = fields.Datetime()
    lottable10 = fields.Datetime()
    age = fields.Integer(string='Age (In Days)')


class InventoryAgingReport(models.AbstractModel):
    _name = 'report.inventory_aging_report_template'
    _description = "Inventory Aging XLSX Report"
    _inherit = 'report.report_xlsx.abstract'

    def generate_xlsx_report(self, workbook, report_detail, wizard_data):
        print("inventory.aging Wiz", wizard_data)
        date_f = wizard_data.date_from
        date_t = wizard_data.date_to

        def date_format(date):
            return date.strftime("%d-%m-%Y")

        heading_format = workbook.add_format({
            "bold": 1,
            "align": 'center',
            "valign": 'vcenter',
            "font_color": 'black',
            'font_size': '12',
            'font_name': 'Metropolis',
        })
        heading_data_format = workbook.add_format({
            "bold": 1,
            "align": 'center',
            "valign": 'vcenter',
            "font_color": 'black',
            'font_size': '11',
            'font_name': 'Metropolis',
        })
        line_heading_format = workbook.add_format({
            "bold": 1,
            "border": 1,
            "align": 'center',
            "valign": 'vcenter',
            "font_color": 'black',
            "bg_color": '#85C1E9',
            'font_size': '9',
            'font_name': 'Metropolis',
        })
        line_data = workbook.add_format({
            "border": 1,
            "align": 'center',
            "valign": 'vcenter',
            "font_color": 'black',
            'font_size': '9',
            'font_name': 'Metropolis',
            "num_format": "#,##0.00",
        })

        line_date_format = workbook.add_format({
            "border": 1,
            "align": 'center',
            "valign": 'vcenter',
            "font_color": 'black',
            'font_size': '9',
            'font_name': 'Metropolis',
            'num_format': "DD/MM/yyyy"
        })

        line_date_and_time_format = workbook.add_format({
            "border": 1,
            "align": 'center',
            "valign": 'vcenter',
            "font_color": 'black',
            'font_size': '9',
            'font_name': 'Metropolis',
            'num_format': "DD/MM/yyyy HH:MM"
        })

        worksheet = workbook.add_worksheet('Inventory Aging Report')

        worksheet.set_column('A:A', 12)
        worksheet.set_column('B:B', 35)
        worksheet.set_column('C:C', 15)
        worksheet.set_column('D:D', 25)
        worksheet.set_column('E:E', 15)
        worksheet.set_column('F:F', 20)

        worksheet.set_row(0, 45)
        worksheet.set_row(3, 30)

        worksheet.merge_range('A1:X1', 'LSC WAREHOUSING AND LOGISTICS SERVICES CO.', heading_format)
        worksheet.merge_range('A2:X2', 'Inventory Aging Report', heading_format)
        # worksheet.merge_range('A5:F5', 'For the Period : ' + str(date_f.strftime("%d/%m/%Y")) + '   -   ' + str(date_t.strftime("%d/%m/%Y")), heading_data_format)

        rows = 5
        worksheet.write_string(rows, 0, 'Customer', line_data)
        worksheet.write(rows, 1, wizard_data.customer_id.name, line_data)
        worksheet.write_string(rows, 2, 'Age As on', line_data)
        worksheet.write(rows, 3, fields.date.today().strftime("%d/%m/%Y"), line_data)

        rows += 1
        worksheet.write_string(rows, 0, 'Warehouse #', line_heading_format)
        worksheet.write_string(rows, 1, 'SKU', line_heading_format)
        worksheet.write_string(rows, 2, 'Description', line_heading_format)
        worksheet.write_string(rows, 3, 'UOM', line_heading_format)
        worksheet.write_string(rows, 4, 'Batch Number', line_heading_format)
        worksheet.write_string(rows, 5, 'Serial Number', line_heading_format)
        worksheet.write_string(rows, 6, 'Business Unit', line_heading_format)
        worksheet.write_string(rows, 7, 'Lot No', line_heading_format)
        worksheet.write_string(rows, 8, 'Manufacturing Date', line_heading_format)
        worksheet.write_string(rows, 9, 'Expiry Date', line_heading_format)
        worksheet.write_string(rows, 10, 'Location', line_heading_format)
        worksheet.write_string(rows, 11, 'Quantity', line_heading_format)
        worksheet.write_string(rows, 12, 'Remarks', line_heading_format)
        worksheet.write_string(rows, 13, 'Lottable01', line_heading_format)
        worksheet.write_string(rows, 14, 'Lottable02', line_heading_format)
        worksheet.write_string(rows, 15, 'Lottable03', line_heading_format)
        worksheet.write_string(rows, 16, 'Lottable04', line_heading_format)
        worksheet.write_string(rows, 17, 'Lottable05', line_heading_format)
        worksheet.write_string(rows, 18, 'Lottable06', line_heading_format)
        worksheet.write_string(rows, 19, 'Lottable07', line_heading_format)
        worksheet.write_string(rows, 20, 'Lottable08', line_heading_format)
        worksheet.write_string(rows, 21, 'Lottable09', line_heading_format)
        worksheet.write_string(rows, 22, 'Lottable10', line_heading_format)
        worksheet.write_string(rows, 23, 'Age (in Days)', line_heading_format)
        rows += 1
        for lines in wizard_data.rec_lines:
            worksheet.write(rows, 0, lines.warehouse, line_data)
            worksheet.write(rows, 1, lines.sku, line_data)
            worksheet.write(rows, 2, lines.description, line_data)
            worksheet.write(rows, 3, lines.uom, line_data)
            worksheet.write(rows, 4, lines.batch_number, line_data)
            worksheet.write(rows, 5, lines.serial_number, line_data)
            worksheet.write(rows, 6, lines.business_unit, line_data)
            worksheet.write(rows, 7, lines.lot_no, line_data)
            worksheet.write(rows, 8, lines.manufacturing_date, line_date_format)
            worksheet.write(rows, 9, lines.expiry_date, line_date_format)
            worksheet.write(rows, 10, lines.location, line_data)
            worksheet.write(rows, 11, lines.quantity, line_data)
            worksheet.write(rows, 12, lines.remarks, line_data)

            worksheet.write(rows, 13, lines.lottable01, line_data)
            worksheet.write(rows, 14, lines.lottable02, line_data)
            worksheet.write(rows, 15, lines.lottable03, line_data)
            worksheet.write(rows, 16, lines.lottable04, line_data)
            worksheet.write(rows, 17, lines.lottable05, line_data)
            worksheet.write(rows, 18, lines.lottable06.name, line_data)
            worksheet.write(rows, 19, lines.lottable07, line_data)
            worksheet.write(rows, 20, lines.lottable08, line_date_and_time_format)
            worksheet.write(rows, 21, lines.lottable09, line_date_and_time_format)
            worksheet.write(rows, 22, lines.lottable10, line_date_and_time_format)

            worksheet.write(rows, 23, lines.age, line_data)

            rows += 1
