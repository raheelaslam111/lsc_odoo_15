from datetime import datetime

from odoo import models, api, fields, _


class PendencyReportWizard(models.TransientModel):
    _name = "pendency.report.wizard"
    _description = "Pendency Report Wizard"

    date_from = fields.Date("From", required=True)
    date_to = fields.Date("To", required=True)

    customer_id = fields.Many2one('res.partner', domain="[('customer_rank','>', 0),('wms','=',True)]")
    sku_id = fields.Many2one('lsc.sku.master', "SKU")
    warehouse_id = fields.Many2one('lsc.warehouse.master', string='Warehouse')
    business_unit_id = fields.Many2one('lsc.bussines.unit', string="Business Unit")
    transaction_type = fields.Selection([('asn', 'ASN'), ('grn', 'GRN'), ('putaway', 'Putaway'), ('outbound', 'Outbound')])

    rec_lines = fields.Many2many('pendency.report.detail')

    file = fields.Binary('Download Report')
    name = fields.Char()

    def run_report(self):

        transaction_type = 'all'
        if self.transaction_type:
            transaction_type = self.transaction_type

        lst = []
        ###################### ASN ##############################
        if transaction_type == 'all' or transaction_type == 'asn':
            filter_val = [('create_date', '>=', self.date_from), ('create_date', '<=', self.date_to),
                          ('state', 'in', ['in_receiving', 'new', 'cancel'])]
            if self.customer_id:
                filter_val.append(tuple(['customer_id_id', '=', self.customer_id.id]))

            grn = self.env['lsc.asn'].search(filter_val)
            for rec in grn:
                dic = {
                    'doc_id': rec.id,
                    'date': rec.create_date,
                    'document_number': rec.name,
                    'transaction_type': 'ASN',
                    'aging': abs((fields.datetime.today() - rec.create_date).days),
                    'status': dict(rec._fields['state'].selection).get(rec.state),
                    'external_ref': rec.ext_ref,
                    'warehouse': rec.warehouse_id.name,
                    'customer': rec.customer_id_id.name
                }

                inv = self.env['pendency.report.detail'].create(dic)
                lst.append(inv.id)

        ###################### GRN ##############################
        if transaction_type == 'all' or transaction_type == 'grn':
            filter_val = [('create_date', '>=', self.date_from), ('create_date', '<=', self.date_to), ('state', 'in', ['in_receiving', 'new', 'cancel'])]
            if self.customer_id:
                filter_val.append(tuple(['customer_id', '=', self.customer_id.id]))
            grn = self.env['lsc.grn'].search(filter_val)
            for rec in grn:
                dic = {
                    'doc_id': rec.id,
                    'date': rec.create_date,
                    'document_number': rec.name,
                    'transaction_type': 'GRN',
                    'aging': abs((fields.datetime.today()-rec.create_date).days),
                    'status': dict(rec._fields['state'].selection).get(rec.state),
                    'external_ref': rec.external_ref,
                    'warehouse': rec.warehouse_id.name,
                    'customer': rec.customer_id.name
                }

                inv = self.env['pendency.report.detail'].create(dic)
                lst.append(inv.id)

        ###################### Putaway ##############################
        if transaction_type == 'all' or transaction_type == 'putaway':
            filter_val = [('create_date', '>=', self.date_from), ('create_date', '<=', self.date_to), ('state', 'in', ['draft', 'cancel'])]
            if self.customer_id:
                filter_val.append(tuple(['customer_id', '=', self.customer_id.id]))
            grn = self.env['wms.putaway'].search(filter_val)
            for rec in grn:
                dic = {
                    'doc_id': rec.id,
                    'date': rec.create_date,
                    'document_number': rec.name,
                    'transaction_type': 'Putaway',
                    'aging': abs((fields.datetime.today()-rec.create_date).days),
                    'status': dict(rec._fields['state'].selection).get(rec.state),
                    'external_ref': rec.external_ref,
                    'warehouse': rec.warehouse_id.name,
                    'customer': rec.customer_id.name
                }

                inv = self.env['pendency.report.detail'].create(dic)
                lst.append(inv.id)

        ###################### Outbound ##############################
        if transaction_type == 'all' or transaction_type == 'outbound':
            filter_val = [('create_date', '>=', self.date_from), ('create_date', '<=', self.date_to), ('state', 'not in', ['shipped_part', 'closed', 'cancel'])]
            if self.customer_id:
                filter_val.append(tuple(['customer_id', '=', self.customer_id.id]))
            grn = self.env['lsc.outbound.orders'].search(filter_val)
            for rec in grn:
                dic = {
                    'doc_id': rec.id,
                    'date': rec.create_date,
                    'document_number': rec.name,
                    'transaction_type': 'Outbound',
                    'aging': abs((fields.datetime.today()-rec.create_date).days),
                    'status': dict(rec._fields['state'].selection).get(rec.state),
                    'external_ref': rec.external_ref,
                    'warehouse': rec.warehouse_id.name,
                    'customer': rec.customer_id.name
                }

                inv = self.env['pendency.report.detail'].create(dic)
                lst.append(inv.id)

        ###################### Allocation ##############################
        # filter_val = [('create_date', '>=', self.date_from), ('create_date', '<=', self.date_to), ('state', 'not in', ['created', 'closed'])]
        # if self.customer_id:
        #     filter_val.append(tuple(['customer_id', '=', self.customer_id.id]))
        # grn = self.env['lsc.allocation'].search(filter_val)
        # for rec in grn:
        #     dic = {
        #         'doc_id': rec.id,
        #         'date': rec.create_date,
        #         'document_number': rec.name,
        #         'transaction_type': 'Allocation',
        #         'aging': abs((fields.datetime.today()-rec.create_date).days),
        #         'status': dict(rec._fields['state'].selection).get(rec.state)
        #     }
        #
        #     inv = self.env['pendency.report.detail'].create(dic)
        #     lst.append(inv.id)

        self.rec_lines = [(6, 0, lst)]

    def generate_wps_xlsx(self):
        return self.env.ref('lsc_wms_reports.pendency_report').report_action(self)


class PendencyReportDetail(models.TransientModel):
    _name = "pendency.report.detail"
    _description = "Pendency Report Detail"

    doc_id = fields.Integer()
    date = fields.Datetime()
    document_number = fields.Char()
    transaction_type = fields.Char()
    aging = fields.Char()
    status = fields.Char()
    external_ref = fields.Char()
    warehouse = fields.Char()
    customer = fields.Char()

    def view_detail(self):
        if self.transaction_type == 'ASN':
            result = self.env['ir.actions.act_window']._for_xml_id('lsc_wms.action_lsc_asn')

            res = self.env.ref('lsc_wms.lsc_asn_view_form', False)

        if self.transaction_type == 'GRN':
            result = self.env['ir.actions.act_window']._for_xml_id('lsc_wms.action_lsc_grn')

            res = self.env.ref('lsc_wms.lsc_grn_view_form', False)

        if self.transaction_type == 'Putaway':
            result = self.env['ir.actions.act_window']._for_xml_id('lsc_wms.action_wms_putaway')

            res = self.env.ref('lsc_wms.wms_putaway_view_form', False)

        if self.transaction_type == 'Outbound':
            result = self.env['ir.actions.act_window']._for_xml_id('lsc_wms.action_lsc_outbound_orders')

            res = self.env.ref('lsc_wms.lsc_outbound_orders_view_form', False)
        form_view = [(res and res.id or False, 'form')]
        if 'views' in result:
            result['views'] = form_view + [(state, view) for state, view in result['views'] if view != 'form']
        else:
            result['views'] = form_view
        result['res_id'] = self.doc_id

        return result


class PendencyReport(models.AbstractModel):
    _name = 'report.pendency_report_template'
    _description = "Pendency XLSX Report"
    _inherit = 'report.report_xlsx.abstract'

    def generate_xlsx_report(self, workbook, report_detail, wizard_data):
        print("pendency Wiz", wizard_data)
        date_f = wizard_data.date_from
        date_t = wizard_data.date_to

        def date_format(date):
            return date.strftime("%d-%m-%Y")

        heading_format = workbook.add_format({
            "bold": 1,
            "align": 'center',
            "valign": 'vcenter',
            "font_color": 'black',
            'font_size': '12',
            'font_name': 'Metropolis',
        })
        heading_data_format = workbook.add_format({
            "bold": 1,
            "align": 'center',
            "valign": 'vcenter',
            "font_color": 'black',
            'font_size': '11',
            'font_name': 'Metropolis',
        })
        line_heading_format = workbook.add_format({
            "bold": 1,
            "border": 1,
            "align": 'center',
            "valign": 'vcenter',
            "font_color": 'black',
            "bg_color": '#85C1E9',
            'font_size': '9',
            'font_name': 'Metropolis',
        })
        line_data = workbook.add_format({
            "border": 1,
            "align": 'center',
            "valign": 'vcenter',
            "font_color": 'black',
            'font_size': '9',
            'font_name': 'Metropolis',
            "num_format": "#,##0.00",
        })

        line_date_format = workbook.add_format({
            "border": 1,
            "align": 'center',
            "valign": 'vcenter',
            "font_color": 'black',
            'font_size': '9',
            'font_name': 'Metropolis',
            'num_format': "DD/MM/yyyy"
        })

        line_date_and_time_format = workbook.add_format({
            "border": 1,
            "align": 'center',
            "valign": 'vcenter',
            "font_color": 'black',
            'font_size': '9',
            'font_name': 'Metropolis',
            'num_format': "DD/MM/yyyy HH:MM"
        })

        worksheet = workbook.add_worksheet('Pendency Report')

        worksheet.set_column('A:A', 20)
        worksheet.set_column('B:B', 35)
        worksheet.set_column('C:C', 20)
        worksheet.set_column('D:D', 25)
        worksheet.set_column('E:E', 20)
        worksheet.set_column('F:F', 20)
        worksheet.set_column('G:G', 25)

        worksheet.set_row(0, 45)
        # worksheet.set_row(3, 30)
        report_type = ''

        worksheet.merge_range('A1:H1', 'LSC WAREHOUSING AND LOGISTICS SERVICES CO.', heading_format)
        worksheet.merge_range('A2:H2', 'Pendency Report', heading_format)
        worksheet.merge_range('A3:H3', 'For the Period : ' + str(date_f.strftime("%d/%m/%Y")) + '   -   ' + str(date_t.strftime("%d/%m/%Y")), heading_data_format)

        rows = 4
        worksheet.write_string(rows, 0, 'Date', line_heading_format)
        worksheet.write_string(rows, 1, 'Document Number', line_heading_format)
        worksheet.write_string(rows, 2, 'Transactions Type', line_heading_format)
        worksheet.write_string(rows, 3, 'External Reference', line_heading_format)
        worksheet.write_string(rows, 4, 'Customer', line_heading_format)
        worksheet.write_string(rows, 5, 'Warehouse', line_heading_format)
        worksheet.write_string(rows, 6, 'Aging', line_heading_format)
        worksheet.write_string(rows, 7, 'Status', line_heading_format)

        rows += 1
        for lines in wizard_data.rec_lines:
            worksheet.write(rows, 0, lines.date, line_date_and_time_format)
            worksheet.write(rows, 1, lines.document_number, line_data)
            worksheet.write(rows, 2, lines.transaction_type, line_data)
            worksheet.write(rows, 3, lines.external_ref, line_data)
            worksheet.write(rows, 4, lines.customer, line_data)
            worksheet.write(rows, 5, lines.warehouse, line_data)
            worksheet.write(rows, 6, lines.aging, line_data)
            worksheet.write(rows, 7, lines.status, line_data)

            rows += 1
