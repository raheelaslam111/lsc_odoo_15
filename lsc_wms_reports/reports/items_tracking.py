from datetime import datetime
import pdb

from odoo import models, api, fields, _


class ItemsTrackingReportWizard(models.TransientModel):
    _name = "items.tracking.report.wizard"
    _description = "Items Tracking Report Wizard"

    report_type = fields.Selection([('batch_wise', 'Batch Wise'), ('serialize', 'Serialize')], default='batch_wise')
    date_from = fields.Date("From", required=True)
    date_to = fields.Date("To", required=True)
    customer_id = fields.Many2one('res.partner', required=1, domain="[('customer_rank','>', 0),('wms','=',True)]")
    sku_id = fields.Many2one('lsc.sku.master', required=1)
    warehouse_id = fields.Many2one('lsc.sub.warehouse', string='Warehouse', required=1)
    batch_no = fields.Char()
    serial_no = fields.Char()
    business_unit_id = fields.Many2one('lsc.bussines.unit', string="Business Unit")

    rec_lines = fields.Many2many('items.tracking.report.detail')

    file = fields.Binary('Download Report')
    name = fields.Char()

    @api.onchange('customer_id')
    def onchange_customer_id(self):
        return {'domain': {'warehouse_id': [('id', 'in', self.customer_id.warhouse_ids.ids)]}}

    @api.onchange('sku_id')
    def onchange_sku_id(self):
        return {'domain': {'business_unit_id': [('id', 'in', self.sku_id.business_id.ids)]}}

    # @api.onchange('sku_id')
    # def onchange_sku_id_for_batch(self):
    #     # batch_rec = self.env['wms.transactions'].search([('sku_id', '=', self.sku_id.id)]).mapped('batch_no').mapped('id')
    #     batch_list_ids = []
    #     batch_rec = self.env['wms.transactions'].search([('sku_id', '=', self.sku_id.id)])
    #     for b in batch_rec:
    #         if b.batch_no:
    #             b.batch_no
    #     # if batch_rec:
    #     #     batch_list = batch_rec.mapped('batch_no')
    #     #     pdb.set_trace()
    #     #     if batch_list != []:
    #     #         batch_list_ids = batch_list.mapped('id')
    #
    #     print("Batch Rec")
    #     return {'domain': {'batch_no': [('id', 'in', batch_list_ids)]}}

    def run_report(self):
        op_filter_val = [('customer_id', '=', self.customer_id.id),
                         ('date', '<', self.date_from),
                         ]

        filter_val = [('customer_id', '=', self.customer_id.id), ('quantity', '>', 0),
                      ('date', '>=', self.date_from), ('date', '<=', self.date_to)]
        if self.report_type == 'batch_wise':
            op_filter_val.append(('batch_no', '=', self.batch_no))
            filter_val.append(('batch_no', '=', self.batch_no))
        else:
            op_filter_val.append(('serial_no', '=', self.serial_no))
            filter_val.append(('serial_no', '=', self.serial_no))

        if self.sku_id:
            op_filter_val.append(tuple(['sku_id', '=', self.sku_id.id]))
            filter_val.append(tuple(['sku_id', '=', self.sku_id.id]))
        if self.warehouse_id:
            op_filter_val.append(tuple(['warehouse_id', '=', self.warehouse_id.id]))
            filter_val.append(tuple(['warehouse_id', '=', self.warehouse_id.id]))
        if self.business_unit_id:
            op_filter_val.append(tuple(['bussines_unit_id', '=', self.business_unit_id.id]))
            filter_val.append(tuple(['bussines_unit_id', '=', self.business_unit_id.id]))

        op_stock = 0

        op_inventory = self.env['wms.transactions'].search(op_filter_val)

        for inv in op_inventory:
            if inv.transaction_type == 'grn':
                op_stock += inv.quantity
            if inv.transaction_type == 'outbound':
                op_stock -= inv.quantity

        lst = []
        inventory = self.env['wms.transactions'].search(filter_val)
        dic = {
            'date': self.date_from,
            'document_number': None,
            'transaction_type': 'Opening Stock',
            'qty': op_stock
        }

        inv = self.env['items.tracking.report.detail'].create(dic)
        lst.append(inv.id)

        for rec in inventory:
            dic = {
                'transaction_id': rec.id,
                'date': rec.date,
                'document_number': rec.document_no,
                'transaction_type': dict(rec._fields['transaction_type'].selection).get(rec.transaction_type),
                'qty': rec.quantity
            }
            if rec.transaction_type == 'grn':
                op_stock += rec.quantity
            if rec.transaction_type == 'outbound':
                op_stock -= rec.quantity
            inv = self.env['items.tracking.report.detail'].create(dic)
            lst.append(inv.id)

        dic = {
            'date': None,
            'document_number': None,
            'transaction_type': 'Closing Stock',
            'qty': op_stock
        }

        inv = self.env['items.tracking.report.detail'].create(dic)
        lst.append(inv.id)

        self.rec_lines = [(6, 0, lst)]

    def generate_wps_xlsx(self):
        return self.env.ref('lsc_wms_reports.items_tracking_report').report_action(self)


class ItemsTrackingReportDetail(models.TransientModel):
    _name = "items.tracking.report.detail"
    _description = "Items Tracking Report Detail"

    transaction_id = fields.Integer()
    date = fields.Date()
    document_number = fields.Char()
    transaction_type = fields.Char()
    qty = fields.Float()

    def view_detail(self):
        result = self.env['ir.actions.act_window']._for_xml_id('lsc_wms.action_wms_transactions')

        res = self.env.ref('lsc_wms.wms_transactions_view_form', False)
        form_view = [(res and res.id or False, 'form')]
        if 'views' in result:
            result['views'] = form_view + [(state, view) for state, view in result['views'] if view != 'form']
        else:
            result['views'] = form_view
        result['res_id'] = self.transaction_id

        return result



class ItemsTrackingReport(models.AbstractModel):
    _name = 'report.items_tracking_report_template'
    _description = "Items Tracking XLSX Report"
    _inherit = 'report.report_xlsx.abstract'

    def generate_xlsx_report(self, workbook, report_detail, wizard_data):
        print("inventory.aging Wiz", wizard_data)
        date_f = wizard_data.date_from
        date_t = wizard_data.date_to

        def date_format(date):
            return date.strftime("%d-%m-%Y")

        heading_format = workbook.add_format({
            "bold": 1,
            "align": 'center',
            "valign": 'vcenter',
            "font_color": 'black',
            'font_size': '12',
            'font_name': 'Metropolis',
        })
        heading_data_format = workbook.add_format({
            "bold": 1,
            "align": 'center',
            "valign": 'vcenter',
            "font_color": 'black',
            'font_size': '11',
            'font_name': 'Metropolis',
        })
        line_heading_format = workbook.add_format({
            "bold": 1,
            "border": 1,
            "align": 'center',
            "valign": 'vcenter',
            "font_color": 'black',
            "bg_color": '#85C1E9',
            'font_size': '9',
            'font_name': 'Metropolis',
        })
        line_data = workbook.add_format({
            "border": 1,
            "align": 'center',
            "valign": 'vcenter',
            "font_color": 'black',
            'font_size': '9',
            'font_name': 'Metropolis',
            "num_format": "#,##0.00",
        })

        line_date_format = workbook.add_format({
            "border": 1,
            "align": 'center',
            "valign": 'vcenter',
            "font_color": 'black',
            'font_size': '9',
            'font_name': 'Metropolis',
            'num_format': "DD/MM/yyyy"
        })

        line_date_and_time_format = workbook.add_format({
            "border": 1,
            "align": 'center',
            "valign": 'vcenter',
            "font_color": 'black',
            'font_size': '9',
            'font_name': 'Metropolis',
            'num_format': "DD/MM/yyyy HH:MM"
        })

        worksheet = workbook.add_worksheet('Inventory Aging Report')

        worksheet.set_column('A:A', 12)
        worksheet.set_column('B:B', 35)
        worksheet.set_column('C:C', 15)
        worksheet.set_column('D:D', 25)
        worksheet.set_column('E:E', 15)
        worksheet.set_column('F:F', 20)

        worksheet.set_row(0, 45)
        # worksheet.set_row(3, 30)
        report_type = ''
        if wizard_data.report_type == 'batch_wise':
            report_type = 'Batch Wise '
        else:
            report_type = 'Serialize '

        worksheet.merge_range('A1:D1', 'LSC WAREHOUSING AND LOGISTICS SERVICES CO.', heading_format)
        worksheet.merge_range('A2:D2', report_type + 'Items Tracking Report', heading_format)
        # worksheet.merge_range('A5:D5', 'For the Period : ' + str(date_f.strftime("%d/%m/%Y")) + '   -   ' + str(date_t.strftime("%d/%m/%Y")), heading_data_format)

        rows = 4
        worksheet.write_string(rows, 0, 'Customer', line_data)
        worksheet.write(rows, 1, wizard_data.customer_id.name, line_data)
        worksheet.write_string(rows, 2, 'SKU', line_data)
        worksheet.write(rows, 3, wizard_data.sku_id.code, line_data)

        rows += 1
        worksheet.write_string(rows, 0, 'Warehouse', line_data)
        worksheet.write(rows, 1, wizard_data.warehouse_id.name, line_data)
        worksheet.write_string(rows, 2, 'Batch / Serial', line_data)
        if wizard_data.report_type == 'batch_wise':
            worksheet.write(rows, 3, wizard_data.batch_no, line_data)
        else:
            worksheet.write(rows, 3, wizard_data.serial_no, line_data)

        rows += 2
        worksheet.write_string(rows, 0, 'Business Unit', line_data)
        worksheet.write(rows, 1, wizard_data.business_unit_id.name, line_data)
        worksheet.write_string(rows, 2, 'Opening Stock', line_data)
        worksheet.write(rows, 3, '0', line_data)

        rows += 1
        worksheet.write_string(rows, 0, 'Date', line_heading_format)
        worksheet.write_string(rows, 1, 'Document Number', line_heading_format)
        worksheet.write_string(rows, 2, 'Transactions Type', line_heading_format)
        worksheet.write_string(rows, 3, 'Qty', line_heading_format)

        rows += 1
        count = 0
        for lines in wizard_data.rec_lines:
            count = count + 1
            if count == 1:
                worksheet.write(rows-2, 3, lines.qty, line_data)
            else:
                if lines.date:
                    worksheet.write(rows, 0, lines.date, line_date_format)
                else:
                    worksheet.write(rows, 0, '', line_data)
                if lines.document_number:
                    worksheet.write(rows, 1, lines.document_number, line_data)
                else:
                    worksheet.write(rows, 0, '', line_data)
                worksheet.write(rows, 2, lines.transaction_type, line_data)
                worksheet.write(rows, 3, lines.qty, line_data)

                rows += 1
