# -*- coding: utf-8 -*-
{
    'name': "lsc_wms_reports",

    'summary': """
        Short (1 phrase/line) summary of the module's purpose, used as
        subtitle on modules listing or apps.openerp.com""",

    'description': """
        Long description of module's purpose
    """,

    'author': "My Company",
    'website': "http://www.yourcompany.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/14.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base', 'lsc_wms','report_xlsx'],

    # always loaded
    'data': [
        'security/ir.model.access.csv',
        'views/views.xml',
        'views/templates.xml',
        'reports/inventory_aging.xml',
        'reports/items_on_hold.xml',
        'reports/items_tracking.xml',
        'reports/pendency_report.xml',
        'prints/grn_print.xml',
        'prints/pick_list.xml',
        'prints/dispatch_note_print.xml',
        'wizard/grn_lot_selection.xml',
        'wizard/allocation_lot_selection.xml',
        'wizard/dn_lot_selection.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}
